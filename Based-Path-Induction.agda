{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`


module Based-Path-Induction where

open import Basics public


-- Definining ℍ

ℍΣ : (X : 𝓤 ℓ) (x : X) (A : Σ (x ≡_) → 𝓤 ℓ') (a : A  (x , refl x)) →  (yp : Σ (x ≡_)) → A yp
ℍΣ X x A a yp = tr {X = Σ (x ≡_)} {Y = A} (eqx/ctr' x yp) a
 where
  eqx/ctr : (x : X) → (y : X) → (p : x ≡ y) → (x , refl x) ≡ (y , p)
  eqx/ctr = 𝕁' (λ x → refl (x , refl x))

  eqx/ctr' : (x : X) (yp : Σ (x ≡_)) → (x , refl x) ≡ yp
  eqx/ctr' x yp = Σ-induction {X = X} {Y = (x ≡_)} {A = λ yp → (x , refl x) ≡ yp} (λ y p → eqx/ctr x y p) yp

ℍΣ/refl : (X : 𝓤 ℓ) (x : X) (A : Σ (x ≡_) → 𝓤 ℓ') (a : A  (x , refl x)) →  ℍΣ X x A a (x , refl x) ≡ a
ℍΣ/refl X x A a = tr/refl-at {X = Σ ( x ≡_)} {Y = A} (x , refl x) a 

ℍ : (X : 𝓤 ℓ) (x : X) (A : (y : X) → (p : x ≡ y) → 𝓤 ℓ') (a : A x (refl x)) →  (y : X) → (p : x ≡ y) → A y p
ℍ X x A a y p =  ℍΣ X x AΣ aΣ (y , p)
 where
  AΣ : Σ (x ≡_) → 𝓤 _
  AΣ = Σ-induction A
  aΣ : AΣ (x , refl x)
  aΣ = a

ℍ/refl : (X : 𝓤 ℓ) (x : X) (A : (y : X) → (p : x ≡ y) → 𝓤 ℓ') (a : A x (refl x)) → ℍ X x A a x (refl x) ≡ a
ℍ/refl X x A a = tr/refl-at {X = Σ (x ≡_)} {Y = Σ-induction A} (x , refl x) a

-- Definining ℍ as in the first chapter of the book (p. 69 of my copy)

ℍ' : (X : 𝓤 ℓ) (x : X) (A : (z : X) → (q : x ≡ z) → 𝓤 ℓ') (a : A x (refl x)) →  (z : X) → (q : x ≡ z) → A z q
ℍ' X x A a z q = 𝕁 X D d x z q A a
 where
  D = λ x y p → (C : (z : X) (q : x ≡ z) → 𝓤 _) → C x (refl x) → C y p
  d : (x : X) → D x x (refl x) 
  d = (λ x → λ C c → c)


-- And getting 𝕁 back from ℍ

𝕁-via-ℍ : (X : 𝓤 ℓ) (B : (x y : X) → x ≡ y → 𝓤 ℓ') (b : (x : X) → B x x (refl x)) → (x y : X) →  (p : x ≡ y) → B x y p
𝕁-via-ℍ X B b x y p =  ℍ X x (λ y p → B x y p) (b x) y p  

𝕁-via-ℍ/refl : (X : 𝓤 ℓ) (B : (x y : X) → x ≡ y → 𝓤 ℓ') (b : (x : X) → B x x (refl x)) → (x : X) → 𝕁-via-ℍ X B b x x (refl x) ≡ b x
𝕁-via-ℍ/refl X B b x = ℍ/refl X x (λ y p → B x y p) (b x)  


