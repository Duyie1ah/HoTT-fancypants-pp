{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`


module Basics where

open import Spartan-MLTT public

-- Define both projections in term of Σ-induction
-- Assuming Σ-induction is our primitive, we have that fst (x , y) = x and snd (x , y) = y, judgementally.


fst : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} → Σ {X = X} Y → X
fst {X = X} {Y = Y} p = Σ-induction {A = λ p → X} (λ x y → x) p

snd : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} → (p : Σ {X = X} Y) → Y (fst p)
snd {X = X} {Y = Y} p = Σ-induction {A = λ p → Y (fst p)} (λ (x : X) (y : Y x) → y) p

fst/eq : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} (x : X) (y : Y x) → (fst {X = X} {Y = Y} (x , y)) ≡ x
fst/eq {X = X} {Y = Y} x y = refl x 

snd/eq : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} (x : X) (y : Y x) → (snd {X = X} {Y = Y} (x , y)) ≡ y
snd/eq {X = X} {Y = Y} x y = refl y 

Σ-uniq : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} → (p : Σ Y) → p ≡ (fst p , snd p)
Σ-uniq {X = X} {Y = Y} = Σ-induction (λ (x : X) (y : Y x) → refl (x , y))

{-
Σ-uniq/pair  : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} → (x : X) → (y : Y x) → Σ-uniq {X = X} {Y} (x , y) ≡ (refl (x , y))
Σ-uniq/pair {X = X} {Y = Y} x y = refl (refl (x , y))
-}


-- ≡ is an equivalence relation

  
≡/refl : {X : 𝓤 ℓ} → (x : X) → x ≡ x
≡/refl = refl

≡/symm : {X : 𝓤 ℓ} → {x y : X} → x ≡ y → y ≡ x
≡/symm {X = X} {x = x} {y = y} p = 𝕁 X (λ x y p → y ≡ x) (λ x → refl x) x y p

_⁻¹ : {X : 𝓤 ℓ} {x y : X} → x ≡ y → y ≡ x
_⁻¹ p = ≡/symm p
infix  40 _⁻¹


≡/tran : {X : 𝓤 ℓ} → {x y z : X} → x ≡ y → y ≡ z → x ≡ z
≡/tran {X = X} {x = x} {y = y} {z = z} p q =
  𝕁
    X
    (λ x y p → (z : X) → (q : y ≡ z) → x ≡ z)
    (λ x z q → q)
    x y p z q

_∙_ : {X : 𝓤 ℓ} → {x y z : X} → x ≡ y → y ≡ z → x ≡ z
_∙_ p q = ≡/tran p q
infixl 30 _∙_


⁻¹/refl : {X : 𝓤 ℓ} (x : X) → (refl x) ⁻¹ ≡ refl x
⁻¹/refl x = refl (refl x)

⁻¹/involution : {X : 𝓤 ℓ} → {x y : X} → (p : x ≡ y) → (p ⁻¹) ⁻¹ ≡ p
⁻¹/involution {X = X} {x} {y} p = 𝕁 X (λ x y p → (p ⁻¹) ⁻¹ ≡ p) (λ x → refl (refl x)) x y p

∙/left-refl : {X : 𝓤 ℓ} → {x y : X} → (p : x ≡ y) → (refl x) ∙ p ≡ p
∙/left-refl {X = X} {x} {y} p = refl p

∙/right-refl : {X : 𝓤 ℓ} → {x y : X} → (p : x ≡ y) → p ∙ (refl y) ≡ p
∙/right-refl {X = X} {x} {y} p = 𝕁 X (λ x y p → p ∙ (refl y) ≡ p) (λ x → refl (refl x)) x y p

∙/left-⁻¹ : {X : 𝓤 ℓ} {x y : X} (p : x ≡ y) →  ((p ⁻¹) ∙ p) ≡ refl y
∙/left-⁻¹ {X = X} {x} {y} p = 𝕁 X (λ x y p → (p ⁻¹) ∙ p ≡ refl y) (λ x → refl (refl x)) x y p

∙/right-⁻¹ : {X : 𝓤 ℓ} {x y : X} (p : x ≡ y) →  (p ∙ (p ⁻¹)) ≡ refl x
∙/right-⁻¹ {X = X} {x} {y} p = 𝕁 X (λ x y p → p ∙  (p ⁻¹) ≡ refl x) (λ x → refl (refl x)) x y p

∙/assoc : {X : 𝓤 ℓ} {x y z w : X} (p : x ≡ y) (q : y ≡ z) (r : z ≡ w) → (p ∙ q) ∙ r ≡ p ∙ (q ∙ r)
∙/assoc {X = X} {x} {y} {z} {w} p q r =
  𝕁
    X
    (λ (x y : X) (p : x ≡ y) → (z w : X) → (q : y ≡ z) (r : z ≡ w) → (p ∙ q) ∙ r ≡ p ∙ (q ∙ r))
    ( λ (x z w : X) (q : x ≡ z) (r : z ≡ w) →
      refl (q ∙ r)
    )
    x y p z w q r


-- version with nested use of 𝕁 (pedagogical purposes)
≡/tran' : {X : 𝓤 ℓ} → {x y z : X} → x ≡ y → y ≡ z → x ≡ z
≡/tran' {X = X} {x = x} {y = y} {z = z} p q =
  𝕁
    X
    (λ x y p → (z : X) → (q : y ≡ z) → x ≡ z)
    (λ x z q → 𝕁 X (λ x z q → x ≡ z) (λ x → refl x) x z q)
    x y p z q


_≡⟨_⟩_ : {X : 𝓤 ℓ} (x : X) {y z : X} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ p ⟩ q = p ∙ q
infixr  0 _≡⟨_⟩_

_∎ : {X : 𝓤 ℓ} (x : X) → x ≡ x
x ∎ = refl x
infix   1 _∎



-- transport and ap


tr : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {x₀ x₁ : X} (p : x₀ ≡ x₁) → Y x₀ → Y x₁
tr {X = X} {Y} {x₀} {x₁} p = 𝕁 X (λ x₀ x₁ p → Y x₀ → Y x₁) (λ x → id) x₀ x₁ p

tr/refl : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} (x : X) → tr {Y = Y} (refl x) ≡ id
tr/refl x = refl (id) 

tr/refl-at : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} (x : X) (y : Y x) → tr {Y = Y} (refl x) y ≡ y
tr/refl-at x y = refl y

tr/∙ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {x₀ x₁ x₂ : X} (p₀ : x₀ ≡ x₁) (p₁ : x₁ ≡ x₂)
     → tr {Y = Y} (p₀ ∙ p₁) ≡ (tr {Y = Y} p₁) ∘ (tr {Y = Y} p₀)
tr/∙ {X = X} {Y} {x₀} {x₁} {x₂} p₁ p₂ =
  𝕁 X (λ x₀ x₁ p₁ → (x₂ : X) → (p₂ : x₁ ≡ x₂) → tr{Y = Y} (p₁ ∙ p₂) ≡ (tr {Y = Y} p₂) ∘ (tr {Y = Y} p₁))
    (λ x → λ x₂ p₂ → refl _)
    x₀ x₁ p₁ x₂ p₂

tr/⁻¹ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {x₀ x₁ : X} (p : x₀ ≡ x₁)
      → tr {Y = Y} p ∘ tr {Y = Y} (p ⁻¹) ≡ id 
tr/⁻¹ {X = X} {Y} {x₀} {x₁} p =  
  𝕁 X (λ x₀ x₁ p → tr {Y = Y} p ∘ tr {Y = Y} (p ⁻¹) ≡ id )
    (λ x → refl _)
    x₀ x₁ p

tr/right-⁻¹ = tr/⁻¹

tr/left-⁻¹ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {x₀ x₁ : X} (p : x₀ ≡ x₁)
      → tr {Y = Y} (p ⁻¹) ∘ tr {Y = Y} p ≡ id 
tr/left-⁻¹ {X = X} {Y} {x₀} {x₁} p =  
  𝕁 X (λ x₀ x₁ p → tr {Y = Y} (p ⁻¹) ∘ tr {Y = Y} p ≡ id )
    (λ x → refl _)
    x₀ x₁ p


ap : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {x₀ x₁ : X} (f : X → Y) (p : x₀ ≡ x₁) → f x₀ ≡ f x₁
ap {X = X} {Y} {x₀} {x₁} f p = 𝕁 X (λ x₀ x₁ p → f x₀ ≡ f x₁) (λ x → refl (f x)) x₀ x₁ p

ap/refl : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} (x : X) (f : X → Y) → ap f (refl x) ≡ refl (f x)
ap/refl {X = X} {Y} x f = refl (refl (f x))

ap/∙  : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {x₀ x₁ x₂ : X}
      → (f : X → Y) (p₀ : x₀ ≡ x₁) (p₁ : x₁ ≡ x₂)
      → ap f (p₀ ∙ p₁) ≡ (ap f p₀) ∙ (ap f p₁)
ap/∙ {X = X} {Y} {x₀} {x₁} {x₂} f p₀ p₁ =
  𝕁'
    { A = λ x₀ x₁ p₀ → (x₂ : X) → (p₁ : x₁ ≡ x₂) → ap f (p₀ ∙ p₁) ≡ (ap f p₀) ∙ (ap f p₁) }
    ( λ x (x₂ : X) (p₁ : x ≡ x₂) → refl (ap f p₁))
    x₀ x₁ p₀ x₂ p₁

ap/⁻¹ : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {x₀ x₁ : X} (f : X → Y) (p : x₀ ≡ x₁) → ap f (p ⁻¹) ≡ (ap f p) ⁻¹
ap/⁻¹ {X = X} {Y} {x₀} {x₁} f p = 𝕁 X (λ x₀ x₁ p → ap f (p ⁻¹) ≡ ap f p ⁻¹) (λ x → refl (refl (f x))) x₀ x₁ p

ap/∘ : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {Z : 𝓤 ℓ''} {x₀ x₁ : X}
     → (f : X → Y) (g : Y → Z) (p : x₀ ≡ x₁)
     → ap (g ∘ f) p ≡ (ap g (ap f p))
ap/∘ {X = X} {x₀ = x₀} {x₁} f g p =  𝕁 X (λ x₀ x₁ p → ap (g ∘ f) p ≡ ap g (ap f p)) (λ x → refl (refl (g (f x)))) x₀ x₁ p

ap/id : {X : 𝓤 ℓ}  {x₀ x₁ : X}
      → (p : x₀ ≡ x₁)
      → ap id p ≡ p
ap/id {X = X} {x₀} {x₁} p = 𝕁 X (λ x₀ x₁ p → ap id p ≡ p) (λ x → refl (refl x)) x₀ x₁ p

apd : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {x₀ x₁ : X} (f : (x : X) → Y x) (p : x₀ ≡ x₁)
    → tr {Y = Y} p (f x₀) ≡ f x₁
apd {X = X} {Y} {x₀} {x₁} f p =  𝕁 X (λ x₀ x₁ p → tr {Y = Y} p (f x₀) ≡ f x₁) (λ x → refl (f x)) x₀ x₁ p

apd/refl : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} (x : X) (f : (x : X) → Y x) → apd {X = X} {Y} {x} {x} f (refl x) ≡ refl (f x)
apd/refl {X = X} {Y} x f = refl _

-- p. 99
-- Lemma 2.3.10
tr/comp : {X : 𝓤 ℓ} {X' : 𝓤 ℓ'} {Y : X' → 𝓤 ℓ''} {x₀ x₁ : X} (f : X → X') (p : x₀ ≡ x₁)
        → tr {Y = Y ∘ f} p ≡ tr {Y = Y} (ap f p) 
tr/comp {X = X} {X'} {Y} {x₀} {x₁} f p = 𝕁 X (λ x₀ x₁ p → tr {Y = Y ∘ f} p ≡ tr {Y = Y} (ap f p)) (λ x → refl _) x₀ x₁ p

tr/ap : {X : 𝓤 ℓ} {X' : 𝓤 ℓ'} {Y : X' → 𝓤  ℓ''} {x₀ x₁ : X}
      → (f : X → X') (p : x₀ ≡ x₁)
      → tr {Y = Y} (ap f p) ≡ tr {Y = Y ∘ f} p
tr/ap  {X = X} {X'} {Y} {x₀} {x₁} f p =
  𝕁
    X
    (λ x₀ x₁ p →  tr {Y = Y} (ap f p) ≡ tr {Y = Y ∘ f} p)
    (λ x → refl _)
    x₀ x₁ p

tr/tr : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
      → {X' : 𝓤 ℓ} {Y' : X' → 𝓤 ℓ'}
      → (f : X → X') → (g : (x : X) → (Y x) → Y' (f x))
      → {x₀ : X} {y : Y x₀} {x₁ : X} 
      → (e : x₀ ≡ x₁)
      → tr e (g x₀ y) ≡ g x₁ (tr e y)
tr/tr {X = X} {Y} {X'} {Y'} f g {x} {y} {x} (refl x) =
   tr {Y = λ x → Y' (f x)} (refl x) (g x y) ≡⟨ refl _ ⟩
   g x y                                    ≡⟨ refl _ ⟩
   g x (tr  {Y = λ x → Y x} (refl x) y)     ∎

tr/ap-at : {X : 𝓤 ℓ} {X' : 𝓤 ℓ'} {Y : X' → 𝓤  ℓ''} {x₀ x₁ : X}
      → (f : X → X') (p : x₀ ≡ x₁) (y : Y (f x₀))
      → tr {Y = Y} (ap f p) y ≡ tr {Y = Y ∘ f} p y
tr/ap-at   {X = X} {X'} {Y} {x₀} {x₁} f p y = ap (λ - → - y) (tr/ap f p)


-- p.99
-- Lemma 2.3.11
tr/family : {X : 𝓤 ℓ} {Y Z : X → 𝓤 ℓ'} {x₀ x₁ : X}
          → (f : (x : X) → Y x → Z x) (p : x₀ ≡ x₁)
          → (tr {Y = Z} p) ∘ (f x₀) ≡ (f x₁) ∘ (tr {Y = Y} p)
tr/family {X = X} {Y} {Z} {x₀} {x₁} f p = 𝕁 X (λ x₀ x₁ p →  (tr {Y = Z} p) ∘ (f x₀) ≡ (f x₁) ∘ (tr {Y = Y} p)) (λ x → refl _) x₀ x₁ p


tr/const : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {x₀ x₁ : X} (p : x₀ ≡ x₁) → (tr {X = X} {Y = λ - → Y} p) ≡ (𝑖𝑑 Y)
tr/const {X = X} {Y} {x₀} {x₁} p =
  𝕁 X
    ( λ x₀ x₁ p →  tr {Y = λ - → Y} p ≡ id )
    ( λ x → refl id )
    x₀ x₁ p

tr/const-at : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {x₀ x₁ : X} (p : x₀ ≡ x₁) (y : Y) → tr {X = X} {Y = λ - → Y} p y ≡ y
tr/const-at {X = X} {Y} {x₀} {x₁} p y = ap (λ - → - y) (tr/const p)

-- p. 98 of the HoTT book, what should I call these ?

tr/const-eq-left : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {x₀ x₁ : X}
               → (f : X → Y) → (p : x₀ ≡ x₁)
               → f x₀ ≡ f x₁ → tr p (f x₀) ≡ f x₁
tr/const-eq-left {X = X} {Y = Y} {x₀} {x₁} f p e = (tr/const-at p (f x₀)) ∙ e

tr/const-eq-right : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {x₀ x₁ : X}
               → (f : X → Y) → (p : x₀ ≡ x₁)
               → tr p (f x₀) ≡ f x₁ → f x₀ ≡ f x₁ 
tr/const-eq-right {X = X} {Y = Y} {x₀} {x₁} f p e = (tr/const-at p (f x₀))⁻¹ ∙ e


-- p. 98
-- Lemma 2.3.8
apd/const : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {x₀ x₁ : X}
          → (f : X → Y) → (p : x₀ ≡ x₁)
          → apd {Y = λ - → Y} f p ≡ tr/const-at p (f x₀) ∙ ap f p
apd/const {X = X} {Y} {x₀} {x₁} f p = 𝕁 X (λ x₀ x₁ p → apd {Y = λ - → Y} f p ≡ tr/const-at p (f x₀) ∙ ap f p) (λ x → refl _) x₀ x₁ p -- hahaha everything holds definitionally






liftpath : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {x₀ x₁ : X}
     → (p : x₀ ≡ x₁) (u₀ : Y x₀)
     → Id (Σ Y) (x₀ , u₀) (x₁ , tr p u₀) -- we don't simply write `blah ≡ bleh` because then agda doesn't know the type we mean
liftpath {X = X} {Y} {x₀} {x₁} p =
  𝕁
    X
    ( λ x₀ x₁ p → (u₀ : Y x₀) →  (x₀ , u₀) ≡ (x₁ , tr p u₀) )
    ( λ x u → refl _)
    x₀ x₁ p

liftpath/refl : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {x₀ : X}
          → (u₀ : Y x₀)
          → liftpath {X = X} {Y} {x₀} (refl x₀) u₀ ≡ refl (x₀ , u₀)
liftpath/refl u₀ = refl _ 

liftpath/fst : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {x₀ x₁ : X}
         → (p : x₀ ≡ x₁) (u₀ : Y x₀)
         → ap fst (liftpath {X = X} {Y} {x₀} {x₁} p u₀) ≡ p
liftpath/fst {X = X} {Y} {x₀} {x₁} p =
  𝕁
    X
    ( λ x₀ x₁ p → (u₀ : Y x₀) →  ap fst (liftpath p u₀) ≡ p )
    ( λ x u → refl _)
    x₀ x₁ p



-- Whiskering

_∙r_ : {X : 𝓤 ℓ} → {x₀ x₁ x₂ : X} → {p q : x₀ ≡ x₁}
     → (α : p ≡ q) → (r : x₁ ≡ x₂)
     → p ∙ r ≡ q ∙ r
_∙r_ {X = X} {x₀} {x₁} {x₂} {p} {q} α r =
  𝕁
    X
    (λ (x₁ x₂ : X) (r : x₁ ≡ x₂) → (x₀ : X) → (p q : x₀ ≡ x₁) → (α : p ≡ q) → p ∙ r ≡ q ∙ r)
    (λ (x : X) x₀ p q α → (  p ∙ (refl x) ≡⟨  ∙/right-refl p ⟩
                            p            ≡⟨ α ⟩
                            q            ≡⟨  (∙/right-refl q) ⁻¹ ⟩
                            q ∙ (refl x) ∎
                          )
    )
    x₁ x₂ r x₀ p q α

_∙l_ : {X : 𝓤 ℓ} → {x₀ x₁ x₂ : X} → {p q : x₁ ≡ x₂}
     → (r : x₀ ≡ x₁) → (α : p ≡ q)
     → r ∙ p ≡ r ∙ q
_∙l_ {X = X} {x₀} {x₁} {x₂} {p} {q} r α =
  𝕁
    X
    ( λ (x₀ x₁ : X) (r : x₀ ≡ x₁) → (x₂ : X) → (p q : x₁ ≡ x₂) → (α : p ≡ q) → r ∙ p ≡ r ∙ q)
    ( λ (x : X) x₂ p q α → ( (refl x) ∙ p ≡⟨ ∙/left-refl p ⟩
                             p            ≡⟨ α ⟩
                             q            ≡⟨ (∙/left-refl q) ⁻¹ ⟩
                             (refl x) ∙ q ∎
                           )
    )
    x₀ x₁ r x₂ p q α


-- homotopy

_∼_ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} (f g : Π Y) → 𝓤 (ℓ ⊔ ℓ')
_∼_ {X = X} f g = Π (λ (x : X) → f x ≡ g x)




-- Equality for functions (aka homotopy)

_≡Π_ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
     → (f₀ f₁ : Π Y) → 𝓤 (ℓ ⊔ ℓ')
f₀ ≡Π f₁ = f₀ ∼ f₁ 

≡Π/refl : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
        → (f : Π Y)
        → f ≡Π f
≡Π/refl f = λ x → refl (f x)

≡Π/symm : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
        → {f₀ f₁ : Π Y}
        → f₀ ≡Π f₁ → f₁ ≡Π f₀
≡Π/symm e = λ x → (e x) ⁻¹

≡Π/tran : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
        → {f₀ f₁ f₂ : Π Y}
        → f₀ ≡Π f₁ → f₁ ≡Π f₂ → f₀ ≡Π f₂
≡Π/tran e₀ e₁ = λ x → (e₀ x) ∙ (e₁ x)

≡Π/natural : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'}
           → (f₀ f₁ : X → Y) → (H : f₀ ≡Π f₁)
           → {x₀ x₁ : X}
           → (p : x₀ ≡ x₁)
           → H x₀ ∙ (ap f₁ p) ≡ (ap f₀ p) ∙ H x₁

≡Π/natural {X = X} f₀ f₁ H {x₀} {x₁} p =
  𝕁
    X
    ( λ x₀ x₁ p →  H x₀ ∙ (ap f₁ p) ≡ (ap f₀ p) ∙ H x₁ )
    ( λ x →   H x ∙ ap f₁ (refl x) ≡⟨ refl _ ⟩
               H x ∙ refl (f₁ x) ≡⟨ ∙/right-refl _ ⟩
               H x ≡⟨  ∙/left-refl _ ⟩
               refl (f₀ x) ∙ H x ≡⟨ refl _ ⟩
               ap f₀ (refl x) ∙ H x ∎
               )
    x₀ x₁ p

∼/natural = ≡Π/natural

∼/natural' : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'}
           → (f₀ f₁ : X → Y) → (H : f₀ ≡Π f₁)
           → {x₀ x₁ : X}
           → (p : x₀ ≡ x₁)
           → (ap f₁ p) ≡ (H x₀) ⁻¹ ∙  (ap f₀ p) ∙ H x₁
∼/natural' f₀ f₁ H {x₀} {x₁} p =
  (ap f₁ p)                          ≡⟨ ∙/left-refl (ap f₁ p) ⟩
  (refl _) ∙ (ap f₁ p)               ≡⟨ ap (λ - → - ∙  (ap f₁ p)) ( (∙/left-⁻¹ (H x₀)) ⁻¹)  ⟩
  (H x₀) ⁻¹ ∙ (H x₀) ∙ (ap f₁ p)     ≡⟨ ∙/assoc (H x₀ ⁻¹) (H x₀) (ap f₁ p) ⟩
  (H x₀) ⁻¹ ∙ ( (H x₀) ∙ (ap f₁ p) ) ≡⟨ ap (λ - → (H x₀ ⁻¹) ∙ -) (∼/natural f₀ f₁ H p) ⟩
  (H x₀) ⁻¹ ∙ ( (ap f₀ p) ∙ H x₁ )   ≡⟨ ∙/assoc (H x₀ ⁻¹) (ap f₀ p) (H x₁) ⁻¹ ⟩
  (H x₀) ⁻¹ ∙ (ap f₀ p) ∙ H x₁       ∎

-- p. 100
-- Corollary 2.4.4
postulate ≡Π/natural-cor : {X : 𝓤 ℓ}
           → (f : X → X) → (H : f ≡Π id) → (x : X)
           → H  (f x) ≡ ap f (H x)
-- Too long a chain of equalities to bother


