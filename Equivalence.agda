-- {-# OPTIONS --without-K --exact-split --safe --allow-unsolved-metas #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`


module Equivalence where

open import Basics public

-- quasi-inverses

qinv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} (f : A → B) → 𝓤 (ℓ ⊔ ℓ')
qinv {A = A} {B = B} f = Σ (λ (g : B → A) → (f ∘ g ∼ id) × (g ∘ f ∼ id))

-- inv = inverse
qinv#inv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B} → (G : qinv f) → (B → A)
qinv#inv {A = A} {B = B} G = fst G
qinv#g = qinv#inv

-- sec = section
qinv#sec : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B} → (G : qinv f) → ((qinv#inv G) ∘ f ∼ id)
qinv#sec {A = A} {B = B} G = snd (snd G)
qinv#β = qinv#sec 

-- ret = retract
qinv#ret : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B} → (G : qinv f) → (f ∘ (qinv#inv G) ∼ id)
qinv#ret {A = A} {B = B} G = fst (snd G)
qinv#α = qinv#ret

-- some examples

id/qinv : (A : 𝓤 ℓ) → qinv (𝑖𝑑 A)
id/qinv A = id , refl , refl 

qinv/symm : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B}
          → (G : qinv f)
          → (qinv (qinv#inv G))
qinv/symm {f = f} G = (f , qinv#β G , qinv#α G)

qinv/tran : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {C : 𝓤 ℓ''} {f : A → B} {f' : B → C}
          → (G : qinv f) → (G' : qinv f')
          → (qinv (f' ∘ f))
qinv/tran {f = f} {f' = f'} G G' = (g'' , α'' , β'' )
 where
  g = qinv#g G 
  α = qinv#α G
  β = qinv#β G
  g' = qinv#g G' 
  α' = qinv#α G'
  β' = qinv#β G'

  f'' = f' ∘ f
  g'' = g ∘ g'
  β'' : g'' ∘ f'' ∼ id
  β'' x =   g'' (f'' x) ≡⟨ ap g ( β' (f x)) ⟩
            g (f x)     ≡⟨ β x ⟩
            x           ∎
  α'' : f'' ∘ g'' ∼ id
  α'' y = f'' (g'' y) ≡⟨ ap f' (α (g' y)) ⟩
           f' (g' y)   ≡⟨ α' y ⟩
           y           ∎

  


left-∙/qinv : {A : 𝓤 ℓ} {x₀ x₁ : A}
             → (p : x₀ ≡ x₁) → (x₂ : A)
             → qinv {A = x₁ ≡ x₂} {B = x₀ ≡ x₂} (p ∙_) 
left-∙/qinv {A = A} {x₀} {x₁} p x₂ =
  ( ( p ⁻¹ ∙_ )
  , ( λ q →  ∙/assoc p (p ⁻¹) q ⁻¹ ∙  ap ( _∙ q) (∙/right-⁻¹ p) )
  , ( λ r →  ∙/assoc (p ⁻¹) p r ⁻¹ ∙  ap ( _∙ r) (∙/left-⁻¹ p) )
  )

right-∙/qinv : {A : 𝓤 ℓ} {x₁ x₂ : A}
             → (p : x₁ ≡ x₂) → (x₀ : A)
             → qinv {A = x₀ ≡ x₁} {B = x₀ ≡ x₂} (_∙ p) 
right-∙/qinv {A = A} {x₁} {x₂} p x₀ =
  ( ( _∙ p ⁻¹ )
  , ( λ q →  ∙/assoc q (p ⁻¹) p ∙ (ap (q ∙_) (∙/left-⁻¹ p)) ∙ (∙/right-refl q) )
  , ( λ r →   ∙/assoc r p (p ⁻¹) ∙ (ap (r ∙_) (∙/right-⁻¹ p)) ∙ (∙/right-refl r) )
  )

tr/qinv : {A : 𝓤 ℓ} {B : A → 𝓤 ℓ'} {x₀ x₁ : A}
        → (p : x₀ ≡ x₁)
        → qinv (tr {Y = B} p)
tr/qinv {A = A} {B = B} {x₀} {x₁} p =
  ( tr (p ⁻¹)
  , (λ x → ap (λ - → - x) (  tr/right-⁻¹ p  ) )
  , λ x → ap (λ - → - x) (tr/left-⁻¹ p) 
  )



-- bi-invertible maps
-- we take those as equivalences


isbiinvt : {A : 𝓤 ℓ}
           {B : 𝓤 ℓ'}
           → (f : A → B)
           → 𝓤 (ℓ ⊔ ℓ')
isbiinvt {A = A} {B = B} f = Σ (λ (g : B → A) → f ∘ g ∼ id) × Σ (λ (h : B → A) → h ∘ f ∼ id)

isequiv = isbiinvt

isbiinvt#rinv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B}
              → (b : isbiinvt f)
              → B → A
isbiinvt#rinv b = fst (fst b)
isbiinvt#g = isbiinvt#rinv

isbiinvt#linv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B}
              → (b : isbiinvt f)
              → B → A
isbiinvt#linv b = fst (snd b)
isbiinvt#h = isbiinvt#linv

isbiinvt#α : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B}
           → (b : isbiinvt f)
           → f ∘ (isbiinvt#rinv b) ∼ id
isbiinvt#α b = snd (fst b)

isbiinvt#β : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B}
           → (b : isbiinvt f)
           → (isbiinvt#linv b) ∘ f ∼ id
isbiinvt#β b = snd (snd b)


qinv→isbiinvt : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B}
              → qinv f
              → isbiinvt f
qinv→isbiinvt q = ( qinv#inv  q , qinv#α q) , (qinv#inv q , qinv#β q)

isbiinvt→qinv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B}
              → isbiinvt f
              → qinv f
isbiinvt→qinv {f = f} b = g , α , β'
 where
  g = isbiinvt#g b
  h = isbiinvt#h b
  α = isbiinvt#α b
  β = isbiinvt#β b
  β' : g ∘ f ∼ id
  β' x =  g (f x) ≡⟨ (β (g (f x))) ⁻¹ ⟩
          h (f (g (f x))) ≡⟨ ap (λ - → h - ) (α (f x)) ⟩
          h (f x) ≡⟨ β x ⟩
          x  ∎




-- equivalence

_≃_ : (A : 𝓤 ℓ) → (B : 𝓤 ℓ') → 𝓤 (ℓ ⊔ ℓ')
_≃_ A B = Σ (λ (f : A → B) → isequiv f)

≃/refl : (A : 𝓤 ℓ) → A ≃ A
≃/refl A = id , ( (id , refl) , (id , refl) )

≃/symm : {A : 𝓤 ℓ} → {B : 𝓤 ℓ'}
       → A ≃ B → B ≃ A
≃/symm {A = A} {B = B} e =  g , E⁻¹
 where
  f : A → B
  f = fst e
  E : isbiinvt f
  E = snd e

  Q : qinv f
  Q = isbiinvt→qinv E
  g : B → A
  g = qinv#g Q
  Q⁻¹ = qinv/symm Q
  E⁻¹ = qinv→isbiinvt Q⁻¹

≃/symm/refl : (A : 𝓤 ℓ) → ≃/symm (≃/refl A) ≡ ≃/refl A
≃/symm/refl A = refl _


-- Could be better to define the composition of biinvt by hand without the back/forth with qinv ? 
≃/tran : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {C : 𝓤 ℓ''}
       → A ≃ B → B ≃ C → A ≃ C
≃/tran {A = A} {B = B} {C = C} e e' = f'' , E''
 where
  f = fst e
  E = snd e

  f' = fst e'
  E' = snd e'

  Q = isbiinvt→qinv E
  Q' = isbiinvt→qinv E'

  f'' = f' ∘ f
  E'' = qinv→isbiinvt (qinv/tran Q Q')

≃/tran/refl-refl : (A : 𝓤 ℓ)  → ≃/tran (≃/refl A) (≃/refl A) ≡ (≃/refl A)
≃/tran/refl-refl A = refl _


_≃∙_ :  {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {Z : 𝓤 ℓ''} → X ≃ Y → Y ≃ Z → X ≃ Z
p ≃∙ q = ≃/tran p q 
infixr  0 _≃∙_

_≃⁻¹ :  {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {Z : 𝓤 ℓ''} → X ≃ Y → Y ≃ X
p ≃⁻¹ = ≃/symm p 
_≃⟨_⟩_ : (X : 𝓤 ℓ) {Y : 𝓤 ℓ'} {Z : 𝓤 ℓ''} → X ≃ Y → Y ≃ Z → X ≃ Z
X ≃⟨ p ⟩ q = p ≃∙  q
infixr  0 _≃⟨_⟩_

_≃∎ : (X : 𝓤 ℓ) → X ≃ X
x ≃∎ = ≃/refl x
infix   1 _≃∎


left-∙/≃ : {A : 𝓤 ℓ} {x₀ x₁ : A}
          → (p : x₀ ≡ x₁) → (x₂ : A)
          → (x₁ ≡ x₂) ≃ (x₀ ≡ x₂)  
left-∙/≃ {A = A} {x₀} {x₁} p x₂ = ((p ∙_) , qinv→isbiinvt (left-∙/qinv p x₂))

right-∙/≃ : {A : 𝓤 ℓ} {x₁ x₂ : A}
             → (p : x₁ ≡ x₂) → (x₀ : A)
             → (x₀ ≡ x₁) ≃ (x₀ ≡ x₂) 
right-∙/≃ {A = A} {x₁} {x₂} p x₀ = ((_∙ p) , qinv→isbiinvt (right-∙/qinv p x₀))

-- We can now prove that lift/lower yields an equivalence:
-- Taken from Escardo


lower/isbiinvt : (X : 𝓤 ℓ) → isbiinvt (lower {ℓ} {ℓ'} {X})
lower/isbiinvt {ℓ} {ℓ'} X = qinv→isbiinvt Q
 where
  Q : qinv {A = Lift ℓ' X} {B = X} (lower {ℓ} {ℓ'} {X})
  Q = (lift , lower-lift {ℓ} {ℓ'} , lift-lower )

lift/isbiinvt : (X : 𝓤 ℓ) → isbiinvt (lift {ℓ} {ℓ'} {X})
lift/isbiinvt {ℓ} {ℓ'} X = qinv→isbiinvt Q
 where
  Q : qinv {A = X} {B = Lift ℓ' X} (lift {ℓ} {ℓ'})
  Q = (lower , lift-lower , lower-lift {ℓ} {ℓ'} )

Lift/≃ : (ℓ' : Level) {X : 𝓤 ℓ} → (X ≃ Lift ℓ' X)
Lift/≃ {ℓ} ℓ' {X = X} = (lift , (lower , lift-lower) , (lower , lower-lift {ℓ} {ℓ'} {X = X}))

