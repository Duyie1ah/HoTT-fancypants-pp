-- {-# OPTIONS --without-K --exact-split --safe --allow-unsolved-metas #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`



module HLevels where

open import Basics public
open import Equivalence public
open import Structure-Of-Type-Formers public
open import More-Equivalence public


-- levels

ishLevel : (n : ℕ) → (A : 𝓤 ℓ) → 𝓤 ℓ
ishLevel {ℓ} =
  ℕ-induction
    ( λ n → 𝓤 ℓ → 𝓤 ℓ )
    ( λ A → isCtr A )
    ( λ n ishLeveln A → (x₀ x₁ : A) → ishLeveln (x₀ ≡ x₁))

isLvl = ishLevel
isLvl0 isLvl1 isLvl2 : (A : 𝓤 ℓ) → 𝓤 ℓ
isLvl0 = isLvl zero
isLvl1 = isLvl one
isLvl2 = isLvl two

isCtr→isLvl0 : {A : 𝓤 ℓ} → isCtr A → isLvl0 A
isCtr→isLvl0 = id
isLvl0→isCtr : {A : 𝓤 ℓ}→ isLvl0 A → isCtr A
isLvl0→isCtr = id

isProp→isLvl1 : {A : 𝓤 ℓ} → isProp A → isLvl1 A
isProp→isLvl1 {A = A} aa a₀ a₁ = center , centrality
 where
  center = ((aa a₀ a₀) ⁻¹ ∙ (aa a₀ a₁))
  centrality =
    𝕁
      A
      ( λ a₀ a₁ e → (aa a₀ a₀) ⁻¹ ∙ (aa a₀ a₁) ≡  e )
      ( λ a → ∙/left-⁻¹ (aa a a) )
      a₀ a₁ 

isLvl1→isProp : {A : 𝓤 ℓ} → isLvl1 A → isProp A 
isLvl1→isProp {A = A} L = λ x₀ x₁ → isCtr#center (L x₀ x₁)

isSet→isLvl2 : {A : 𝓤 ℓ} → isSet A → isLvl2 A
isSet→isLvl2 {A = A} L a₀ a₁ = isProp→isLvl1 (L a₀ a₁)
isLvl2→isSet : {A : 𝓤 ℓ} → isLvl2 A → isSet A
isLvl2→isSet {A = A} L a₀ a₁ = isLvl1→isProp (L a₀ a₁)

isLvl/succ : {A : 𝓤 ℓ} (n : ℕ) → isLvl n A → isLvl (succ n) A
isLvl/succ {A = A} zero (a , aa) a₀ a₁ = center , centrality
 where
  center = ((aa a₀) ⁻¹ ∙ (aa a₁))
  centrality =
    𝕁
      A
      ( λ a₀ a₁ e → (aa a₀) ⁻¹ ∙ (aa a₁) ≡  e )
      ( λ a → ∙/left-⁻¹ (aa a) )
      a₀ a₁
{-
   Assume A is of level zero, i.e. contractible.
   We need to show that A is of level one, i.e. its equality types are contractible.
   We want to use 𝕁.
   We want to show that for any a₀ a₁ and e : a₀ ≡ a₁, e is equal to _something_ (the center of contractibility).
   The only path that makes sense is (aa a₀) ⁻¹ ∙ (aa a₁) (or some other permutation of this).
   In the case where `a₀ = a = a₁` and `e = refl a`, everything collapses nicely.
-}

isLvl/succ {A = A} (succ n) L a₀ a₁ = isLvl/succ n (L a₀ a₁)
{-
   Assume A is of level (succ n).
   Assume (Induction hypothesis) that for all B of level n, we have that B is of level (succ n).
   To show A of level (succ succ n), we need to show that any `a₀ ≡ a₁` is of level (succ n).
   Fix a₀ and a₁; since A is of level (succ n), by definition, a₀ ≡ a₁ is of level n.
   By the IH, a₀ ≡ a₁ is of level (succ n), which means by definition that A is of level succ succ n.
-}



isLvl-isProp : (A : 𝓤 ℓ) (n : ℕ) → isProp (isLvl n A)
isLvl-isProp A (zero) = isCtr-isProp A
isLvl-isProp A (succ n) = isProp/Π₂ (λ a₀ a₁ → isLvl-isProp (a₀ ≡ a₁) n)

isLvl/≃ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
        → (n : ℕ) → (isLvl n B)
        → (A ≃ B)
        → isLvl n A
isLvl/≃ {A = A} {B = B} zero BC E = isCtr/≃ BC E
isLvl/≃ {A = A} {B = B} (succ n) BL (f , e) a₀ a₁ = isLvl/≃ n (BL (f a₀) (f a₁)) (ap/≃ ((f , e)) a₀ a₁) 

isLvl/Σ : {A : 𝓤 ℓ} {B : A → 𝓤 ℓ'}
        → (n : ℕ) → (isLvl n A) → ((a : A) → isLvl n (B a))
        → isLvl n (Σ B)
isLvl/Σ {A = A} {B = B} zero ACtr BCtr = center , centrality
 where
  a = fst ACtr
  aa = snd ACtr
  
  center : Σ B
  center = a , fst (BCtr a)

  centrality : (p : Σ B) → center ≡ p
  centrality (a' , b') = ≡Σ→≡ (aa a' , ((snd (BCtr a') _) ⁻¹ ∙ (snd (BCtr a') b')) )

isLvl/Σ {A = A} {B = B} (succ n) AL BL (a₀ , b₀) (a₁ , b₁) = isLvl/≃ n (isLvl/Σ n (AL a₀ a₁) λ p → BL a₁ _ _ ) ≡→≡Σ-≃

isLvl/Π : {A : 𝓤 ℓ} {B : A → 𝓤 ℓ'}
        → (n : ℕ) → ((a : A) → isLvl n (B a))
        → isLvl n (Π B)
isLvl/Π {A = A} {B = B} zero BC = isCtr/Π BC
isLvl/Π {A = A} {B = B} (succ n) BL f₀ f₁ = isLvl/≃ n (isLvl/Π n (λ a → BL a _ _)) funext-≃ 
-- To show that Π B is of level n+1, we need to show that f₀ ≡ f₁ is of level n.
-- This type is equivalent to f₀ ∼ f₁ = Π[ a ] f₀ a ≡ f₁ a.
-- Since B is of level n+1, f₀ a ≡ f₁ a is of level n for all a, hence so is Π[ a ] f₀ a ≡ f₁ a by induction



isLvl/embedding : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
                → {f : A → B}
                → embedding f
                → {n : ℕ}
                → isLvl (succ n) B
                → isLvl (succ n) A
isLvl/embedding {A = A} {B} {f} emb {n} BL a₀ a₁ = isLvl/≃ n (BL (f a₀) (f a₁)) (ap f , emb a₀ a₁) 

HLevel : ℕ → 𝓤 (ℓ ₊)
HLevel {ℓ} n = Σ (λ (A : 𝓤 ℓ) → isLvl n A)




isCtr→≃-isCtr : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
              → isCtr A → isCtr B
              → isCtr (A ≃ B)
isCtr→≃-isCtr {A = A} {B = B} (a , aa) (b , bb) = (f , ef) , centrality
 where
  f : A → B
  f - = b
  g : B → A
  g - = a
  α : f ∘ g ∼ id
  α - = bb _ 
  β : g ∘ f ∼ id
  β - = aa _
  ef = (g , α) , (g , β)


  -- Yes, there are fancy abstract arguments but gluing everything together is more trouble than bruteforcing it here
  centrality : (E : A ≃ B) → (f , ef) ≡ E
  centrality (f' , ef') = ≡Σ→≡ ((funext λ x → bb (f x) ⁻¹ ∙ bb (f' x)) , isbiinvt-isProp {f = _} _ _)


isLvl→≃-isLvl : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
              → (n : ℕ)
              → isLvl n A → isLvl n B
              → isLvl n (A ≃ B)
isLvl→≃-isLvl {A = A} {B = B} zero AC BC = isCtr→≃-isCtr AC BC
isLvl→≃-isLvl {A = A} {B = B} (succ n) AL BL (f₀ , e₀) (f₁ , e₁) = isLvl/≃ n f₀∼f₁-Lvl lol 
 where
  
  lol = ((f₀ , e₀) ≡ (f₁ , e₁)) ≃⟨ Σ-Prop/≡-≃ (λ f → isbiinvt-isProp) (f₀ , e₀) (f₁ , e₁) ⟩
        (f₀ ≡ f₁)               ≃⟨ funext-≃ ⟩
        (f₀ ∼ f₁)               ≃∎

  f₀∼f₁-Lvl : isLvl n (f₀ ∼ f₁)
  f₀∼f₁-Lvl = isLvl/Π n (λ x → BL (f₀ x) (f₁ x))

              
HLevel-isLvlsucc : (n : ℕ) → isLvl (succ n) (HLevel {ℓ} n)
HLevel-isLvlsucc {ℓ} n (A , An) (B , Bn) = isLvl/≃ n (isLvl→≃-isLvl n An Bn) lol 
 where
  lol = ((A , An) ≡ (B , Bn)) ≃⟨  Σ-Prop/≡-≃ {A = 𝓤 ℓ} {B = λ C → isLvl n C} (λ C →  isLvl-isProp C n) (A , An) (B , Bn)  ⟩
        (A ≡ B)               ≃⟨ ≡-≃-≃ ⟩
        (A ≃ B)              ≃∎

