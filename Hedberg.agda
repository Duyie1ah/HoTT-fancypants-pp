-- {-# OPTIONS --without-K --exact-split --safe --allow-unsolved-metas #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`



module Hedberg where

open import Spartan-MLTT public
open import Basics public
open import Equivalence public
open import Structure-Of-Type-Formers public
open import Props-Etc public
open import Propositional-Resizing public
open import Logic public
open import More-Equivalence public
open import Nat-Arithmetic public


AxiomK : (A : 𝓤 ℓ) → 𝓤 ℓ
AxiomK A = (x : A) → (p : x ≡ x) → p ≡ refl x

isSet→K : {A : 𝓤 ℓ}
        → isSet A
        → AxiomK A
isSet→K AS x p = AS x x p (refl x)

K→isSet : {A : 𝓤 ℓ}
        → AxiomK A
        → isSet A
K→isSet {A = A} AK x₀ x₁ p₀ p₁ =
  𝕁
    A
    (λ x₀ x₁ p₀ → (p₁ : x₀ ≡ x₁) → p₀ ≡ p₁)
    (λ x p₁ → AK x p₁ ⁻¹)
    x₀ x₁ p₀ p₁


-- p. 284
-- Theorem 7.2.2
-- “Nice” means a reflexive mere relation implying identity.

module NiceRel→Set'
  {X     : 𝓤 ℓ}
  {R     : X → X → 𝓤 ℓ'}                 
  (Rprop : (x₀ x₁ : X) → isProp (R x₀ x₁))      -- R is a mere relation;
  (Rrefl : (x : X) → R x x)                     -- R is reflexive;
  (R→≡   : (x₀ x₁ : X) → (R x₀ x₁) → x₀ ≡ x₁)   -- R implies identity;
 where

  ≡→R : (x₀ x₁ : X) → x₀ ≡ x₁ → R x₀ x₁
  ≡→R = 𝕁' (λ x → Rrefl x)

  -- If X is a set, then R is equivalent to ≡
  Set→R-≃-≡ : isSet X → ((x₀ x₁ : X) → (R x₀ x₁) ≃ (x₀ ≡ x₁))
  Set→R-≃-≡ Xset x₀ x₁ =
    biimplied-Props→≃
      (Rprop x₀ x₁)
      (Xset x₀ x₁)
      (R→≡ x₀ x₁)
      (≡→R x₀ x₁)

  -- and conversely, if R is equivalent to ≡, then X is a set
  R-≃-≡→Set : ((x₀ x₁ : X) → (R x₀ x₁) ≃ (x₀ ≡ x₁)) → isSet X
  R-≃-≡→Set e x₀ x₁ = isProp/≃ (Rprop x₀ x₁) (≃/symm (e x₀ x₁))

  -- We show that `≡→R` is an equivalence
  -- and we actually need more tools than have covered until now …
  -- TODO

postulate
  NiceRel→Set :
      {X : 𝓤 ℓ}
      {R : X → X → 𝓤 ℓ'}
    → ((x₀ x₁ : X) → isProp (R x₀ x₁))
    → ((x : X) → R x x)
    → ((x₀ x₁ : X) → (R x₀ x₁) → x₀ ≡ x₁)
    → isSet X 


decidable-≡→isSet :
    {X : 𝓤 ℓ}
  → ((x₀ x₁ : X) → (x₀ ≡ x₁) + (x₀ ≢ x₁))
  → isSet X
decidable-≡→isSet {X = X} lem≡ = NiceRel→Set Rprop Rrefl dne≡
 where
  dne≡ : (x₀ x₁ : X) → ¬¬(x₀ ≡ x₁) → x₀ ≡ x₁
  dne≡ x₀ x₁ nn =  +-recursion id (λ n → !𝟘 _ (nn n)) (lem≡ x₀ x₁)

  R = λ (x₀ x₁ : X) → ¬¬ (x₀ ≡ x₁)

  Rprop : (x₀ x₁ : X) → isProp (R x₀ x₁)
  Rprop x₀ x₁ = isProp/¬ _

  Rrefl : (x : X) → R x x
  Rrefl x = λ - → - (refl x)

ℕ-isSet' : isSet ℕ
ℕ-isSet' = decidable-≡→isSet {X = ℕ} f
 where
  f : ((x₀ x₁ : ℕ) → (x₀ ≡ x₁) + (x₀ ≢ x₁))
  f =
    ℕ-induction
      ( λ (x₀ : ℕ) → (x₁ : ℕ) → (x₀ ≡ x₁) + (x₀ ≢ x₁))
      ( ℕ-induction
        ( λ (x₁ : ℕ) → (zero ≡ x₁) + (zero ≢ x₁) )
        ( inl (refl zero) )
        ( λ m IHm → inr (zero≢succ m) )
      )
      ( λ n IHn →
        ℕ-induction
          ( λ (x₁ : ℕ) → (succ n ≡ x₁) + (succ n ≢ x₁) )
          ( inr (λ p → zero≢succ n (p ⁻¹)))
           λ m IHm → +-recursion (λ e → inl (ap succ e)) (λ ne → inr (ne ∘ (s≡s→≡ n m))) (IHn m)
        
      )
  

  
