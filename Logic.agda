-- {-# OPTIONS --without-K --exact-split --safe --allow-unsolved-metas #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`



module Logic where

open import Spartan-MLTT public
open import Basics public
open import Equivalence public
open import Structure-Of-Type-Formers public
open import Props-Etc public
open import Propositional-Resizing public

DNE∞ : 𝓤 (ℓ ₊)
DNE∞ {ℓ} = (A : 𝓤 ℓ) →  ¬¬ A → A 

LEM∞ : 𝓤 (ℓ ₊)
LEM∞ {ℓ} = (A : 𝓤 ℓ) → A + ¬ A

DNE∞→LEM∞ : DNE∞ {ℓ} → LEM∞ {ℓ} 
DNE∞→LEM∞ dne A =  dne (A + ¬ A) (λ (f : ¬ (A + ¬ A)) → (f ∘ inr) (f ∘ inl))

LEM∞→DNE∞ : LEM∞ {ℓ} → DNE∞ {ℓ}
LEM∞→DNE∞ lem A nnA = +-induction (λ - → A) id (λ nA → !𝟘 _ (nnA nA)) (lem A)


-- Theorem 3.2.2
postulate not-DNE∞ : ¬ (DNE∞ {ℓ₀})
{- not-DNE∞ dne = {!!}
 where

  d = dne 𝔹

  s : 𝔹 → 𝔹
  s = 𝔹-recursion _ 𝕗 𝕥
  swap : 𝔹 ≃ 𝔹
  swap = (s , (s , 𝔹-induction _ (refl _) (refl _)) , (s , 𝔹-induction _ (refl _) (refl _)))

  sx≢x : (x : 𝔹) → s x ≢ x
  sx≢x = 𝔹-induction _ (𝕥≢𝕗 ∘ _⁻¹) (𝕥≢𝕗)

  swap≡ : 𝔹 ≡ 𝔹
  swap≡ = ua swap
  p = swap≡

  swap≡-≢-refl : swap≡ ≢ refl _
  swap≡-≢-refl e =
    𝕥≢𝕗
    (
     𝕥                          ≡⟨ refl _ ⟩
     s 𝕗                        ≡⟨ refl _ ⟩
     (fst swap) 𝕗               ≡⟨ ap (λ - → (fst -) 𝕗) ( ua-α swap ⁻¹) ⟩
     (fst (idtoeqv swap≡)) 𝕗    ≡⟨ ap (λ - → fst (idtoeqv - ) 𝕗) e ⟩
     (fst (idtoeqv (refl 𝔹))) 𝕗 ≡⟨ refl _  ⟩
     𝕗 ∎
   ) 

  𝔹≡𝔹-isnotProp : ¬ (isProp (𝔹 ≡ 𝔹))  
  𝔹≡𝔹-isnotProp = λ ϕ →  swap≡-≢-refl (ϕ swap≡ (refl _))  
 
  lol₁ : tr {Y = λ (A : 𝓤₀) → ¬¬ A → A} swap≡ d ≡ d
  lol₁ = apd dne p

  lol₂ : (u : ¬¬ 𝔹) → tr {Y = λ (A : 𝓤₀) → ¬¬ A → A} swap≡ d u ≡ d u
  lol₂ u = happly lol₁ u

  …
-}
-- The proof is relatively tedious.
-- A nicer (but slightly weaker?) argument as follows:
-- We show that ¬ LEM {lzero} which implies ¬ DNE {lzero ₊}
-- To show ¬ LEM {lzero} it suffices to exhibit one type in 𝓤₀ which is not decidable.
-- It is a named result (forgot the name) that a type is a set iff its path types are decidable.
-- We know already that 𝓤₀ is not a set (since the path type 𝔹 ≡ 𝔹 is not a prop), and the path types of 𝓤₀ lie in 𝓤₀, hence 𝓤₀ has a type which is not decidable (or rather, does not have all decidable path types) ?!?


¬-isProp : (A : 𝓤 ℓ) → isProp (¬ A)
¬-isProp A = isProp/Π (λ (x : A) → 𝟘-isProp)

isProp/¬ : (A : 𝓤 ℓ) → isProp (¬ A)
isProp/¬ A = isProp/Π (λ x → 𝟘-isProp)


isProp/em : {A : 𝓤 ℓ} → (isProp A) → isProp (A + ¬ A)
isProp/em {A = A} AP (inl a₀) (inl a₁) = ≡+→≡ (lift (AP a₀ a₁)) 
isProp/em {A = A} AP (inl a₀) (inr n₁) = !𝟘 _ (n₁ a₀) 
isProp/em {A = A} AP (inr n₀) (inr n₁) = ≡+→≡ (lift (¬-isProp A n₀ n₁))
isProp/em {A = A} AP (inr n₀) (inl a₁) = !𝟘 _ (n₀ a₁) 

-- Focusing on props

DNE : 𝓤 (ℓ ₊)
DNE {ℓ} = (A : 𝓤 ℓ) → isProp A → ¬¬ A → A 

LEM : 𝓤 (ℓ ₊)
LEM {ℓ} = (A : 𝓤 ℓ) → isProp A → A + ¬ A

DNE→LEM : DNE {ℓ} → LEM {ℓ} 
DNE→LEM dne A AP =  dne (A + ¬ A) (isProp/em AP) (λ (f : ¬ (A + ¬ A)) → (f ∘ inr) (f ∘ inl))

LEM→DNE : LEM {ℓ} → DNE {ℓ}
LEM→DNE lem A AP nnA = +-induction (λ - → A) id (λ nA → !𝟘 _ (nnA nA)) (lem A AP)


-- Logic of mere propositions

-- ?

-- AC

AC₀→  : 𝓤 (ℓ ₊ ⊔ ℓ' ₊ ⊔ ℓ'' ₊)
AC₀→ {ℓ} {ℓ'} {ℓ''} = (X : 𝓤 ℓ)
                    → (A : X → 𝓤 ℓ')
                    → (P : Π[ x ∈ X ] (A x → 𝓤 ℓ''))
                    → (isSet X)
                    → ((x : X) → isSet (A x))
                    → ((x : X) → (a : A x) → isProp (P x a))
                    →    Π[ x ∈ X ]  ∥ Σ[ a ∈ A x ] P x a     ∥
                    → ∥ Σ[ g ∈ Π A ]   Π[ x ∈ X ]   P x (g x) ∥


AC→ : 𝓤 (ℓ ₊ ⊔ ℓ' ₊)
AC→ {ℓ} {ℓ'} = (X : 𝓤 ℓ)
    → (Y : X → 𝓤 ℓ')
    → (isSet X)
    → ((x : X) → isSet (Y x))
    → Π (∥_∥ ∘ Y)
    → ∥ Π Y ∥

AC← : 𝓤 (ℓ ₊ ⊔ ℓ' ₊)
AC← {ℓ} {ℓ'} = (X : 𝓤 ℓ)
    → (Y : X → 𝓤 ℓ')
    → (isSet X)
    → ((x : X) → isSet (Y x))
    → ∥ Π Y ∥
    → Π (∥_∥ ∘ Y)

ac← : AC← {ℓ} {ℓ'} 
ac← X Y XS YS =
  ∥∥-recursion
    ( isProp/Π (λ y → ∥∥-isProp) )
    ( λ (f : Π Y) → λ y → ∣ f y ∣ )

AC-≃ : (X : 𝓤 ℓ)
     → (Y : X → 𝓤 ℓ')
     → (XS : isSet X)
     → (YS : (x : X) → isSet (Y x))
     → (ac→ : AC→ {ℓ} {ℓ'})
     → (∥ Π Y ∥) ≃ (Π (∥_∥ ∘ Y))
AC-≃ X Y XS YS ac→ =
  biimplied-Props→≃
    ( ∥∥-isProp )
    ( isProp/Π (λ y → ∥∥-isProp) )
    ( ac← X Y XS YS ) 
    ( ac→ X Y XS YS )





-- Corollary 3.9.2
-- The principle of unique choice

unique-choice : {A : 𝓤 ℓ} {P : A → 𝓤 ℓ'}
              → ((x : A) → isProp (P x))
              → ((x : A) → ∥ P x ∥)
              → Π P
unique-choice {A = A} {P} PProp Pnempty x = ∥∥-recursion (PProp x) id (Pnempty x)


