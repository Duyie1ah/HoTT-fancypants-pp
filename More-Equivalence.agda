-- {-# OPTIONS --without-K --exact-split --safe --allow-unsolved-metas #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`



module More-Equivalence where

open import Spartan-MLTT public
open import Basics public
open import Equivalence public
open import Structure-Of-Type-Formers public
open import Props-Etc public
open import Propositional-Resizing public
open import Logic public


linv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
     → (f : A → B)
     → 𝓤 (ℓ ⊔ ℓ')
linv {A = A} {B = B} f = Σ λ (g : B → A) → g ∘ f ∼ id
rinv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
     → (f : A → B)
     → 𝓤 (ℓ ⊔ ℓ')
rinv {A = A} {B = B} f = Σ λ (g : B → A) → f ∘ g ∼ id

qinv→postcomp-qinv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
                   → (f : A → B) → (qinv f)
                   → {C : 𝓤 ℓ''}
                   → (qinv λ (g : C → A) → f ∘ g)
qinv→postcomp-qinv {A = A} {B} f (f⁻¹ , α , β) {C} =
  ( (λ (h : C → B) → f⁻¹ ∘ h)
  , (λ g → ap (_∘ g) (funext α))
  , (λ h → ap (_∘ h) (funext β))
  )
  
qinv→precomp-qinv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
                   → (f : A → B) → (qinv f)
                   → {C : 𝓤 ℓ''}
                   → (qinv λ (g : B → C) → g ∘ f)
qinv→precomp-qinv {A = A} {B} f (f⁻¹ , α , β) {C} =
  ( (λ (h : A → C) → h ∘ f⁻¹)
  , (λ g → ap (g ∘_) (funext β))
  , (λ h → ap (h ∘_) (funext α))
  )


qinv→linv-isCtr : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
              → (f : A → B)
              → (q : qinv f)
              → isCtr (linv f)
     
qinv→linv-isCtr {A = A} {B = B} f (f⁻¹ , α , β) =
  isCtr/≃ (Σ≡-isCtr' f⁻¹) (Σ/fibers-≃ combined)
 where

  step1 : (g : B → A) → (g ∘ f ∼ id) ≃ (g ∘ f ≡ id)
  step1 g =  ≃/symm (funext-≃ {f₀ = g ∘ f} {f₁ = id})

  step2 : (g : B → A) → (g ∘ f ≡ id) ≃ (g ∘ f ∘ f⁻¹ ≡ f⁻¹)
  step2 g = ap/≃ (( _∘ f⁻¹ , qinv→isbiinvt (qinv→precomp-qinv f⁻¹ (f , β , α)))) (g ∘ f) id

  step3 : (g : B → A) → (g ∘ f ∘ f⁻¹ ≡ f⁻¹) ≃ (g ≡ f⁻¹)
  step3 g = ≃/symm ((λ p → q ∙ p) , qinv→isbiinvt (left-∙/qinv q f⁻¹))
   where
    q : g ∘ f ∘ f⁻¹ ≡ g
    q = (ap (λ - → g ∘ -) (funext α))

  combined = λ g → (((step1 g) ≃∙ (step2 g)) ≃∙ (step3 g))


qinv→rinv-isCtr : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
              → (f : A → B)
              → (q : qinv f)
              → isCtr (rinv f)
     
qinv→rinv-isCtr {A = A} {B = B} f (f⁻¹ , α , β) =
  isCtr/≃ (Σ≡-isCtr' f⁻¹) (Σ/fibers-≃ combined)
 where

  step1 : (g : B → A) → (f ∘ g ∼ id) ≃ (f ∘ g ≡ id)
  step1 g =  ≃/symm (funext-≃ {f₀ = f ∘ g} {f₁ = id})

  step2 : (g : B → A) → (f ∘ g ≡ id) ≃ (f⁻¹ ∘ f ∘ g ≡ f⁻¹)
  step2 g = ap/≃ ((f⁻¹ ∘_ , qinv→isbiinvt (qinv→postcomp-qinv f⁻¹ (f , β , α)))) (f ∘ g) id

  step3 : (g : B → A) → (f⁻¹ ∘ f ∘ g ≡ f⁻¹) ≃ (g ≡ f⁻¹)
  step3 g = ≃/symm ((λ p → q ∙ p) , qinv→isbiinvt (left-∙/qinv q f⁻¹))
   where
    q : (f⁻¹ ∘ f) ∘ g ≡ g
    q = (ap (λ - → - ∘ g) (funext β))

  combined = λ g → (((step1 g) ≃∙ (step2 g)) ≃∙ (step3 g))



inhabited-isCtr→isProp : {A : 𝓤 ℓ} → (A → isCtr A) → isProp A
inhabited-isCtr→isProp {A = A} f a₀ a₁ = snd (f a₀) a₀ ⁻¹ ∙ snd (f a₀) a₁


-- FINALLY!!!
isbiinvt-isProp : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
                → {f : A → B}
                → isProp (isbiinvt f)
isbiinvt-isProp {A = A} {B = B} {f} =
  inhabited-isCtr→isProp λ e →
    ( isCtr/×
        ( qinv→rinv-isCtr f (isbiinvt→qinv e ))
        ( qinv→linv-isCtr f (isbiinvt→qinv e) )
    )


-- Now, time to prove that `qinv` is not a prop in general.
-- OK I'm not up for the challenge
{-
Lem-4-1-2 : {A : 𝓤 ℓ}
          → (a : A)
          → (q : a ≡ a)
          → isSet (a ≡ a)
          → ((x : A) → ∥ a ≡ x ∥)
          → ((p : a ≡ a) → p ∙ q ≡ q ∙ p)
          → Σ (λ (f : (x : A) → x ≡ x) → f a ≡ q)
Lem-4-1-2 {A = A} a q σ g γ = {!!}
 where

  ≡-areSets : (x y : A) → isSet (x ≡ y)
  {-
     Goal: construct  `(x y : A) → isSet (x ≡ y)`
     Note that `isProp (isSet _)`
     and also `isProp (_ → (isSet _))` where `_` stands for any type.
     We construct a function
     ```
         ∥ a ≡ x ∥ → ∥ a ≡ y ∥ → isSet (x ≡ y)
     ```
     By `∥∥-recursion`, we may assume that we start with `a ≡ x` instead of its truncation.
     Hence, we're given `p : a ≡ x` and need to construct `∥ a ≡ y ∥ → isSet (x ≡ y)`
     We apply `∥∥-recursion` again, so that we have `q : a ≡ y` in addition to `p`, and need
     to construct `isSet x ≡ y`.
     But concatenating any path `a ≡ a` with `p` and `q` appropriately yields an equivalence
     between `a ≡ a` and `x ≡ y`.
     The former type is a set, and we're done
  -}
  ≡-areSets x y =
    ∥∥-recursion {X = (a ≡ x)} {P = ∥ (a ≡ y) ∥ → isSet (x ≡ y)}
      ( isProp/→ ∥ a ≡ y ∥ (isSet-isProp (x ≡ y)))
      ( λ (p : a ≡ x) (q' : ∥ a ≡ y ∥) →
        ∥∥-recursion {X = (a ≡ y)} {P = isSet (x ≡ y)}
          (isSet-isProp (x ≡ y))
          ( λ (q : a ≡ y) → isSet/≃ σ (lol p q))
          q'
      )
      (g x)
      (g y)
   where
    lol : a ≡ x → a ≡ y → (x ≡ y) ≃ (a ≡ a)
    lol p q = (x ≡ y) ≃⟨ left-∙/≃ p y  ⟩
              (a ≡ y) ≃⟨ ≃/symm (right-∙/≃ q a) ⟩
              (a ≡ a) ≃∎

  B = λ (x : A) → Σ[ r ∈ (x ≡ x) ] Π[ s ∈ (a ≡ x) ] (r ≡ s ⁻¹ ∙ q ∙ s)

  Bx-isProp : (x : A) →  isProp (B x)
  Bx-isProp x =
    ∥∥-recursion {X = a ≡ x} {P = isProp (B x)}
      (isProp-isProp (B x))
      (λ p → Σ-induction (λ r h → Σ-induction λ r' h' →
          ≡Σ→≡ ( ((h p) ∙ (h' p ⁻¹))
               , funext λ s → ≡-areSets x x _ _ _ _))
               )
      (g x)

  piB : Π B
  piB x =
    ∥∥-recursion {X = a ≡ x}
      (Bx-isProp x)
      (λ p → ((p ⁻¹ ∙ q ∙ p ) , {!!}))
      (g x)
-}

Lem-4-1-1 : {A : 𝓤 ℓ} {B : 𝓤 ℓ} {f : A → B}
          → qinv f
          → qinv f ≃ (Π λ (x : A) → x ≡ x)
Lem-4-1-1 {A = A} {B} {f} q = case-≃ A B f (qinv→isbiinvt q)
 where
  case-≡-refl : (A : 𝓤 ℓ)  → (qinv (𝑖𝑑 A)) ≃ (Π λ (x : A) → x ≡ x)
  case-≡-refl A =
    (
       ( qinv (𝑖𝑑 A)                           )            ≃⟨ ≃/refl _ ⟩
       ( Σ λ (g : A → A) → (g ∼ id) × (g ∼ id) )           ≃⟨ Σ/fibers-≃ (λ g → ×/≃ (≃/symm funext-≃) (≃/symm funext-≃)) ⟩
       ( Σ λ (g : A → A) → (g ≡ id) × (g ≡ id) )           ≃⟨ ≃1 ⟩
       ( Σ λ (h : Σ λ (g : A → A) → g ≡ id) →  fst h ≡ id) ≃⟨ ≃2 (Σ≡-isCtr' (𝑖𝑑 A)) ⟩ -- Here we use that `fst (Σ≡-isCtr' (𝑖𝑑 A)) = 𝑖𝑑 A` (judgementally) 
       ( (𝑖𝑑 A) ≡ (𝑖𝑑 A) )                                  ≃⟨  funext-≃ ⟩
       ( (𝑖𝑑 A) ∼ (𝑖𝑑 A) )                                   ≃⟨ ≃/refl _ ⟩
       ( Π λ (x : A) → x ≡ x )  ≃∎
    )
   where
    ≃1 : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {Z : X → 𝓤 ℓ''} → (Σ[ x ∈ X ] (Y x) × (Z x)) ≃ (Σ[ p ∈ (Σ[ x ∈ X ] Y x)] Z (fst p)) 
    ≃1 {X = X} {Y} {Z} = e , (g , α) , (h , β)
     where
      e : (Σ[ x ∈ X ] (Y x) × (Z x)) → (Σ[ p ∈ (Σ[ x ∈ X ] Y x)] Z (fst p))
      e (x , (y , z)) = (x , y) , z
      g : (Σ[ p ∈ (Σ[ x ∈ X ] Y x)] Z (fst p)) → (Σ[ x ∈ X ] (Y x) × (Z x))
      g ((x , y) , z) = (x , (y , z))
      h : (Σ[ p ∈ (Σ[ x ∈ X ] Y x)] Z (fst p)) → (Σ[ x ∈ X ] (Y x) × (Z x))
      h ((x , y) , z) = (x , (y , z))
      α : e ∘ g ∼ id
      α ((x , y) , z) = refl _
      β : h ∘ e ∼ id
      β (x , (y , z)) = refl _

    ≃2 :  {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} (γ : isCtr X) → Σ Y ≃ Y (fst γ)
    ≃2 {X = X} {Y} (x , xx) = Σ-base-isCtr-≃-fiber (x , xx)
    


  case-≡ : (A : 𝓤 ℓ) → (B : 𝓤 ℓ) → (p : A ≡ B) → (qinv (tr {Y = id} p)) ≃ (Π λ (x : A) → x ≡ x)
  case-≡ = 𝕁' case-≡-refl

  case-≃ : (A B : 𝓤 ℓ) → (f : A → B) → isbiinvt f → (qinv f) ≃ (Π λ (x : A) → x ≡ x)
  case-≃ {ℓ} A B f e = step2
   where
    step0 : qinv (tr {Y = id} (ua (f , e))) ≃ (Π λ (x : A) → x ≡ x)
    step0 = case-≡ A B (ua (f , e))
    step1 : qinv (fst (idtoeqv (ua (f , e)))) ≃ (Π λ (x : A) →  x ≡ x)
    step1 = tr
      {X = A → B}
      {Y = λ - → (qinv -) ≃ (Π λ (x : A) →  x ≡ x)}
      {x₀ = tr (ua (f , e))}
      {x₁ = fst (idtoeqv (ua (f , e)))}
      (idtoeqv/fst (ua (f , e)) ⁻¹)
      (step0)
    step2 : qinv (fst {X = A → B} {Y = λ - → isbiinvt - } (f , e)) ≃ (Π λ (x : A) →  x ≡ x)
    step2 =
      tr
        {X = A ≃ B}
        {Y = λ - → (qinv (fst -)) ≃ (Π λ (x : A) →  x ≡ x)}
        {x₀ = idtoeqv (ua (f , e))}
        {x₁ = (f , e)}
        (ua-α (f , e))
        step1
    
    



-- Contractible fibers

fib : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
    → (f : A → B) → B
    → 𝓤 (ℓ ⊔ ℓ')
fib {A = A} {B = B} f b = Σ λ (x : A) → f x ≡ b

fib-const : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
          → (isSet B)
          → (f : A → B)
          → (b : B) → ((x : A) → f x ≡ b)
          → fib f b ≃ A
fib-const {A = A} {B = B} BS f b lowol = e , (g , α) , (g , β)
 where
  e : fib f b → A
  g : A → fib f b
  α : e ∘ g ∼ id
  β : g ∘ e ∼ id
  e (a , p) = a
  g a = a , lowol a
  α a = refl _
  β (a , u) = ≡Σ→≡ (refl _ , BS _ _ _ _)


Ctr-fib : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
        → (f : A → B) → 𝓤 (ℓ ⊔ ℓ')
Ctr-fib {A = A} {B = B} f = (b : B) → isCtr (fib f b)

Ctr-fib-isProp : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} → (f : A → B) → isProp (Ctr-fib f)
Ctr-fib-isProp {A = A} {B = B} f = isProp/Π (λ b → isCtr-isProp (fib f b))

qinv→Ctr-fib : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B}
             → qinv f → Ctr-fib f
qinv→Ctr-fib {A = A} {B = B} {f} (g , α , β) b = center , centrality
 where
  center : fib f b
  center = (g b , α b)
  centrality : ∀ x → center ≡ x
  centrality (a , e) = ≡Σ→≡ (pp , qq)
   where
    pp : g b ≡ a
    pp = g b     ≡⟨  ap g (e ⁻¹) ⟩
         g (f a) ≡⟨ β a ⟩
         a       ∎
    postulate qq : tr pp (α b) ≡ e -- TODO!!!! HOW????
    {-qq = tr {X = A} {Y =  λ x →  f x ≡ b} pp (α b)                       ≡⟨ {!!} ⟩
           e                                                              ∎
     -}
     -- need to show tr ((ap g (e ⁻¹) ∙ (β a)) (α b) ≡ e)
     -- This is (ap g (e ⁻¹) ∙ (β a)) (α b)

    
Ctr-fib→qinv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {f : A → B}
             → Ctr-fib f
             → qinv f
Ctr-fib→qinv {A = A} {B = B} {f} C = g , α , β
 where
  g : B → A
  α : f ∘ g ∼ id
  β : g ∘ f ∼ id

  g b = fst (fst (C b))
  α b = snd (fst (C b))
  β a = ap fst (snd (C (f a)) ((a , refl (f a))))
  {- `C (f a)` is the contrability of `fib f (f a)`.
     the center of this fiber is `(g (f a) , α (f a))`
     `β a` should be a path `g (f a) ≡ a`
     `snd (C (f a))` says that any element in the fiber at `f a` is equal to `(g b , α b)`
     `(a , refl (f a))` is in this fiber, hence applying the above to it, and then projecting onto the first coordinate,
     we get the desired equality
  -}
