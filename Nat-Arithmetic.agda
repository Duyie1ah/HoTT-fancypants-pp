-- {-# OPTIONS --without-K --exact-split --safe #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Yage numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`


module Nat-Arithmetic where

open import Spartan-MLTT public
open import Basics public
open import Structure-Of-Type-Formers public






pred : ℕ → ℕ
pred = ℕ-recursion ℕ zero (λ n - → n)

pred/zero : pred zero ≡ zero
pred/zero = refl _

pred/succ : (n : ℕ) → pred (succ n) ≡ n
pred/succ n = refl _

succ/pred-zero : succ (pred zero) ≡ one
succ/pred-zero = refl _

succ/pred-succ : (n : ℕ) → succ (pred (succ n)) ≡ succ n
succ/pred-succ = ℕ-induction (λ n → succ (pred (succ n)) ≡ succ n) (refl _) (λ n IHn → ap succ IHn)


-- If successors are equal, so are the elements
s≡s→≡ : (m n : ℕ) → (succ m ≡ succ n) → m ≡ n
s≡s→≡ m n eq = (pred/succ m)⁻¹ ∙ (ap pred eq) ∙ (pred/succ n)


≡-nat-decidable : (m n : ℕ) → (m ≡ n) + ¬ (m ≡ n)
≡-nat-decidable =
  ℕ-induction (λ m → (n : ℕ) → (m ≡ n) + ¬ (m ≡ n))
    (λ (n : ℕ) →
      ℕ-induction (λ n → (zero ≡ n) + ¬ (zero ≡ n))
        ( inl (refl _))
        ( λ n IHn →  inr (≡→≡ℕ zero (succ n)))
        n
    )
    (λ (m : ℕ) IHm → 
      ℕ-induction _
        (inr (≡→≡ℕ (succ m) zero))
        (λ (n : ℕ) IHn →
          +-recursion
            (λ (eq : m ≡ n) → inl (ap succ eq))
            -- alternative : -- (λ (neq : m ≢ n) → inr  λ seq → neq ( ≡ℕ→≡ m n (≡→≡ℕ (succ m) (succ n) seq)))
            (λ (neq : m ≢ n) → inr λ seq → neq (s≡s→≡ m n seq))
            
            (IHm n))
    )


-- Alternatively we first show that `≡ℕ` is decidable
≡ℕ-decidable : (m n : ℕ) → (m ≡ℕ n) + ¬ (m ≡ℕ n)
≡ℕ-decidable =  ℕ-induction (λ m → (n : ℕ) → (m ≡ℕ n) + ¬ (m ≡ℕ n))
    (λ (n : ℕ) →
      ℕ-induction (λ n → (zero ≡ℕ n) + ¬ (zero ≡ℕ n))
        ( inl ⋆)
        ( λ n IHn →  inr id)
        n
    )
    (λ (m : ℕ) IHm → 
      ℕ-induction _
        (inr id)
        (λ (n : ℕ) IHn →
          +-recursion
            (λ (eq : m ≡ℕ n) → inl eq)
            (λ (neq : ¬ (m ≡ℕ n)) → inr neq)
            (IHm n)
        )
    )

-- And then can get `≡` decidable for `ℕ` easily:

≡-nat-decidable' : (m n : ℕ) → (m ≡ n) + ¬ (m ≡ n)
≡-nat-decidable' m n =
  +-induction (λ either → (m ≡ n) + ¬ (m ≡ n))
  ( λ eq → inl (≡ℕ→≡ m n eq) )
  ( λ ne → inr λ x → ne (≡→≡ℕ m n x) )
  (≡ℕ-decidable m n)







_-ℕ_ : ℕ → ℕ → ℕ
_-ℕ_ m =
  ℕ-recursion ℕ
    m
    (λ n m-n → pred m-n)


_+ℕ_ : ℕ → ℕ → ℕ
_+ℕ_ m =
  ℕ-recursion ℕ
    m
    (λ n m+n → succ m+n)



+ℕ/rzero : (n : ℕ) → n +ℕ zero ≡ n
+ℕ/rzero = refl

+ℕ/lzero : (n : ℕ) → zero +ℕ n ≡ n
+ℕ/lzero =
  ℕ-induction
    (λ n → zero +ℕ n ≡ n)
    (refl _)
    (λ n z+ℕn≡n → zero +ℕ succ n ≡⟨ refl _ ⟩
                   succ (zero +ℕ n) ≡⟨ ap succ z+ℕn≡n ⟩
                   succ n ∎)

+ℕ/rsucc : (m : ℕ) (n : ℕ) → n +ℕ (succ m) ≡ succ (n +ℕ m)
+ℕ/rsucc m n = refl _

+ℕ/lsucc : (m : ℕ) (n : ℕ) → (succ m) +ℕ n ≡ succ (m +ℕ n)
+ℕ/lsucc m =
  ℕ-induction
    (λ n → (succ m) +ℕ n ≡ succ (m +ℕ n))
    (refl _)
    (λ n ih → succ m +ℕ succ n     ≡⟨ refl _ ⟩
              succ (succ m +ℕ n)   ≡⟨ ap succ ih ⟩
              succ (succ (m +ℕ n)) ≡⟨ refl _ ⟩
              succ (m +ℕ succ n)   ∎)

+ℕ/assoc : (m n k : ℕ) → m +ℕ (n +ℕ k) ≡ (m +ℕ n) +ℕ k
+ℕ/assoc m n =
  ℕ-induction
    _
    (refl _)
    (λ k ih → m +ℕ (n +ℕ succ k)   ≡⟨ ap (m +ℕ_ ) (+ℕ/rsucc k n) ⟩ -- could also just use refl _
              m +ℕ succ (n +ℕ k)   ≡⟨ refl _ ⟩                -- could also use +ℕ/rsucc
              succ (m +ℕ (n +ℕ k)) ≡⟨ ap succ ih ⟩
              succ ((m +ℕ n) +ℕ k) ≡⟨ refl _ ⟩
              (m +ℕ n) +ℕ succ k   ∎)

+ℕ/comm : (m n : ℕ) → m +ℕ n ≡ n +ℕ m
+ℕ/comm m =
  ℕ-induction
    (λ n → m +ℕ n ≡ n +ℕ m)
    ((+ℕ/lzero m)⁻¹)
    λ n ih → m +ℕ succ n    ≡⟨ refl _  ⟩
              succ (m +ℕ n) ≡⟨ ap succ ih ⟩
              succ (n +ℕ m) ≡⟨ (+ℕ/lsucc n m)⁻¹ ⟩
              succ n +ℕ m   ∎



_*ℕ_ : ℕ → ℕ → ℕ
_*ℕ_ m = ℕ-recursion ℕ zero (λ n m*n → m*n +ℕ m)

_^ℕ_ : ℕ → ℕ → ℕ
_^ℕ_ m = ℕ-recursion ℕ one (λ n m^n → m^n *ℕ m)

test : two ^ℕ two ≡ two +ℕ two
test = refl _
