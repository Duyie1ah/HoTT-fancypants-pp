-- {-# OPTIONS --without-K --exact-split --safe #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Yage numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`


module Notes.Chapter-01 where

open import Basics public


-- p. 43
-- TODO why can't I use the λ-less notation for `f` and `f-good`

ac : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {R : A → B → 𝓤 ℓ''}
   → ( Π[ x ∈ A ] Σ[ y ∈ B ] R x y )
   → ( Σ[ f ∈ (A → B) ] Π[ x ∈ A ] R x (f x))
ac {A = A} {B = B} {R = R} F = f , f-good
 where
  f : A → B
  -- f = fst ∘ F
  f x = fst (F x)
  f-good : Π[ x ∈ A ] R x (f x)
  -- f-good = snd ∘ F
  f-good x = snd (F x)

ac-inv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {R : A → B → 𝓤 ℓ''}
   → ( Σ[ f ∈ (A → B) ] Π[ x ∈ A ] R x (f x))
   → ( Π[ x ∈ A ] Σ[ y ∈ B ] R x y )
ac-inv {A = A} {B = B} {R = R} =
  Σ-induction
    λ f f-good → (λ (x : A) → f x , f-good x)


-- p. 56

p56-demorgan : {A B : 𝓤 ℓ} → (¬ A × ¬ B) → ¬ (A + B)
p56-demorgan =
  Σ-recursion -- {X = ¬ A} {Y = λ - → ¬ B} {A = ¬ (A + B)}
  ( λ na nb →
    +-recursion na nb)


p58-demorgan : {A B : 𝓤 ℓ} → ¬ (A + B) → (¬ A × ¬ B)
p58-demorgan {A = A} {B = B} naorb = (λ x → naorb (inl x)) , λ x → naorb (inr x)


-- Pi distributes over ×
p59-Π-dist-× : {A : 𝓤 ℓ} {P Q : A → 𝓤 ℓ'}
             → (Π (λ x → P x × Q x))
             → (Π P) × (Π Q)
p59-Π-dist-× f = (λ x → fst (f x)) , λ x → snd (f x)


