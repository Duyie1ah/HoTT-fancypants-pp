{-# OPTIONS --without-K --exact-split #-}

module Notes.Chapter-02 where

open import Spartan-MLTT public
open import Basics public
open import Structure-Of-Type-Formers public
open import Notes.Chapter-01 public

-- Stick to one level to make things simpler

module Eckmann-Hilton where

  _■l_ : {A : 𝓤₀}
         {a₀ a₁ a₂ : A}
         {p₀₀ p₀₁ : a₀ ≡ a₁}
         {p₁₀ p₁₁ : a₁ ≡ a₂}
       → (α₀ : p₀₀ ≡ p₀₁)
       → (α₁ : p₁₀ ≡ p₁₁)
       → (p₀₀ ∙ p₁₀ ≡ p₀₁ ∙ p₁₁)
  _■l_ {p₀₁ = p₀₁} {p₁₀ = p₁₀} α₀ α₁ = (α₀ ∙r p₁₀) ∙ (p₀₁ ∙l α₁) 
  
  _■r_ : {A : 𝓤₀}
         {a₀ a₁ a₂ : A}
         {p₀₀ p₀₁ : a₀ ≡ a₁}
         {p₁₀ p₁₁ : a₁ ≡ a₂}
       → (α₀ : p₀₀ ≡ p₀₁)
       → (α₁ : p₁₀ ≡ p₁₁)
       → (p₀₀ ∙ p₁₀ ≡ p₀₁ ∙ p₁₁)
  _■r_ {p₀₀ = p₀₀} {p₁₁ = p₁₁} α₀ α₁ =  (p₀₀ ∙l α₁) ∙  (α₀ ∙r p₁₁)


  ■l/all-refl : {A : 𝓤₀} (a : A) → (refl (refl a)) ■l (refl (refl a)) ≡ (refl (refl a))
  ■l/all-refl {A = A} a =
    (refl (refl a)) ■l (refl (refl a)) ≡⟨ refl _ ⟩
    ((refl (refl a)) ∙r (refl a)) ∙ ((refl a) ∙l (refl (refl a))) ≡⟨ refl _ ⟩
    refl (refl a) ∎

  -- Can't bother
  ■r/all-refl : {A : 𝓤₀} (a : A) → (refl (refl a)) ■r (refl (refl a)) ≡ (refl (refl a))
  ■r/all-refl {A = A} a = refl _

  ■rl-agree : {A : 𝓤₀} {a₀ a₁ a₂ : A} {p₀₀ p₀₁ : a₀ ≡ a₁} {p₁₀ p₁₁ : a₁ ≡ a₂}
            → (α₀ : p₀₀ ≡ p₀₁)
            → (α₁ : p₁₀ ≡ p₁₁)
            → (α₀ ■l α₁) ≡ (α₀ ■r α₁)
  ■rl-agree {A = A} {a₀} {a₁} {a₂} {p₀₀} {p₀₁} {p₁₀} {p₁₁} α₀ α₁ =
    𝕁'
      { X = a₀ ≡ a₁ }
      { A =  λ (p₀₀ p₀₁ : a₀ ≡ a₁) (α₀ : p₀₀ ≡ p₀₁) → (p₁₀ p₁₁ : a₁ ≡ a₂) → (α₁ : p₁₀ ≡ p₁₁) → (α₀ ■l α₁) ≡ (α₀ ■r α₁) }
      ( λ (p₀ : a₀ ≡ a₁) →
      𝕁'
        { X = a₁ ≡ a₂ }
        { A = λ  (p₁₀ p₁₁ : a₁ ≡ a₂) (α₁ : p₁₀ ≡ p₁₁) → ((refl p₀) ■l α₁) ≡ ((refl p₀) ■r α₁) }
        ( λ (p₁ : a₁ ≡ a₂) →
          𝕁'
            { X = A }
            { A = λ (a₀ a₁ : A) (p₀ : a₀ ≡ a₁) → (a₂ : A) → (p₁ : a₁ ≡ a₂) → (refl p₀ ■l refl p₁) ≡ (refl p₀ ■r refl p₁) }
            ( 𝕁'
              {X = A}
              {A = λ (a₀ a₂ : A ) (p₁ : a₀ ≡ a₂) → (refl (refl a₀) ■l refl p₁) ≡ (refl (refl a₀) ■r refl p₁)}
               λ a →  ■l/all-refl a ∙ (■r/all-refl a ⁻¹)
            )
            a₀ a₁ p₀ a₂ p₁
        )
      )
      p₀₀ p₀₁ α₀ p₁₀ p₁₁ α₁ 

  postulate ■l-and-∙-refl : {A : 𝓤₀} (a : A) → (α β : refl a ≡ refl a) → α ■l β ≡ α ∙ β
  {- c.f. the book… it's a chain of equalities using cancellings of inverses and assoc. 
  ■l-and-∙-refl {A = A} a α β =
    α ■l β ≡⟨ refl _ ⟩
    (α ∙r (refl a)) ∙ ((refl a) ∙l β) ≡⟨ refl _ ⟩
    ((refl (refl a) ⁻¹) ∙ α ∙ (refl (refl a))) ∙ ((refl (refl a) ⁻¹) ∙ β ∙ (refl (refl a ))) ≡⟨ {!!} ⟩
    α ∙ β ∎
  -}

  postulate ■r-and-∙-refl : {A : 𝓤₀} (a : A) → (α β : refl a ≡ refl a) → α ■r β ≡ β ∙ α


  {- etc but it's very tiresome but not really useful to learn things I guess
     so let's stop with Eckmann-hilton for now.
     What would remain is :
     1. prove the postulated identities above
     2. Conclude that `α ∙ β ≡ β ∙ α`
     3. Define `Ω : ℕ → Ptd → Ptd`
  

  Ptd : 𝓤₁
  Ptd = Σ (𝑖𝑑 (𝓤₀)) 
  
  Ω¹ : Ptd → Ptd
  Ω¹ = Σ-induction (λ A a →  (a ≡ a) , refl a)
  
  Ω : ℕ → Ptd → Ptd
  Ω zero 𝔸 = 𝔸
  Ω (succ n) 𝔸 = Ω¹ (Ω n 𝔸)


  # : Ptd → 𝓤₀
  # = pr₁ -- If we used `fst` we would have to use `fst/base` and do a back-and-forth below

  _∙Ω_ : {𝔸 : Ptd} → # (Ω¹ 𝔸) → # (Ω¹ 𝔸) → # (Ω¹ 𝔸)
  _∙Ω_ {𝔸 = 𝔸} = Σ-induction (λ A a → λ (p q : # (Ω¹ (A , a))) → p ∙ q) 𝔸

  Eckmann-Hilton : {𝔸 : Ptd} → (p q : # (Ω two 𝔸)) → p ∙Ω q ≡ q ∙Ω p
  Eckmann-Hilton {𝔸 = 𝔸} p q = {! !} 

  I get shitty type errors
  
  -}
   

-- p. 92
-- Section 2.2

-- p. 96
-- lifting a path through a dependent function by passing through a non-dependent one

indep : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
      → (f : Π Y)
      → X → Σ Y
indep f x = (x , f x)

indep/fst : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
          → (f : Π Y)
          → fst ∘ (indep f) ≡ id
indep/fst f = refl id
indep/snd : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
          → (f : Π Y)
          → snd ∘ (indep f) ≡ f
indep/snd f = refl f

apd'  : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {x₀ x₁ : X}
      → (f : Π Y) → (p : x₀ ≡ x₁)
      → (indep f x₀) ≡ (indep f x₁)
apd' {X = X} {Y} {x₀} {x₁} f p = ap (indep f) p

apd'/fst : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {x₀ x₁ : X}
      → (f : Π Y) → (p : x₀ ≡ x₁)
      → ap fst (apd' f p) ≡ p
apd'/fst {X = X} {Y} {x₀} {x₁} f p =   ap fst (apd' f p) ≡⟨ refl _ ⟩
                                      ap fst (ap (indep f) p) ≡⟨ (ap/∘ (indep f) fst p) ⁻¹  ⟩
                                      ap (fst ∘ (indep f)) p ≡⟨ refl _ ⟩
                                      ap id p ≡⟨ ap/id p ⟩
                                      p ∎

apd'/refl : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {x : X}
      → (f : Π Y)
      → (apd' f (refl x)) ≡ refl (x , f x)
apd'/refl {X = X} {Y} {x} f = refl _




-- Universal properties

{- recall
ac : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {R : A → B → 𝓤 ℓ''}
   → ( Π[ x ∈ A ] Σ[ y ∈ B ] R x y )
   → ( Σ[ f ∈ (A → B) ] Π[ x ∈ A ] R x (f x))
ac {A = A} {B = B} {R = R} F = f , f-good
 where
  f : A → B
  -- f = fst ∘ F
  f x = fst (F x)
  f-good : Π[ x ∈ A ] R x (f x)
  -- f-good = snd ∘ F
  f-good x = snd (F x)

ac-inv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {R : A → B → 𝓤 ℓ''}
   → ( Σ[ f ∈ (A → B) ] Π[ x ∈ A ] R x (f x))
   → ( Π[ x ∈ A ] Σ[ y ∈ B ] R x y )
ac-inv {A = A} {B = B} {R = R} =
  Σ-induction
    λ f f-good → (λ (x : A) → f x , f-good x)
-}

ac-→ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {R : A → B → 𝓤 ℓ''}
     → ( Π[ x ∈ A ] Σ[ y ∈ B ] R x y )
     → ( Σ[ f ∈ (A → B) ] Π[ x ∈ A ] R x (f x))
ac-→ = ac
ac-← : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {R : A → B → 𝓤 ℓ''}
     → ( Σ[ f ∈ (A → B) ] Π[ x ∈ A ] R x (f x))
     → ( Π[ x ∈ A ] Σ[ y ∈ B ] R x y )
ac-← = ac-inv

ac-α : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {R : A → B → 𝓤 ℓ''}
     → ac-→ {R = R} ∘ ac-← {R = R} ∼ id 
ac-α = Σ-induction (λ f f-good → refl (f , f-good))

-- note that η holds for functions (we use that above):
η-fun : {A : 𝓤 ℓ} {B : A → 𝓤 ℓ'}
      → (f : Π B)
      → f ≡ (λ x → f x)
η-fun = refl 

ac-β : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {R : A → B → 𝓤 ℓ''}
     → ac-← {R = R} ∘ ac-→ {R = R} ∼ id 
ac-β F =  funext (λ a → (ac-← ∘ ac-→) F a                              ≡⟨ refl _ ⟩
                       (ac-← ((λ a → fst (F a)) , λ a → snd (F a))) a  ≡⟨ refl _ ⟩
                       (λ a → fst (F a) , snd (F a)) a                 ≡⟨ refl _ ⟩
                       fst (F a) , snd (F a)                           ≡⟨ Σ-uniq (F a) ⁻¹ ⟩
                       F a                                             ∎)
