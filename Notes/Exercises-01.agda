-- {-# OPTIONS --without-K --exact-split --safe #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Yage numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`


module Notes.Exercises-01 where

open import Spartan-MLTT public
open import Basics public
open import Notes.Chapter-01 public


-- Exercise 1

comp-assoc : {A B C D : 𝓤 ℓ} {f : A → B} {g : B → C} {h : C → D}
           → h ∘ (g ∘ f) ≡ (h ∘ g) ∘ f
comp-assoc = refl _
 {- why is that the case? using `.` for composition
   h . (g . f) = λ x → h ((g.f) x)            -- by defn of `.`
               = λ x → h (λ y → (g (f y)) x)  -- by defn of `.`
               = λ x → h (g (f x))            -- by β-reduction
    and
    (h . g) . f = λ x → (h.g) (f x)           -- by defn of `.`
                = λ x → (λ y → h (g y)) (f x) -- by defn of `.`
                = λ x → h (g (f x))           -- by β-reduction
    both terms are definitionally equal, hence we can use refl
-}

-- Exercise 2 & 3

Σ-induction' : {A : 𝓤 ℓ} {B : A → 𝓤 ℓ'} {C : Σ B → 𝓤 ℓ'}
             → (c : (a : A) → (b : B a) → C (a , b))
             → (p : Σ B) → C p
Σ-induction' {A = A} {B = B} {C = C} c p = tr {X = Σ B} {Y = C} {x₀ = fst p , snd p} {x₁ = p} ((Σ-uniq p)⁻¹) (c (fst p) (snd p))

{- doesn't work :(

Σ-induction'/pair : {A : 𝓤 ℓ} {B : A → 𝓤 ℓ'} {C : Σ B → 𝓤 ℓ'}
             → (c : (a : A) → (b : B a) → C (a , b))
             → (a : A) → (b : B a) → Σ-induction' {A = A} {B} {C} (a , b) ≡ c a b
Σ-induction'/pair {A = A} {B} {C} c a b = Σ-induction' c (a , b) ≡⟨ ? ⟩
                                          c a b ∎

-}

-- Exercise 4

-- We're given `ℕ-iteration : (X : 𝓤 ℓ) → X → (X → X) → ℕ → X`
-- satisfying the computation rules (judgementally)

module Exercise-04 where
  
  ℕ-iteration-base : {X : 𝓤 ℓ} (x : X) (f : X → X)
                   → ℕ-iteration X x f zero ≡ x
  ℕ-iteration-base x f = refl _
  ℕ-iteration-ind : {X : 𝓤 ℓ} (x : X) (f : X → X) (n : ℕ)
                   → ℕ-iteration X x f (succ n) ≡ f (ℕ-iteration X x f n)
  ℕ-iteration-ind x f n = refl _
  
  -- because of the definition of its definition in terms of `ℕ-recursion` and
  -- its computation rules
  
  ℕ-iter = ℕ-iteration
  
  itrfy : {X : 𝓤₀} →  (ℕ → X → X) → (ℕ × X) → (ℕ × X)
  itrfy {X} f = Σ-recursion {X = ℕ} {Y = λ - → X} {A = ℕ × X} ( λ n xn → (succ n , f n xn) )

  itrfy/eq : {X : 𝓤₀} (f : ℕ → X → X) (x : X) (n : ℕ) → itrfy {X} f (n , x) ≡ (succ n , f n x)
  itrfy/eq {X} f xz n = refl _

  itrfy/eq' : {X : 𝓤₀} (f : ℕ → X → X) (p : ℕ × X) → itrfy {X} f p ≡ (succ (fst p) , f (fst p) (snd p))
  itrfy/eq' {X} f p =  (ap (itrfy f) (Σ-uniq p)) ∙ itrfy/eq f (snd p) (fst p)
  
  ℕ-rec₀ : (X : 𝓤₀) (xz : X) (f : (n : ℕ) (xn : X) → X)
         → ℕ → (ℕ × X)
  ℕ-rec₀ X xz f n = ℕ-iter
    ( ℕ × X )
    ( zero , xz )
    ( itrfy f )
    n


  ℕ-rec₀/base : (X : 𝓤₀) (xz : X) (f : (n : ℕ) (xn : X) → X)
             → ℕ-rec₀ X xz f zero ≡ zero , xz
  ℕ-rec₀/base X xz f = refl _

  ℕ-rec₀/ind : (X : 𝓤₀) (xz : X) (f : (n : ℕ) (xn : X) → X) (n : ℕ)
             → ℕ-rec₀ X xz f (succ n) ≡ itrfy {X} f (ℕ-rec₀ X xz f n)
  ℕ-rec₀/ind X xz f n = refl _

  ℕ-rec₀/fst : (X : 𝓤₀) (xz : X) (f : (n : ℕ) (xn : X) → X) (n : ℕ)
             → fst (ℕ-rec₀ X xz f n) ≡ n
  ℕ-rec₀/fst X xz f =
    ℕ-induction (λ n → fst (ℕ-rec₀ X xz f n) ≡ n)
      (fst/eq {X = ℕ} {Y = λ - → X} zero xz)
      λ n IHn → ( fst (ℕ-rec₀ X xz f (succ n))
               ≡⟨ ap fst (ℕ-rec₀/ind X xz f n) ⟩
                  fst  {X = ℕ} {Y = λ - → X} (itrfy {X} f (ℕ-rec₀ X xz f n)) ≡⟨ ap (fst  {X = ℕ} {Y = λ - → X} ) (itrfy/eq' {X} f _) ⟩
                  fst  {X = ℕ} {Y = λ - → X} ( (succ (fst (ℕ-rec₀ X xz f n)) , f (fst {X = ℕ} {Y = λ - → X} (ℕ-rec₀ X xz f n)) (snd (ℕ-rec₀ X xz f n))) ) ≡⟨ fst/eq {X = ℕ} {Y = λ - → X} _ _ ⟩
                  succ (fst (ℕ-rec₀ X xz f n)) ≡⟨ ap succ IHn ⟩
                  succ n ∎)

 
      
  ℕ-rec : (X : 𝓤₀) (xz : X) (f : (n : ℕ) (xn : X) → X)
        → ℕ → X
  ℕ-rec X xz f n = snd (ℕ-rec₀ X xz f n)
  
  ℕ-rec/base : (X : 𝓤₀) (xz : X) (f : (n : ℕ) (xn : X) → X)
             → ℕ-rec X xz f zero ≡ xz
  ℕ-rec/base X xz f = snd/eq {Y = λ (- : ℕ) → X} zero xz
  
  ℕ-rec/ind : (X : 𝓤₀) (xz : X) (f : (n : ℕ) (xn : X) → X) (n : ℕ)
             → ℕ-rec X xz f (succ n) ≡ f n (ℕ-rec X xz f n)
  ℕ-rec/ind X xz f n =  ℕ-rec X xz f (succ n)               ≡⟨ refl _ ⟩
                        snd (ℕ-rec₀ X xz f (succ n))        ≡⟨ refl _ ⟩
                        snd (itrfy {X} f (ℕ-rec₀ X xz f n)) ≡⟨ ap (snd {Y = λ - → X}) ( itrfy/eq' {X} f _) ⟩
                        snd  {Y = λ - → X} ((succ (fst (ℕ-rec₀ X xz f n)) , f (fst (ℕ-rec₀ X xz f n)) (snd (ℕ-rec₀ X xz f n)))) ≡⟨ snd/eq {X = ℕ} {Y = λ (- : ℕ) → X} _ _ ⟩
                        f (fst (ℕ-rec₀ X xz f n)) (snd (ℕ-rec₀ X xz f n)) ≡⟨ refl _ ⟩
                        f (fst (ℕ-rec₀ X xz f n)) (ℕ-rec X xz f n) ≡⟨ ap (λ - → f - (ℕ-rec X xz f n)) (ℕ-rec₀/fst X xz f n) ⟩
                        f n  (ℕ-rec X xz f n) ∎

module Exercise-05 where
  
  _+'_ : (A B : 𝓤 ℓ) → 𝓤 ℓ
  _+'_ {ℓ = ℓ} A B = Σ (𝔹-recursion (𝓤 ℓ) A B)

  inl' : {A B : 𝓤 ℓ} (x : A) → A +' B
  inl' {A} {B} x = (𝕥 , x)
  inr' : {A B : 𝓤 ℓ} (x : B) → A +' B
  inr' {A} {B} x = (𝕗 , x)

  +'-induction : {A B : 𝓤 ℓ} {C : A +' B → 𝓤 ℓ}
               → ((a : A) →  C (inl' a)) → ((b : B) → C (inr' b))
               → (x : A +' B) → C x
  +'-induction {ℓ = ℓ} {A} {B} {C} left right =
    Σ-induction
      ( 𝔹-induction (λ b → (v : 𝔹-recursion (𝓤 ℓ) A B b) → C (b , v))
          left
          right
      )

  +'-induction/eql : {A B : 𝓤 ℓ} {C : A +' B → 𝓤 ℓ}
                   → (left : (a : A) →  C (inl' a)) → (right : (b : B) → C (inr' b))
                   → (a : A)
                   → +'-induction {ℓ} {A} {B} {C} left right (inl' a) ≡ left a
  +'-induction/eql {ℓ} {A} {B} {C} left right a = refl _


  +'-induction/eqr : {A B : 𝓤 ℓ} {C : A +' B → 𝓤 ℓ}
                   → (left : (a : A) →  C (inl' a)) → (right : (b : B) → C (inr' b))
                   → (b : B)
                   → +'-induction {ℓ} {A} {B} {C} left right (inr' b) ≡ right b
  +'-induction/eqr {ℓ} {A} {B} {C} left right b = refl _


module Exercise-06
  (𝔹-funext : {ℓ : Level} {A B : 𝓤 ℓ} (f g : Π (𝔹-recursion (𝓤 ℓ) A B)) → f ∼ g → f ≡ g)
 where

  _×'_ : (A B : 𝓤 ℓ) → 𝓤 ℓ
  _×'_ {ℓ} A B = Π (𝔹-recursion (𝓤 ℓ) A B)

  _,'_ : {A B : 𝓤 ℓ} → A → B → A ×' B
  _,'_ {ℓ} {A} {B} a b = 𝔹-induction (𝔹-recursion (𝓤 ℓ) A B) a b

  unpack : {A B : 𝓤 ℓ} → A ×' B → A ×' B
  unpack {ℓ} {A} {B} p = p 𝕥 ,' p 𝕗

  unpack/base : {A B : 𝓤 ℓ} → (a : A) → (b : B) → unpack (a ,' b) ≡ (a ,' b)
  unpack/base {ℓ} {A} {B} a b = refl _

  ,'/left : {A B : 𝓤 ℓ} → (a : A) → (b : B) → (a ,' b) 𝕥 ≡ a
  ,'/left a b = refl _

  ,'/right : {A B : 𝓤 ℓ} → (a : A) → (b : B) → (a ,' b) 𝕗 ≡ b
  ,'/right a b = refl _

  η' : {A B : 𝓤 ℓ} → (p : A ×' B) → p ≡ unpack p
  η' {ℓ} {A} {B} p = 𝔹-funext {ℓ = ℓ} {A = A} {B = B} p q homotopic
   where
    q = unpack p
    homotopic : p ∼ q
    homotopic = 𝔹-induction (λ x → p x ≡ q x) (refl _) (refl _)

  postulate η'/base : {A B : 𝓤 ℓ} → (a : A) → (b : B) → η' (a ,' b) ≡ refl _
  -- η'/base {ℓ} {A} {B} a b = {!!}
  -- It seems complicated to prove this without funext as an equivalence
  -- If funext is an equivalence with inverse the usual one, though:
  {-
     We know that if `p = (a ,' b)` then `q = p`.
     Let `h₁` be the homotopy called `homotopic` in the def of `η'` for `p,q` as above.
     Let `h₀` be the homotopy given by `refl : p ≡ q`.
     By definition, `h₀` is `refl` on each coordinate; the same holds for `h₁`.
     It follows that `h₀ ∼ h₁`, and thus `h₀ ≡ h₁` (by funext).
     It follows, applying `funext` to this equality, that `funext h₀ ≡ funext h₁`, but `funext h₀ ≡ refl` since FUNEXT IS AN EQUIVALENCE.
     And thus `refl ≡ funext h₁ ≡ η' a b`.
  -}

  ×'-induction : {A B : 𝓤 ℓ} {C : A ×' B → 𝓤 ℓ} (c : (a : A) → (b : B) → C (a ,' b))
               → (p : A ×' B) → C p
  ×'-induction {ℓ} {A} {B} {C} c p =  tr {Y = C} (η' p ⁻¹) (c (p 𝕥) (p 𝕗))

  ×'-induction/eq : {A B : 𝓤 ℓ} {C : A ×' B → 𝓤 ℓ} (c : (a : A) → (b : B) → C (a ,' b))
                  → (a : A) → (b : B) → ×'-induction {ℓ} {A} {B} {C} c (a ,' b) ≡ c a b
  ×'-induction/eq {ℓ} {A} {B} {C} c a b =
   let
    p = (a ,' b)
   in
    ×'-induction c p                                             ≡⟨  refl _ ⟩
    tr {Y = C} (η' p ⁻¹) (c (p 𝕥) (p 𝕗))                         ≡⟨ refl _ ⟩
    tr {Y = C} (η' p ⁻¹) (c a b)                        ≡⟨ ap (λ - → tr {Y = C} (- ⁻¹) (c a b)) ( η'/base {ℓ} {A} {B} a b) ⟩
    tr {Y = C} ((refl p) ⁻¹) (c a b)                    ≡⟨ ap (λ - → tr {Y = C} - (c a b)) (⁻¹/refl p ) ⟩
    tr {Y = C} ((refl p)) (c a b)                       ≡⟨ tr/refl-at {Y = C} p (c a b) ⟩
    c a b ∎


-- Exercise 7
-- This is covered in `Based-Path-Induction`

-- Exercise 8
-- This should (TODO) be covered in `Nat-Arithmetic`

-- Exercise 9

module Exercise-9 where

  Fin : ℕ → 𝓤₀
  Fin = ℕ-recursion 𝓤₀ 𝟘 (λ n Fin-n → Fin-n + 𝟙)

  fmax : (n : ℕ) → Fin (succ n)
  fmax = ℕ-induction _ (inr ⋆) (λ - - → inr ⋆)

  fmin : (n : ℕ) → Fin (succ n)
  fmin = ℕ-induction _ (inr ⋆) (λ - min-n → inl min-n)


-- Exercise 10
-- not today

-- Exercise 11

¬⁰→¬² : {A : 𝓤 ℓ} → A → ¬¬ A
¬⁰→¬² a = λ f → f a

¬³→¬ : {A : 𝓤 ℓ} → ¬¬¬ A → ¬ A
¬³→¬ F a = F (¬⁰→¬² a)


  
