-- {-# OPTIONS --without-K --exact-split --safe #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Yage numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`


module Notes.Exercises-02 where

open import Spartan-MLTT public
open import Basics public
open import Nat-Arithmetic public
open import Notes.Chapter-02 public
--open import Notes.Exercises-01 public


-- Exercise 1 & 2

module Exercise-01-and-02 (A : 𝓤 ℓ) where

  conc₁ : (a b c : A) → a ≡ b → b ≡ c → a ≡ c
  conc₁ a b c p q =
    𝕁
      A
      ( λ a b p → (c : A) → (q : b ≡ c) → a ≡ c )
      ( λ a c q → q)
      a b p c q

  conc₂ : (a b c : A) → a ≡ b → b ≡ c → a ≡ c
  conc₂ a b c p q =
     𝕁
      A
      ( λ a b p → (c : A) → (q : b ≡ c) → a ≡ c )
      ( 𝕁' (λ a → refl a))
      a b p c q

  conc₃ : (a b c : A) → a ≡ b → b ≡ c → a ≡ c
  conc₃ a b c p q = 
     𝕁
      A
      ( λ b c q → (a : A) → (p : a ≡ b) → a ≡ c )
      ( λ c a p → p)
      b c q a p


  conc₁≡conc₂ : (a b c : A) → (p : a ≡ b) → (q : b ≡ c) → conc₁ a b c p q ≡ conc₂ a b c p q
  conc₁≡conc₂ a a a (refl a) (refl a) = refl _
  -- all equalities use double induction like that

  conc₁≡conc₃ : (a b c : A) → (p : a ≡ b) → (q : b ≡ c) → conc₁ a b c p q ≡ conc₃ a b c p q
  conc₁≡conc₃ a a a (refl a) (refl a) = refl _

  conc₂≡conc₃ : (a b c : A) → (p : a ≡ b) → (q : b ≡ c) → conc₂ a b c p q ≡ conc₃ a b c p q
  conc₂≡conc₃ a a a (refl a) (refl a) = refl _
  

  triangle : (a b c : A) → (p : a ≡ b) → (q : b ≡ c)
           → (conc₁≡conc₂ a b c p q) ∙ (conc₂≡conc₃ a b c p q) ≡ (conc₁≡conc₃ a b c p q)
  triangle a a a (refl a) (refl a) = refl _

-- Exercise 2.3 T'exagères là!
-- Tu peux faire l'induction de 4 manières: juste p, juste q, p puis q, q puis p


module Exercise-04 where

  Path : (n : ℕ) → (A : 𝓤 ℓ) → 𝓤 ℓ
  Path zero A = A
  Path (succ n) A = (p q : Path n A) → p ≡ q
  {- Not sure what they want exactly
  ∂₀ : {n : ℕ} {A : 𝓤 ℓ} {p q : Path n A} → Path (pred n) A
  ∂₀ {n = zero} {A = A} p = p
  ∂₀ {n = succ m} {A = A} p = lhs p
  -}

module Exercise-05 where
 
  -- special case of Exercise 2.6 isn't it?
  

module Exercise-06 where

  -- already done

module Exercise-07 where

  -- already tried

module Exercise-08 where

  {- 2.6.5 says (verbatim) :
    In the above situation, given x, y : A × B and p : pr1 x = pr1 y
    and q : pr2 x = pr2 y, we have
    f (pair= (p, q)) =( f (x) = f (y)) pair= (g(p), h(q)).
  -}

  fun-+ : {A A' : 𝓤 ℓ} {B B' : 𝓤 ℓ'}
         → (f : A → A') (g : B → B')
         → (A + B)
         → (A' + B')
  fun-+ {A = A} {B} {A'} {B'} f g = +-recursion (λ a → inl (f a)) (λ b → inr (g b))

  ap/≡+  : {A A' : 𝓤 ℓ} {B B' : 𝓤 ℓ'}
         → (f : A → A') (g : B → B') 
         → (x₀ x₁ : A + B)
         → x₀ ≡+ x₁
         → fun-+ f g x₀ ≡+ fun-+ f g x₁
  ap/≡+ {A = A} {A'} {B} {B'} f g =
    +-induction
      ( λ x₀ → (x₁ : A + B) → (e : x₀ ≡+ x₁) →  fun-+ f g x₀ ≡+ fun-+ f g x₁)
      ( λ a₀ →
        +-induction (λ x₁ → (e : inl a₀ ≡+ x₁) →  fun-+ f g (inl a₀) ≡+ fun-+ f g x₁)
        ( λ a₁ e →
            𝕁 A
              ( λ a₀ a₁ e → fun-+ f g (inl a₀) ≡+ fun-+ f g (inl a₁) )
              ( λ a → lift ( refl (f a)) )
              a₀ a₁ (lower e)
        )
        ( λ b₁ e → !𝟘 _ (lower e) )
      )
      ( λ b₀ →
        +-induction (λ x₁  →  (e : inr b₀ ≡+ x₁) → fun-+ f g (inr b₀) ≡+ fun-+ f g x₁)
        ( λ a₁ e → !𝟘 _ (lower e) )
        ( λ b₁ e →
           𝕁 B
            ( λ b₀ b₁ e → fun-+ f g (inr b₀) ≡+ fun-+ f g (inr b₁) )
            ( λ b → lift ( refl (g b)) )
            b₀ b₁ (lower e)
       
        )
        
      )
      
  

  
  ap/≡+-agree-type : {A A' : 𝓤 ℓ} {B B' : 𝓤 ℓ'}
        → (f : A → A') (g : B → B') 
        → (x₀ x₁ : A + B)
        → (e : x₀ ≡+ x₁)
        → 𝓤 (ℓ ⊔ ℓ')
  ap/≡+-agree-type f g x₀ x₁ e =  ap (fun-+ f g) (≡+→≡ {x₀ = x₀} {x₁ = x₁} e) ≡ ≡+→≡ (ap/≡+ f g x₀ x₁ e)

  ap/≡+-agree : {A A' : 𝓤 ℓ} {B B' : 𝓤 ℓ'}
        → (f : A → A') (g : B → B') 
        → (x₀ x₁ : A + B)
        → (e : x₀ ≡+ x₁)
        → ap/≡+-agree-type f g x₀ x₁ e
  ap/≡+-agree {A = A} {A'} {B} {B'} f g =
    +-induction
      ( λ x₀ → (x₁ : A + B) → (e : x₀ ≡+ x₁) → ap/≡+-agree-type f g x₀ x₁ e)
      ( λ a₀ →
        +-induction (λ x₁ → (e : inl a₀ ≡+ x₁) →  ap/≡+-agree-type f g (inl a₀) x₁ e)
        ( λ a₁ e →
            𝕁 A
              ( λ a₀ a₁ e → ap/≡+-agree-type f g (inl a₀) (inl a₁) (lift e))
              ( λ a → refl _ )
              a₀ a₁ (lower e)
        )
        ( λ b₁ e → !𝟘 _ (lower e) )
      )
      ( λ b₀ →
        +-induction (λ x₁  →  (e : inr b₀ ≡+ x₁) →  ap/≡+-agree-type f g (inr b₀) x₁ e)
        ( λ a₁ e → !𝟘 _ (lower e) )
        ( λ b₁ e →
           𝕁 B
            ( λ b₀ b₁ e →  ap/≡+-agree-type f g (inr b₀) (inr b₁) (lift e))
            ( λ b → refl _ )
            b₀ b₁ (lower e)
       
        )
        
      )


module Exercise-09 where

  -- UMP of coproduct:

  +/UMP : (A B X : 𝓤 ℓ)
        → (A + B → X) ≃ (A → X) × (B → X)
  +/UMP A B X = forward , qinv→isbiinvt ( backward , α , β )
   where
    forward : (A + B → X) → (A → X) × (B → X)
    forward = λ ϕ → ϕ ∘ inl , ϕ ∘ inr

    backward : (A → X) × (B → X) → A + B → X
    backward =
      Σ-induction
      ( λ f g → +-recursion (λ a → f a) (λ b → g b) )

    α : forward ∘ backward ∼ id
    α (f , g) = ≡Σ→≡ ( refl _ ,  refl _)
    {-
       `fwd ∘ bwd ∼ id`
       By `Σ-induction`, we may assume given a pair `f , g : (A → X) × (B → X)`.
       Have to show that `fwd (bwd (f , g)) ≡ f , g`.
       Can use pairs of paths: using `≡Σ→≡` we have to construct:
       ```
         p : fst (fwd ∘ bwd (f , g)) ≡ fst (f , g) = f
       ```
       and
       ```
         q : tr p (snd (fwd ∘ bwd (f , g))) ≡ snd (f , g) = g
       ```
       By definition:
       ```
         (fwd ∘ bwd (f , g))                   ≡⟨ refl _ ⟩ -- by def
         fwd (+-rec f g)                       ≡⟨ refl _ ⟩ -- by def
         (+-rec f g) ∘ inl , (+-rec f g) ∘ inl ∎           -- by def
       ```
       so that
       ```
         fst (fwd ∘ bwd (f , g))               ≡⟨ refl _ ⟩
         (+-rec f g) ∘ inl                     ∎
       ```
       Now,
       ```
         (+rec f g) ∘ inl                      ≡⟨ refl _ ⟩ -- def of composition
         λ a → +-rec f g (inl a)               ≡⟨ refl _ ⟩ -- computation rule for +-rec
                                                           -- and that `λ a → ϕ = λ a → ψ` if `a : A |- ϕ = ψ`
         λ a → f a                             ≡⟨ refl _ ⟩ -- eta
         f                                     ∎
       ```
       so that we can take `p = refl _`.
       Now, transporting against `refl _` is the identity,
       and the same argument as above shows that we can take `q = refl _` too
    -}

    β : backward ∘ forward ∼ id
    β ϕ = funext (+-induction (λ x → (backward ∘ forward) ϕ x ≡ ϕ x) (λ a → refl _) (λ b → refl _))
    {-
       `bwd ∘ fwd ∼ id`
       We're given `ϕ : A + B → X` and need to show
       ```
         +-induction (ϕ ∘ inl) (ϕ ∘ inr) ≡ ϕ
       ```
       We almost surely need `funext`, which reduces the problem to showing
       ```
         λ (x : A + B) → +-induction (ϕ ∘ inl) (ϕ ∘ inr) x ≡ ϕ x
       ```
       By `+-induction`, we can treat both sides independently, hence need to show:
       ```
         λ (a : A) → (ϕ ∘ inl a) ≡ ϕ (inl a)
       ```
       and similarly for `b`, but given `a : A`, the identity holds by definition.
    -}


  -- Haven't checked the details: merely made some changes until it typechecks, but it should all be the same
  +/dUMP : (A B : 𝓤 ℓ) (X : (A + B) → 𝓤 ℓ')
        → ((p : A + B) → X p) ≃ ((a : A) → X (inl a)) × ((b : B) → X (inr b))
  +/dUMP A B X = forward , qinv→isbiinvt ( backward , α , β )
   where
    forward : Π X → (Π (X ∘ inl)) × (Π (X ∘ inr))
    forward = λ ϕ → ϕ ∘ inl , ϕ ∘ inr

    backward : (Π (X ∘ inl)) × (Π (X ∘ inr)) → Π X
    backward =
      Σ-induction
      ( λ f g → +-induction X (λ a → f a) (λ b → g b) )

    α : forward ∘ backward ∼ id
    α (f , g) = ≡Σ→≡ ( refl _ ,  refl _)
   
    β : backward ∘ forward ∼ id
    β ϕ = funext (+-induction (λ x → (backward ∘ forward) ϕ x ≡ ϕ x) (λ a → refl _) (λ b → refl _))

module Exercise-10 where

  Σ/assoc : (A : 𝓤 ℓ) (B : A → 𝓤 ℓ) (C : Σ B → 𝓤 ℓ)
          → (Σ[ x ∈ A ] ( Σ[ y ∈ B x ] C (x , y))) ≃ Σ C
  Σ/assoc A B C = fwd , (bwd , α) , (bwd , β)
   where

    L = Σ[ x ∈ A ] ( Σ[ y ∈ B x ] C (x , y))
    R = Σ C

    fwd : L → R
    fwd (x , y , z) = (x , y) , z
    bwd : R → L
    bwd ((x , y) , z) = x , y , z
    α : fwd ∘ bwd ∼ id
    α ((x , y) , z) = refl _
    β : bwd ∘ fwd ∼ id
    β (x , y , z) = refl _


module Exercise-13 where
  -- Show that (𝟚 ≃ 𝟚) ≃ 𝟚

  postulate ₀≢₁ : ₀ ≢ ₁

  T = 𝟚
  E = T ≃ T

  i : T → T
  i = id

  i-biinvt : isbiinvt i
  i-biinvt = (i , refl) , (i , refl)

  I : E
  I = (i , i-biinvt)

  s : T → T
  s ₀ = ₁
  s ₁ = ₀

  ss∼i : s ∘ s ∼ i
  ss∼i ₀ = refl ₀
  ss∼i ₁ = refl ₁

  s-biinvt : isbiinvt s
  s-biinvt = (s , ss∼i) , (s , ss∼i)

  S : E
  S = (s , s-biinvt)


  fwd : T → E
  fwd ₀ = I
  fwd ₁ = S

  bwd : E → T
  bwd (f , fe) = f ₀

  ₀at₀' : (e : E)
       → (fst e) ₀ ≡ ₀ → (fst e) ≡ i
  ₀at₀' (f , (g , α) , (h , β)) p =
      𝟚-induction
        ( λ v → (q : f ₁ ≡ v) → f ≡ i)
        ( λ q → !𝟘 _ (₀≢₁ ( ₀        ≡⟨ β ₀ ⁻¹ ⟩
                            h (f ₀)  ≡⟨ ap h p ⟩
                            h ₀      ≡⟨ ap h q ⁻¹ ⟩
                            h (f ₁)  ≡⟨ β ₁ ⟩
                            ₁        ∎
                           )
                      )
        )
        (λ q → funext (𝟚-induction (λ x → f x ≡ x) p q))
        (f ₁) (refl (f ₁))

  -- being biinvt is a prop, so if (f , B) (f' , B') are given with f ≡ f', then the pairs are equal.
  -- But we don't know that yet, so should prove it by hand in our specific case of equivalences on 𝟚
  -- Let's skip that for now…

  postulate ₀at₀ : (e : E) → (fst e) ₀ ≡ ₀ → e ≡ I

  postulate ₁at₀ : (e : E) → (fst e) ₀ ≡ ₁ → e ≡ S
  -- same idea -- ₁at₀ (f , fe) = {!!}

  α : fwd ∘ bwd ∼ id  -- note that `fwd (bwd (f , e)) = fwd (f ₀)`
  α (f , fe) =
    𝟚-induction
      ( λ v → (e : f ₀ ≡ v) → (fwd (f ₀) ≡ (f , fe)) )
      ( λ (e : f ₀ ≡ ₀) →  fwd (f ₀) ≡⟨ ap fwd e ⟩
                           fwd ₀     ≡⟨ refl _ ⟩
                           I         ≡⟨ ₀at₀ (f , fe) e ⁻¹ ⟩
                           f , fe    ∎
      )
      ( λ (e : f ₀ ≡ ₁) →  fwd (f ₀) ≡⟨ ap fwd e ⟩
                           fwd ₁     ≡⟨ refl _ ⟩
                           S         ≡⟨ ₁at₀ (f , fe) e ⁻¹ ⟩
                           f , fe    ∎
      )
      (f ₀) (refl (f ₀)) 

  β : bwd ∘ fwd ∼ id
  β ₀ = refl _
  β ₁ = refl _


  equiv : 𝟚 ≃ (𝟚 ≃ 𝟚)
  equiv = fwd , (bwd , α) , (bwd , β)


module Not-an-Exercise where

  lol : (𝟙 + ℕ) ≃ ℕ

  f : 𝟙 + ℕ → ℕ
  f (inl ⋆) = zero
  f (inr n) = succ n

  g : ℕ → 𝟙 + ℕ
  g zero = inl ⋆
  g (succ n) = inr n

  α : f ∘ g ∼ id
  α zero = refl _
  α (succ n) = refl _

  β : g ∘ f ∼ id
  β (inl ⋆) = refl _
  β (inr n) = refl _

  lol = f , qinv→isbiinvt (g , α , β)


module Exercise-14 where

  {- (Warning: ≡ and = are swapped compared to the book)
  Given `p : x ≡ x`, need to show `p = refl x`.
  Need to show `p ≡ refl x` i.e. construct a term of this type.
  We construct a term of type `(x y : X) (p : x ≡ y) → p ≡ refl x`, and instantiate it at `x` `x` and `p`.
  This typechecks since from the existence of `p : x ≡ y`, we have `p : x ≡ x` since `x = x`.
  By `𝕁`, it suffices to check it at `x` `x` and `refl x`, in which case the needed term is just `refl (refl x)`.
  -}

module Exercise-15 where

  tr/idtoeqv : {A : 𝓤 ℓ} {B : A → 𝓤 ℓ}
             → {x₀ x₁ : A}
             → (p : x₀ ≡ x₁)
             → tr p ≡ fst (idtoeqv (ap B p))
  tr/idtoeqv {A = A} {B} {x} {x} (refl x) = refl _


module Exercise-17 where

  -- confusion!
    
    
  +/≡ : (A B A' B' : 𝓤 ℓ)
      → (A ≡ A') → (B ≡ B')
      → A + B ≡ A' + B'
  +/≡ A B A' B' p q = (A + B)   ≡⟨ ap ( _+ B) p ⟩
                      (A' + B)  ≡⟨ ap (A' +_ ) q ⟩
                      (A' + B') ∎

  
  +/≃' : (A B A' B' : 𝓤 ℓ)
      → (A ≃ A') → (B ≃ B')
      → (A + B) ≃ (A' + B')
  +/≃' A B A' B' ff gg = idtoeqv ( +/≡ A B A' B' (ua ff) (ua gg) ) 

  +-fun : {A B A' B' : 𝓤 ℓ} (f : A → A') (g : B → B') → A + B → A' + B'
  +-fun f g = +-recursion (inl ∘ f) (inr ∘ g)

  +/≃ : (A B A' B' : 𝓤 ℓ)
      → (A ≃ A') → (B ≃ B')
      → (A + B) ≃ (A' + B')
  +/≃ A B A' B' (f , (fl , fα) , (fr , fβ)) (g , (gl , gα) , (gr , gβ)) = +-fun f g , (  +-fun fl gl , α ) , ( +-fun fr gr , β)
   where
    α :  (+-fun f g ∘ +-fun fl gl) ∼ id
    β : (+-fun fr gr ∘ +-fun f g) ∼ id

    α (inl a')= (+-fun f g ∘ +-fun fl gl) (inl a') ≡⟨ refl _ ⟩
                 +-fun f g (inl (fl a'))           ≡⟨ refl _ ⟩
                 inl ((f ∘ fl) a')                 ≡⟨ ap inl (fα a') ⟩
                 inl a'                            ∎ 
    α (inr b') = ap inr (gα b')
    β (inl a) = ap inl (fβ a)
    β (inr b) = ap inr (gβ b)


  Σ/≡ : (A₀ A₁ : 𝓤 ℓ)
      → (B₀ : A₀ → 𝓤 ℓ)
      → (B₁ : A₁ → 𝓤 ℓ)
      → (p : A₀ ≡ A₁)
      → (q : B₀ ≡ B₁ ∘ (tr p))
      → Σ B₀ ≡ Σ B₁
  Σ/≡ {ℓ = ℓ} A₀ A₁ B₀ B₁ p q = 𝕁 (𝓤 ℓ) (λ A₀ A₁ p → (B₀ : A₀ → 𝓤 ℓ) → (B₁ : A₁ → 𝓤 ℓ) → (q : B₀ ≡ B₁ ∘ (tr p)) → Σ B₀ ≡ Σ B₁ ) (λ A → 𝕁 (A → 𝓤 ℓ) _ (λ B → refl _)) A₀ A₁ p B₀ B₁ q

  Π/≡ : (A₀ A₁ : 𝓤 ℓ)
      → (B₀ : A₀ → 𝓤 ℓ)
      → (B₁ : A₁ → 𝓤 ℓ)
      → (p : A₀ ≡ A₁)
      → (q : B₀ ≡ B₁ ∘ (tr p))
      → Π B₀ ≡ Π B₁
  Π/≡ {ℓ = ℓ} A₀ A₁ B₀ B₁ p q = 𝕁 (𝓤 ℓ) (λ A₀ A₁ p → (B₀ : A₀ → 𝓤 ℓ) → (B₁ : A₁ → 𝓤 ℓ) → (q : B₀ ≡ B₁ ∘ (tr p)) → Π B₀ ≡ Π B₁ ) (λ A → 𝕁 (A → 𝓤 ℓ) _ (λ B → refl _)) A₀ A₁ p B₀ B₁ q

  Σ/≃ : (A₀ A₁ : 𝓤 ℓ)
      → (B₀ : A₀ → 𝓤 ℓ)
      → (B₁ : A₁ → 𝓤 ℓ)
      → (Ae : A₀ ≃ A₁)
      → (Be : (a : A₀) → B₀ a ≃ (B₁ (fst Ae a)))
      → Σ B₀ ≃ Σ B₁
  Σ/≃  {ℓ = ℓ} A₀ A₁ B₀ B₁ Ae Be =
    idtoeqv
    ( Σ/≡ A₀ A₁ B₀ B₁
      ( ua Ae )
      ( funext
        ( λ a → ua (Be a) ∙  ap (λ - → B₁ (- a)) (ua/fst A₀ A₁ Ae) )
       )
     )

  {-
  Σ/≃' : (A₀ A₁ : 𝓤 ℓ)
      → (B₀ : A₀ → 𝓤 ℓ)
      → (B₁ : A₁ → 𝓤 ℓ)
      → (Ae : A₀ ≃ A₁)
      → (Be : (a : A₀) → B₀ a ≃ (B₁ (fst Ae a)))
      → Σ B₀ ≃ Σ B₁
  Σ/≃'  {ℓ = ℓ} A₀ A₁ B₀ B₁ Ae Be  = {!!}
   where
    Af = fst Ae
    Ag = fst (fst (snd Ae))
    Aα : (a : A₁) →  (Af ∘ Ag) a ≡ a
    Aα = snd (fst (snd Ae))
    Ah = fst (snd (snd Ae))
    Aβ : (a : A₀) →  (Ah ∘ Af) a ≡ a
    Aβ = snd (snd (snd Ae))
   
    Bf = λ (a : A₀) →  fst (Be a)
    Bg = λ (a : A₀) →  fst (fst (snd (Be a)))
    Bα = λ (a : A₀) →  snd (fst (snd (Be a)))
    Bh = λ (a : A₀) →  fst (snd (snd (Be a)))
    Bβ = λ (a : A₀) →  snd (snd (snd (Be a)))

    f : Σ B₀ → Σ B₁
    f (a , b) = ( Af a , Bf a b )

    g : Σ B₁ → Σ B₀
    g (a , b) = (Ag a , Bg (Ag a) (tr {Y = B₁} (Aα a ⁻¹) b) )

    h : Σ B₁ → Σ B₀
    h (a , b) = (Ag a , Bg (Ag a) (tr {Y = B₁} ( {!Aα !} ⁻¹) b) )
  -}
