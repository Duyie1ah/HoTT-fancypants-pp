-- {-# OPTIONS --without-K --exact-split --safe --allow-unsolved-metas #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`



module Notes.Exercises-03 where

open import Spartan-MLTT public
open import Basics public
open import Equivalence public
open import Structure-Of-Type-Formers public
open import Props-Etc public
open import Logic public
open import Propositional-Resizing public
-- open import Notes.Exercises-02 public


module Exercise-01 where

  isSet/≃' : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
          → A ≃ B → isSet B
          → isSet A
  isSet/≃' {A = A} {B = B} (f , e) BS a₀ a₁ =  isProp/≃   fa₀≡fa₁-isProp a₀≡a₁≃fa₀≡fa₁ 
   where
    a₀≡a₁≃fa₀≡fa₁ = ap/≃ (f , e) a₀ a₁
    fa₀≡fa₁-isProp = BS (f a₀) (f a₁)
    
module Exercise-02 where

  isSet/+ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
          → isSet A → isSet B
          → isSet (A + B)
  isSet/+ {ℓ} {A = A} {B = B} AS BS (inl a₀) (inl a₁) p₀ p₁ =
    p₀                            ≡⟨ ≡→≡+-β p₀ ⁻¹ ⟩
    ≡+→≡ (lift (lower (≡→≡+ p₀))) ≡⟨ ap (λ - → ≡+→≡ (lift -)) (AS a₀ a₁ (lower (≡→≡+ p₀)) (lower (≡→≡+ p₁))) ⟩
    ≡+→≡ (lift (lower (≡→≡+ p₁))) ≡⟨ ≡→≡+-β p₁ ⟩
    p₁                            ∎ 
  isSet/+ {A = A} {B = B} AS BS (inl a₀) (inr b₁) p₀ p₁ = !𝟘 _ (lower (≡→≡+ p₀))  
  isSet/+ {A = A} {B = B} AS BS (inr b₀) (inl a₁) p₀ p₁ = !𝟘 _ (lower (≡→≡+ p₀))
  isSet/+ {A = A} {B = B} AS BS (inr b₀) (inr b₁) p₀ p₁ =
    p₀                            ≡⟨ ≡→≡+-β p₀ ⁻¹ ⟩
    ≡+→≡ (lift (lower (≡→≡+ p₀))) ≡⟨ ap (λ - → ≡+→≡ (lift -)) (BS b₀ b₁ (lower (≡→≡+ p₀)) (lower (≡→≡+ p₁))) ⟩
    ≡+→≡ (lift (lower (≡→≡+ p₁))) ≡⟨ ≡→≡+-β p₁ ⟩
    p₁                            ∎ 

module Exercise-03 where

  isSet/Σ : {A : 𝓤 ℓ} {B : A → 𝓤 ℓ'}
          → (isSet A) → ((x : A) → isSet (B x))
          → isSet (Σ B)
  isSet/Σ {A = A} {B = B} AS BS (a₀ , b₀) (a₁ , b₁) p₀ p₁ =
    p₀             ≡⟨ ≡→≡Σ-β p₀ ⁻¹ ⟩
    ≡Σ→≡ P₀        ≡⟨ ap ≡Σ→≡ (Σ-uniq P₀) ⟩
    ≡Σ→≡ (r₀ , q₀) ≡⟨ ap {x₀ = (r₀ , q₀)} {x₁ = (r₁ , q₁)} ≡Σ→≡ (≡Σ→≡ (r≡ , q≡)) ⟩
    ≡Σ→≡ (r₁ , q₁) ≡⟨ ap ≡Σ→≡ (Σ-uniq P₁ ⁻¹) ⟩
    ≡Σ→≡ P₁        ≡⟨ ≡→≡Σ-β p₁ ⟩
    p₁             ∎ 
   where
   
    P₀ = ≡→≡Σ p₀
    P₁ = ≡→≡Σ p₁
    r₀ = fst P₀
    q₀ : tr r₀ b₀ ≡ b₁
    q₀ = snd P₀
    r₁ = fst P₁
    q₁ : tr r₁ b₀ ≡ b₁
    q₁ = snd P₁

    r≡ : r₀ ≡ r₁
    r≡ = AS a₀ a₁ r₀ r₁

    q≡ : tr {Y = λ x → tr x b₀ ≡ b₁} r≡ q₀ ≡ q₁
    q≡ = BS a₁ (tr r₁ b₀) b₁ (tr {Y = λ x → tr x b₀ ≡ b₁} r≡ q₀) q₁

    
module Exercise-04 (A : 𝓤 ℓ) where

  fwd : isProp A → isCtr (A → A)
  fwd ϕ = id , λ (f : A → A) → funext (λ a → ϕ a (f a))

  bwd : isCtr (A → A) → isProp A
  bwd (c , cc) a₀ a₁ =
    a₀     ≡⟨ refl _ ⟩
    ca₀ a₀ ≡⟨ ap (λ f → f a₀) (cc ca₀) ⁻¹ ⟩
    c a₀   ≡⟨ ap (λ f → f a₀) (cc ca₁) ⟩
    ca₁ a₀ ≡⟨ refl _ ⟩
    a₁ ∎
   where
    ca₀ : A → A
    ca₀ = λ - → a₀
    ca₁ : A → A
    ca₁ = λ - → a₁

module Exercise-05 (A : 𝓤 ℓ) where

  isProp≃inhabited→isCtr : (isProp A) ≃ (A → isCtr A)
  isProp≃inhabited→isCtr = biimplied-Props→≃ (isProp-isProp A) (isProp/Π (λ - → isCtr-isProp A)) f g
   where
    f : isProp A → (A → isCtr A)
    f p a = inhabited-isProp→isCtr a p
    g : (A → isCtr A) → isProp A
    g ϕ a₀ a₁ = isCtr→isProp (ϕ a₀) a₀ a₁

module Exercise-07 (A : 𝓤 ℓ) (B : 𝓤 ℓ') where

  isProp/+ : (isProp A) → (isProp B) → ¬ (A × B)
           → isProp (A + B)
  isProp/+ AP BP notAB (inl a₀) (inl a₁) = ≡+→≡ (lift (AP a₀ a₁))
  isProp/+ AP BP notAB (inl a₀) (inr b₁) = !𝟘 _ (notAB (a₀ , b₁))
  isProp/+ AP BP notAB (inr b₀) (inl a₁) = !𝟘 _ (notAB (a₁ , b₀))
  isProp/+ AP BP notAB (inr b₀) (inr b₁) = ≡+→≡ (lift (BP b₀ b₁))
  

module Exercise-08
  (isequiv : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} →  (A → B) → 𝓤 ℓ'')
  (cond-i : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} (f : A → B) → isProp (isequiv f))
  (cond-ii : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} (f : A → B) → isequiv f → qinv f )
  (cond-iii : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} (f : A → B) → qinv f → isequiv f)
 where

  cond-i' :  {A : 𝓤 ℓ} {B : 𝓤 ℓ'} (f : A → B) → isProp (∥ qinv f ∥)
  cond-i' f = ∥∥-isProp
  cond-ii' : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} (f : A → B) → ∥ qinv f ∥ → qinv f
  cond-ii' f = (cond-ii f) ∘ (∥∥-recursion (cond-i f) (cond-iii f))
  cond-iii' : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} (f : A → B) → qinv f → ∥ qinv f ∥
  cond-iii' f q = ∣ q ∣

  isequiv≃∥qinv∥ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} (f : A → B)
                  → (isequiv f) ≃ ∥ qinv f ∥
  isequiv≃∥qinv∥ f = biimplied-Props→≃ (cond-i f) (cond-i' f) (λ x → ∣ cond-ii f x ∣ ) (∥∥-recursion (cond-i f) (cond-iii f)) 
  

module Exercise-09
  (lem : LEM {ℓ})
 where

  -- need some glue/boilerplate code for level management/lifting.
  Propℓ = PROP {ℓ}

  𝟙ℓ = Lift ℓ 𝟙
  𝟙ℓ-isProp : isProp 𝟙ℓ
  𝟙ℓ-isProp x y = ap lift (𝟙-isProp (lower x) (lower y))

  𝟙≃𝟙ℓ : (𝟙 ≃ 𝟙ℓ)
  𝟙≃𝟙ℓ = lift , (lower , lift-lower) , (lower , lower-lift {ℓ₀} {ℓ} {X = 𝟙}) 

  𝟘ℓ = Lift ℓ 𝟘
  𝟘ℓ-isProp : isProp 𝟘ℓ
  𝟘ℓ-isProp x y = ap lift (𝟘-isProp (lower x) (lower y))

  𝟘≃𝟘ℓ : (𝟘 ≃ 𝟘ℓ)
  𝟘≃𝟘ℓ = lift , (lower , lift-lower) , (lower , lower-lift {ℓ₀} {ℓ} {X = 𝟘}) 

  𝟙p : Propℓ
  𝟙p = 𝟙ℓ , 𝟙ℓ-isProp
  𝟘p : Propℓ
  𝟘p = 𝟘ℓ , 𝟘ℓ-isProp

  f : 𝔹 → Propℓ
  f 𝕥 = 𝟙p
  f 𝕗 = 𝟘p

  g : Propℓ → 𝔹
  g (A , AP) = +-recursion (λ - → 𝕥) (λ - → 𝕗) (lem A AP)

  inhabitedProp→≃𝟙ℓ :{A : 𝓤 ℓ} → (AP : isProp A) → A → (A ≃ 𝟙ℓ)
  inhabitedProp→≃𝟙ℓ AP a = ≃/tran (inhabitedProp→≃𝟙 AP a) 𝟙≃𝟙ℓ

  negatedProp→≃𝟘ℓ :{A : 𝓤 ℓ} → (AP : isProp A) → ¬ A → (A ≃ 𝟘ℓ)
  negatedProp→≃𝟘ℓ AP a = ≃/tran (negatedProp→≃𝟘 AP a) 𝟘≃𝟘ℓ

  -- just a special case of equality in Σ being reduced to equality in
  -- the base type when the "fiber type" is a prop at all points in the base
  A≡B→Ap≡Bp  : {ℓ : Level} (Ap Bp : PROP {ℓ}) → (fst Ap ≡ fst Bp) → (Ap ≡ Bp)
  A≡B→Ap≡Bp (A , AP₀) (A , AP₁) (refl A) = ≡Σ→≡ ( refl A , isProp-isProp A AP₀ AP₁)

  inhabited→≡𝟙p : (Ap : Propℓ) → (a : fst Ap) → (Ap ≡ 𝟙p)
  inhabited→≡𝟙p (A , AP) a =  A≡B→Ap≡Bp (A , AP) 𝟙p (ua ( inhabitedProp→≃𝟙ℓ AP a ))
  
  negated→≡𝟘p : (Ap : Propℓ) → ¬ (fst Ap) → (Ap ≡ 𝟘p)
  negated→≡𝟘p (A , AP) na =  A≡B→Ap≡Bp (A , AP) 𝟘p (ua ( negatedProp→≃𝟘ℓ AP na ))

  α : f ∘ g ∼ id
  α (A , AP) =
    +-induction
      ( λ (x : A + ¬ A) → (e : lem A AP ≡ x) → f (g (A , AP)) ≡ (A , AP) )
      ( λ (a : A) (e : lem A AP ≡ inl a) →
          f (g (A , AP))
        ≡⟨ refl _ ⟩
          f ( +-recursion (λ - → 𝕥) (λ - → 𝕗) (lem A AP) )
        ≡⟨ ap (λ - → f (+-recursion (λ - → 𝕥) (λ - → 𝕗) -)) e ⟩
          f ( +-recursion {X = A} {Y = ¬ A}  (λ - → 𝕥) (λ - → 𝕗) (inl a) )
        ≡⟨ refl _ ⟩
          f 𝕥
        ≡⟨ refl _ ⟩
          (𝟙ℓ , 𝟙ℓ-isProp)
        ≡⟨ inhabited→≡𝟙p (A , AP) a ⁻¹ ⟩
          (A , AP)
        ∎
      )
      ( λ (na : ¬ A) (e : lem A AP ≡ inr na) →
          f (g (A , AP))
        ≡⟨ refl _ ⟩
          f ( +-recursion (λ - → 𝕥) (λ - → 𝕗) (lem A AP) )
        ≡⟨ ap (λ - → f (+-recursion (λ - → 𝕥) (λ - → 𝕗) -)) e ⟩
          f ( +-recursion {X = A} {Y = ¬ A}  (λ - → 𝕥) (λ - → 𝕗) (inr na) )
        ≡⟨ refl _ ⟩
          f 𝕗
        ≡⟨ refl _ ⟩
          (𝟘ℓ , 𝟘ℓ-isProp)
        ≡⟨ negated→≡𝟘p (A , AP) na ⁻¹ ⟩
          (A , AP)
        ∎
      )
      ( lem A AP )
      ( refl (lem A AP) )

  lem𝟙 : lem 𝟙ℓ 𝟙ℓ-isProp ≡ inl (lift ⋆)
  lem𝟙 =
    +-induction
      ( λ x → (lem 𝟙ℓ 𝟙ℓ-isProp ≡ x) → lem 𝟙ℓ 𝟙ℓ-isProp ≡ inl (lift ⋆) )
      ( λ - e → lem 𝟙ℓ 𝟙ℓ-isProp ≡⟨ e ⟩
                inl -            ≡⟨ ap inl (ap lift (⋆≡ (lower -) ⁻¹)) ⟩
                inl (lift ⋆)     ∎)
      ( λ - e → !𝟘 _ (- (lift ⋆)) )
      ( lem 𝟙ℓ 𝟙ℓ-isProp )
      ( refl (lem 𝟙ℓ 𝟙ℓ-isProp) )


  lem𝟘 : lem 𝟘ℓ 𝟘ℓ-isProp ≡ inr (lower) 
  lem𝟘 =
    +-induction
      ( λ x → (lem 𝟘ℓ 𝟘ℓ-isProp ≡ x) → (lem 𝟘ℓ 𝟘ℓ-isProp) ≡ inr lower )
      ( λ x → !𝟘 _ (lower x) )
      ( λ - e → lem 𝟘ℓ 𝟘ℓ-isProp   ≡⟨ e ⟩
                inr -            ≡⟨ ap inr (isProp/¬ 𝟘ℓ - lower) ⟩
                inr lower           ∎
      )
      ( lem 𝟘ℓ 𝟘ℓ-isProp )
      ( refl (lem 𝟘ℓ 𝟘ℓ-isProp) )
    

  β : g ∘ f ∼ id
  β 𝕥 = g (f 𝕥)          ≡⟨ refl _ ⟩
        g (𝟙ℓ , 𝟙ℓ-isProp) ≡⟨  ap (+-recursion (λ - → 𝕥) (λ - → 𝕗)) lem𝟙 ⟩
        𝕥                ∎
  β 𝕗 = g (f 𝕗)          ≡⟨ refl _ ⟩
        g (𝟘ℓ , 𝟘ℓ-isProp) ≡⟨ ap (+-recursion (λ - → 𝕥) (λ - → 𝕗)) lem𝟘 ⟩ 
        𝕗                ∎

  --
  finally : 𝔹 ≃ Propℓ
  finally = f , (g , α) , (g , β)

module Exercise-10
  (lemℓ₊ : LEM {ℓ ₊})
 where

  -- If 𝓤 (ℓ ₊) satisfies LEM
  -- then Lift : Prop ℓ → Prop (ℓ ₊)
  -- is an equivalence.
  --
  -- Why is that?

  lemℓ : LEM {ℓ}
  lemℓ A AP =
    +-recursion
     ( λ a₊ → inl (lower a₊) )
     ( λ na₊ → inr (na₊ ∘ lift) )
     ( lemℓ₊ A₊ AP₊ )
   where
    A₊ = fst (Lift-PROP (A , AP))
    AP₊ = snd (Lift-PROP (A , AP))

  --
  finally : PROP {ℓ} ≃ PROP {ℓ ₊}
  finally = ≃/tran (≃/symm (Exercise-09.finally (lemℓ))) (Exercise-09.finally (lemℓ₊))

  postulate
    finally/inhabited : {A : 𝓤 ℓ} → (Ap : isProp A) → A
                      → fst (fst finally (A , Ap)) ≡ Lift (ℓ ₊) 𝟙
    finally/negated : {A : 𝓤 ℓ} → (Ap : isProp A) → ¬ A
                      → fst (fst finally (A , Ap)) ≡ Lift (ℓ ₊) 𝟘
    -- should be provable by case analysis on `lem A Ap`, since it's the definition of `finally`.
    -- That is, depending on the result of `lem A Ap`:
    -- If inhabited, we first map it to 𝕥 : 𝔹, and then to Lift 𝟙, etc
    -- If negated, we first map it to 𝕗 : 𝔹, and then to Lift 𝟘, etc.
    
  finally₂ : isbiinvt (Lift-PROP {ℓ})
  finally₂ = tr {X = PROP {ℓ} → PROP {ℓ ₊}} {Y = isbiinvt} ((funext eq) ⁻¹) (snd finally)
   where
    eq : (Lift-PROP {ℓ}) ∼ (fst finally)
    eq (A , AP) =
      Exercise-09.A≡B→Ap≡Bp
        lemℓ
        ( Lift-PROP {ℓ} (A , AP) )
        ( fst finally (A , AP) )
        ( +-induction {ℓ = ℓ} {ℓ' = ℓ} 
          ( λ x → Lift (ℓ ₊) A ≡ fst (fst finally (A , AP)))
          ( λ a →
            ua (≃/tran (≃/tran (≃/symm (Lift/≃ (ℓ ₊)))
                                (inhabitedProp→≃𝟙 AP a))
                                (Lift/≃ (ℓ ₊))
            ) ∙ finally/inhabited AP a ⁻¹
          ) 
          ( λ na →
            ua (≃/tran (≃/tran (≃/symm (Lift/≃ (ℓ ₊)))
                                (negatedProp→≃𝟘 AP na))
                                (Lift/≃ (ℓ ₊))
            ) ∙ finally/negated AP na ⁻¹
          )
          (lemℓ A AP)
        )

module Exercise-11 where

  postulate lol : ¬ ( (A : 𝓤 ℓ ) → ∥ A ∥ → A )
  -- lol ϕ = {!!}
  -- que???

module Exercise-12 
  (lemℓ : LEM {ℓ})
 where

  -- To show that ∥(∥ A ∥ → A)∥ is inhabited.
  -- It is a prop , so we have that either it or its negation holds.
  -- Hence, remains to show that ¬ ∥(∥ A ∥ → A)∥ is impossible.
  -- Assume ϕ : ∥(∥ A ∥ → A)∥ → 𝟘
  -- Need need to get to 𝟘
  -- ???

  ch : (A : 𝓤 ℓ) → 𝓤 ℓ
  ch A = ∥ A ∥ → A
  chtr : (A : 𝓤 ℓ) → 𝓤 ℓ
  chtr A = ∥ ch A ∥

  postulate finally : {A : 𝓤 ℓ} → ∥ (∥ A ∥ → A) ∥
  {- finally {A = A} =
    +-induction
      (λ x → chtr A)
      id
      (λ no → !𝟘 _ {!∥∥-recursion 𝟘-isProp !})
      (lemℓ (chtr A) ∥∥-isProp)
  -}
  
module Exercise-13
  (lem : LEM∞ {ℓ})
 where

  
  {- 
  AC→ : 𝓤 (ℓ ₊ ⊔ ℓ' ₊)
  AC→ {ℓ} {ℓ'} = (X : 𝓤 ℓ)
      → (Y : X → 𝓤 ℓ')
      → (isSet X)
      → ((x : X) → isSet (Y x))
      → Π (∥_∥ ∘ Y)
      → ∥ Π Y ∥
   -}

  {-
    We're given ϕ : Π[ x ∈ X ] ∥ Y x ∥
    and want ∥ Π[ x ∈ X ] Y x ∥.
    We just construct an element of Π Y
    Given x : X, we need to construct an element of type Y x.
    Applying LEM∞ on Y x, either we already have our element, or ¬ (Y x) holds.
    But this is a functino Y x → 𝟘, with codomain a prop, so yields a function ∥Y x∥ → 𝟘
    and since ϕ x is of type ∥Y x∥, we get a contradiction and we're done.
  -}


  ac : AC→ {ℓ} {ℓ}
  ac X Y XS YS ϕ =  ∣ f ∣
   where
    f : (x : X) → Y x
    f x =
      +-induction
        ( λ c → Y x )
        id
        ( λ nY → !𝟘 _ (∥∥-recursion 𝟘-isProp nY (ϕ x)) )
        ( lem (Y x) ) 
   
module Exercise-14
  (lemℓ : LEM {ℓ})
 where

  ¬¬-isProp : {A : 𝓤 ℓ} → isProp (¬¬ A)
  ¬¬-isProp {A = A} = isProp/¬ (¬ A)

  →¬¬ : {A : 𝓤 ℓ} → A → ¬¬ A
  →¬¬ {A = A} a f = f a 

  ump : {A : 𝓤 ℓ} {B : 𝓤 ℓ}
      → (isProp B) → (A → B) → ¬¬ A → B
  ump {A = A} {B = B} BP f nnA =
    +-induction
      ( λ - → B)
      id
      (λ nB → !𝟘 _ (nnA (nB ∘ f)))
      (lemℓ B BP)

  ¬¬≃∥∥ : (A : 𝓤 ℓ) → (¬¬ A) ≃ ∥ A ∥
  ¬¬≃∥∥ A =
    biimplied-Props→≃
      ¬¬-isProp
      ∥∥-isProp
      (ump ∥∥-isProp (∣_∣))
      (∥∥-recursion ¬¬-isProp →¬¬) 

  
module Exercise-15
  (pr : PROP-RESIZING {ℓ})
 where

  pr→ = Lift-PROP {ℓ}
  pr← = fst pr
  prα = fst (snd pr)
  prβ = snd (snd pr)

  truncP : (A : 𝓤 ℓ) → PROP {ℓ}
  truncP A =
    pr←
      (
        ( Π[ P ∈ PROP {ℓ} ] ((A → fst P) → fst P) )
      ,
        isProp/Π λ P → isProp/Π λ - → snd P
      )

  trunc : (A : 𝓤 ℓ) → 𝓤 ℓ
  trunc A = fst (truncP A)

  trunc-isProp : (A : 𝓤 ℓ) → isProp (trunc A)
  trunc-isProp A = snd (truncP A)
  {- CONFUSION!!!!
  Lifter : (A : 𝓤 ℓ) → Lift (ℓ ₊) (fst (truncP A)) ≡ ( Π[ P ∈ PROP {ℓ} ] ((A → fst P) → fst P) )
  Lifter A = (  Lift (ℓ ₊) (fst (truncP A))            ≡⟨  refl _ ⟩
                fst (Lift-PROP {!(truncP A)!})  ≡⟨ ap fst ({!!}) ⟩
                fst (pr→ (pr← AA))                     ∎) ∙ lol2
   where
    AA =  ( ( Π[ P ∈ PROP {ℓ} ] ((A → fst P) → fst P) ) , isProp/Π λ P → isProp/Π λ - → snd P )
    lol : pr→ (pr← AA) ≡ AA
    lol = prα AA

    lol2 : fst (pr→ (pr← AA)) ≡ ( Π[ P ∈ PROP {ℓ} ] ((A → fst P) → fst P))
    lol2 = ap fst lol
  -}
  -- →trunc : {A : 𝓤 ℓ} → A → trunc A
  -- →trunc {A = A} a = lower {!!}
  -- CONFUSION
  
  -- trunc-UMP : {A : 𝓤 ℓ} {B : 𝓤 ℓ}
  --    → (isProp B) → (A → B) → trunc A → B
  -- trunc-UMP {A = A} {B} BP f F = {!!}


module Exercise-21 (P : 𝓤 ℓ) where

  f : isProp P → (P ≃ ∥ P ∥)
  f PP = ∥∥-of-Prop PP

  g : (P ≃ ∥ P ∥) → isProp P
  g e = isProp/≃ ∥∥-isProp e

  postulate ≃∥∥-isProp : isProp (P ≃ ∥ P ∥)
  -- ≃∥∥-isProp (f₀ , e₀) (f₁ , e₁) = ≡Σ→≡ (isProp/Π (λ - → ∥∥-isProp) f₀ f₁ , {! !})
  {-
     In words:
     * Since ∥P∥ is a prop, f₀ and f₁ must be equal.
     * Now, remains to show e₀ and e₁ are equal after appropriate transport.
     * By 𝕁, suffices to show that for any given f : P → ∥P∥, any e₀ and e₁
       making f biinvertible are equal.
     * If e₀ = ((g₀ , α₀) , (h₀ , β₀)) and e₁ ((g₁ , α₁) , (h₁ , β₁)) are such:
     * By either e₀ or e₁, P is equivalent to ∥P∥, hence a Prop, hence
       g₀ ≡ g₁ ≡ h₀ ≡ h₁.
     * Remains to show α₀ ≡ α₁ and β₀ ≡ β₁
       But since P and ∥P∥ are propositions, if they are inhabited,
       they are contractible.
       Hence for any x, α₀ x ≡ α₁ x, and the same for β.
  -}

  α : f ∘ g ∼ id
  α e = ≃∥∥-isProp _ _

  β : g ∘ f ∼ id
  β PP = isProp-isProp P _ _ 

