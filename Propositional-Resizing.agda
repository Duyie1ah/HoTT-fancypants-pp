-- {-# OPTIONS --without-K --exact-split --safe --allow-unsolved-metas #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`



module Propositional-Resizing where

open import Spartan-MLTT public
open import Basics public
open import Equivalence public
open import Structure-Of-Type-Formers public
open import Props-Etc public

SET : (ℓ : Level) → 𝓤 (ℓ ₊)
SET ℓ = Σ isSet

PROP : {ℓ : Level} → 𝓤 (ℓ ₊)
PROP {ℓ} = Σ isProp

Lift-PROP : PROP {ℓ} → PROP {ℓ ₊}
Lift-PROP {ℓ} (A , AP) = (Lift (ℓ ₊) A , λ x y → ap lift (AP (lower x) (lower y)) )

PROP-RESIZING : {ℓ : Level} → 𝓤 ((ℓ ₊) ₊)
PROP-RESIZING {ℓ} = qinv (Lift-PROP {ℓ})

