-- {-# OPTIONS --without-K --exact-split --safe --allow-unsolved-metas #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`



module Props-Etc where

open import Basics public
open import Equivalence public
open import Structure-Of-Type-Formers public

-- First some stuff about the low levels.

-- Contractible

isCtr : (X : 𝓤 ℓ) → 𝓤 ℓ
--isCtr X = Σ[ x ∈ X ] Π[ y ∈ X ] (x ≡ y)
isCtr X = Σ λ (x : X) → Π (x ≡_)

isCtr#center : {X : 𝓤 ℓ}
             → isCtr X
             → X
isCtr#center (x , xx) = x

isCtr#central : {X : 𝓤 ℓ}
              → (c : isCtr X)
              → Π[ y ∈ X ] (isCtr#center c ≡ y)
isCtr#central (x , xx) = xx

recenter-isCtr : {X : 𝓤 ℓ}
             → isCtr X
             → isCtr X
recenter-isCtr {X = X} (x , xx) = x , λ y → xx x ⁻¹ ∙ xx y

recenter-isCtr/refl-at-center : {X : 𝓤 ℓ}
                              → (c : isCtr X)
                              → isCtr#central (recenter-isCtr c) (isCtr#center (recenter-isCtr c)) ≡ refl ((isCtr#center (recenter-isCtr c)))
recenter-isCtr/refl-at-center {X = X} (x , xx) = ∙/left-⁻¹ (xx x) 

⋆≡ : (x : 𝟙) → ⋆ ≡ x 
⋆≡ =  𝟙-induction _ (refl ⋆)

𝟙-isCtr : isCtr 𝟙
𝟙-isCtr = ⋆ , ⋆≡ 


-- Note: this follows also from `isProp/≃` and the fact that contractibility is equivalent
-- to the conjunction of Prop'ness and inhabitedness:
-- The equivalence preserves propness, and inhabitedness "separately" and we get a Contr.
-- The proof of `isCtr/≃` exactly follows this pattern, but we keep it since we don't have
-- the other tools yet.
isCtr/≃ : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'}
        → isCtr Y → X ≃ Y
        → isCtr X
isCtr/≃ {X = X} {Y = Y} (y , yy) (f , (g , α) , (h , β)) = x , xx
 where
  x = h y
  xx = λ (x' : X) → x        ≡⟨ β x ⁻¹ ⟩
                    h (f x)  ≡⟨ ap h ( yy (f x) ⁻¹ ∙ yy (f x')) ⟩
                    h (f x') ≡⟨ β x' ⟩
                    x'       ∎

-- Makes more sense to do this via the equivalence contractible ⇔ prop∧inhabited
isCtr→≃𝟙 : {X : 𝓤 ℓ} → isCtr X → X ≃ 𝟙
isCtr→≃𝟙 {X = X} (x , xx) = f , (g , α) , (g , β)
 where
  f = λ - → ⋆
  g = λ - → x
  α = ⋆≡ 
  β = λ (x' : X) →  (xx x') 

isCtr/Π : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
        → ((x : X) → isCtr (Y x))
        → isCtr (Π Y)
isCtr/Π {X = X} {Y = Y} C = center , central
 where
  center = λ (x : X) → fst (C x)
  central = λ (ϕ : Π Y) → funext λ x → snd (C x) (ϕ x)

isCtr/× : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'}
        → isCtr X 
        → isCtr Y
        → isCtr (X × Y)
isCtr/× {X = X} {Y = Y} (Xc , Xcc) (Yc , Ycc) = center , central
 where
  center = (Xc , Yc)
  central =  Σ-induction λ x y →  ≡Σ→≡ (Xcc x , (Ycc _ ⁻¹ ∙ Ycc y))


Σ≡-isCtr : {A : 𝓤 ℓ}
         → (a : A)
         → isCtr (Σ (a ≡_))
Σ≡-isCtr {A = A} a = center , centrality
 where
  center = (a , refl a)
  centrality =
    Σ-induction λ x p →
      𝕁 A (λ a x p → (a , refl a) ≡ (x , p)) (λ a → refl (a , refl a)) a x p

Σ≡-isCtr' : {A : 𝓤 ℓ}
         → (a : A)
         → isCtr (Σ (_≡ a))
Σ≡-isCtr' {A = A} a = center , centrality
 where
  center = (a , refl a)
  centrality : (pp : Σ (_≡ a)) → center ≡ pp
  centrality =
    Σ-induction λ x p →
      𝕁 A (λ x a p → (a , refl a) ≡ (x , p)) (λ a → refl (a , refl a)) x a p



-- Lemma 3.11.9
Σ-isCtr-≃-base : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
                 → ((x : X) → isCtr (Y x))
                 → (Σ Y) ≃ X
Σ-isCtr-≃-base {X = X} {Y = Y} C = f , (g , α) , (g , β)
 where
  f : Σ Y → X
  f = fst
  g : X → Σ Y
  g = λ x → x , fst (C x)

  α : f ∘ g ∼ id
  α = refl
  β : g ∘ f ∼ id
  β (x , y) = ≡Σ→≡ (refl _ , snd (C x) y)

Σ-base-isCtr-≃-fiber : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
                     → (c : isCtr X)
                     → (Σ Y) ≃ ( Y (fst c))
-- The proof uses the fact that the equality types on X are props. This is shown further down.




-- Proposition

isProp : (X : 𝓤 ℓ) → 𝓤 ℓ
isProp X = (x₀ x₁ : X) → x₀ ≡ x₁

isCtr→isProp : {X : 𝓤 ℓ} → isCtr X → isProp X
isCtr→isProp {X = X} (x , xx) = λ x₀ x₁ → (xx x₀) ⁻¹ ∙ (xx x₁)

inhabited-isProp→isCtr : {X : 𝓤 ℓ}
                         → (x : X) → isProp X
                         → isCtr X
inhabited-isProp→isCtr {X = X} x ϕ = x , λ y → ϕ x y


𝟘-isProp : isProp 𝟘
𝟘-isProp = λ x₀ x₁ → !𝟘 _ x₀

-- Already defined
-- 𝟙-isProp : isProp 𝟙
-- 𝟙-isProp = isContr→isProp (𝟙-isContr)

isProp/≃ : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'}
        → isProp Y → X ≃ Y
        → isProp X
isProp/≃ {X = X} {Y = Y} yy (f , (g , α) , (h , β)) x₀ x₁ =
  x₀       ≡⟨ β x₀ ⁻¹ ⟩
  h (f x₀) ≡⟨ ap h (yy (f x₀) (f x₁)) ⟩
  h (f x₁) ≡⟨ β x₁ ⟩
  x₁       ∎



maps-to-Prop-homotopic : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
                       → ((x : X) → isProp (Y x))
                       → (f₀ f₁ : Π Y)
                       → f₀ ∼ f₁
maps-to-Prop-homotopic {X} {Y} P f₀ f₁ x = P x (f₀ x) (f₁ x)

biimplied-Props→≃ : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'}
                  → isProp X → isProp Y
                  → (X → Y) → (Y → X)
                  → X ≃ Y
biimplied-Props→≃ {X} {Y} PX PY f g = f , (g , λ y → PY (f (g y)) y) , (g , λ x → PX (g (f x)) x)

inhabitedProp→≃𝟙 : {X : 𝓤 ℓ} → isProp X → X → X ≃ 𝟙
inhabitedProp→≃𝟙 {X = X} PX x = biimplied-Props→≃ {X = X} {Y = 𝟙} PX 𝟙-isProp (λ - → ⋆) (λ - → x)

negatedProp→≃𝟘 : {X : 𝓤 ℓ} → isProp X → ¬ X → X ≃ 𝟘
negatedProp→≃𝟘 {X = X} PX nx = biimplied-Props→≃ {X = X} {Y = 𝟘} PX 𝟘-isProp nx (!𝟘 _)


isProp/Π : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
         → ((x : X) → isProp (Y x))
         → isProp (Π Y)
isProp/Π {X = X} {Y = Y} ϕ f₀ f₁ = funext λ x → (ϕ x) (f₀ x) (f₁ x)

isProp/→ : (X : 𝓤 ℓ) {Y : 𝓤 ℓ'}
         → (isProp Y)
         → isProp (X → Y)
isProp/→ X P = isProp/Π (λ - → P)  

isProp/Π₂ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {Z : (x : X) → Y x → 𝓤 ℓ''}
         → ((x : X) → (y : Y x) → isProp (Z x y))
         → isProp ((x : X) → (y : Y x) → Z x y)
isProp/Π₂ {X = X} {Y = Y} {Z = Z} ϕ = isProp/Π (λ x → isProp/Π (ϕ x))


isProp/× : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'}
         → isProp X → isProp Y
         → isProp (X × Y)
isProp/× {X = X} {Y = Y} XP YP (x₀ , y₀) (x₁ , y₁) = ≡Σ→≡ ((XP x₀ x₁) , (YP _ _))

isProp/Σ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
         → isProp X → ((x : X) → isProp (Y x))
         → isProp (Σ Y)
isProp/Σ {X = X} {Y = Y} XP YP (x₀ , y₀) (x₁ , y₁) = ≡Σ→≡ ((XP x₀ x₁) , (YP x₁ _ _))



isProp/Σ-≡ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
           → ((x : X) → isProp (Y x))
           → {p₀ p₁ : Σ Y}
           → (fst p₀ ≡ fst p₁)
           → p₀ ≡ p₁
isProp/Σ-≡ {X = X} {Y = Y} YP {p₀} {p₁} e = ≡Σ→≡ (e , YP (fst p₁) _ _)
{- isProp/Σ-≡ {X = X} {Y = Y} YP {p₀ = (x₀ , y₀)} {p₁ = (x₁ , y₁)} e =
  𝕁
    X
    (λ x₀ x₁ e → (y₀ : Y x₀) → (y₁ : Y x₁) → (x₀ , y₀) ≡ (x₁ , y₁))
    (λ x y₀ y₁ → ≡Σ→≡ (refl _ , YP x y₀ y₁))
    x₀ x₁ e y₀ y₁  -}


-- Set

isSet : (X : 𝓤 ℓ) → 𝓤 ℓ
isSet X = (x₀ x₁ : X) → isProp (x₀ ≡ x₁)

𝟘-isSet : isSet 𝟘
𝟘-isSet = λ x₀ x₁ → !𝟘 _ x₀

𝟙-isSet : isSet 𝟙
𝟙-isSet x y p q = p       ≡⟨ β p ⁻¹ ⟩
                  h (f p) ≡⟨ ap h (⋆≡ (f p) ⁻¹)  ⟩
                  h ⋆     ≡⟨ ap h (⋆≡ (f q)) ⟩
                  h (f q) ≡⟨ β q ⟩
                  q       ∎
                  
 where
  f : x ≡ y → 𝟙
  f = fst (≡𝟙 x y)
  h : 𝟙 → x ≡ y
  h = fst (snd (snd (≡𝟙 x y)))
  β : h ∘ f ∼ id
  β = snd (snd (snd (≡𝟙 x y)))


isSet/≃ : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'}
        → isSet Y → X ≃ Y
        → isSet X
isSet/≃ {X = X} {Y = Y} σ (f , b) x₀ x₁ =
  isProp/≃ {X = x₀ ≡ x₁} {Y = f x₀ ≡ f x₁}
    (σ (f x₀) (f x₁))
    (ap/≃ {A = X} {B = Y} (f , b) x₀ x₁)


ℕ-isSet : isSet ℕ
ℕ-isSet zero zero         = isProp/≃ 𝟙-isProp (≡-≃-≡ℕ zero zero) 
ℕ-isSet zero (succ n)     = isProp/≃ 𝟘-isProp (≡-≃-≡ℕ zero (succ n))
ℕ-isSet (succ m) zero     = isProp/≃ 𝟘-isProp (≡-≃-≡ℕ (succ m) zero)
ℕ-isSet (succ m) (succ n) = isProp/≃ (ℕ-isSet m n) (succ/≃ m n)



-- Those are all props

isCtr→≡-isProp : {A : 𝓤 ℓ}
               → isCtr A
               → (x₀ x₁ : A)
               → isProp (x₀ ≡ x₁)
isCtr→≡-isProp {A = A} (x , xx) x₀ x₁ p₀ p₁ = (centrality x₀ x₁ p₀) ⁻¹ ∙ (centrality x₀ x₁ p₁) 
 where
  centrality = 
    𝕁
      A
      ( λ a₀ a₁ e → (xx a₀) ⁻¹ ∙ (xx a₁) ≡  e )
      ( λ a → ∙/left-⁻¹ (xx a) )


Σ-base-isCtr-≃-fiber {X = X} {Y = Y} (c , cc) = f , (g , α) , (g , β)
 where
  f : Σ Y → Y c
  g : Y c → Σ Y
  α : f ∘ g ∼ id
  β : g ∘ f ∼ id

  ≡X-isCtr = isCtr→≡-isProp (c , cc)

  -- f (x , y) = tr ((cc x ⁻¹) ∙ (cc c)) y
  f (x , y) = tr ((cc x ⁻¹)) y
  g y = (c , y)
  α y = f (g y) ≡⟨ refl _ ⟩
        f (c , y) ≡⟨ refl _ ⟩
        tr {Y = Y} (cc c ⁻¹) y ≡⟨ ap (λ - → tr {Y = Y}  - y) (≡X-isCtr c c (cc c ⁻¹) (refl c))  ⟩
        tr {Y = Y} (refl c) y ≡⟨ refl _  ⟩
        y ∎
  β (x , y) =
    g (f (x , y))                            ≡⟨ refl _ ⟩
    (c , tr (cc x ⁻¹) y)                     ≡⟨ ≡Σ→≡ (cc x , refl _) ⟩
    (x , tr {Y = Y} (cc x) (tr (cc x ⁻¹) y)) ≡⟨  ap (λ - → x , - y) ( (tr/∙ {Y = Y} (cc x ⁻¹) (cc x) ⁻¹)) ⟩
    (x , tr {Y = Y} (cc x ⁻¹ ∙ cc x) y)      ≡⟨ ap (λ - → x , tr {Y = Y} - y) ( ∙/left-⁻¹ (cc x)) ⟩
    (x , tr {Y = Y} (refl x) y)              ≡⟨ refl _ ⟩
    (x , y)                                  ∎


isProp→isSet : {A : 𝓤 ℓ} → isProp A → isSet A
isProp→isSet {A = A} ϕ a₀ a₁ p₀ p₁ = (lol a₀ a₁ p₀) ⁻¹ ∙ (lol a₀ a₁ p₁)
 where
  lol =
    𝕁
      A
      ( λ a₀ a₁ e → ϕ a₀ a₀ ⁻¹ ∙ ϕ a₀ a₁ ≡  e )
      ( λ a → ∙/left-⁻¹ (ϕ a a) )


isCtr-isProp : (A : 𝓤 ℓ) → isProp (isCtr A)
isCtr-isProp A (x₀ , xx₀) (x₁ , xx₁) =
  ≡Σ→≡
  ( xx₀ x₁
  , funext (λ (x : A) → isCtr→≡-isProp (x₁ , xx₁) x₁ x _ _)
  )


-- Corollary 3.11.15
inhabited-isCtr-isCtr : {A : 𝓤 ℓ} → isCtr A → isCtr (isCtr A)
inhabited-isCtr-isCtr {A = A} γ = inhabited-isProp→isCtr γ (isCtr-isProp A) 

isProp-isProp : (A : 𝓤 ℓ) → isProp (isProp A)
isProp-isProp A ϕ₀ ϕ₁ = funext₂ λ x₀ x₁ → isProp→isSet ϕ₀ x₀ x₁ (ϕ₀ x₀ x₁) (ϕ₁ x₀ x₁)

isSet-isProp : (A : 𝓤 ℓ) → isProp (isSet A)
isSet-isProp A ψ₀ ψ₁ =
  funext₂
    λ x₀ x₁ →
      funext₂
        λ p₀ p₁ →
          isProp→isSet
            (ψ₀ x₀ x₁)
            p₀ p₁
            (ψ₀ x₀ x₁ p₀ p₁)
            (ψ₁ x₀ x₁ p₀ p₁)


embedding : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} → (f : A → B) → 𝓤 (ℓ ⊔ ℓ')
embedding {A = A} f = (a₀ a₁ : A) → isbiinvt (ap {x₀ = a₀} {x₁ = a₁} f) 


Σ-Prop/fst-embedding : {A : 𝓤 ℓ} {B : A → 𝓤 ℓ'}
                    → ((a : A) → isProp (B a))
                    → embedding (fst {X = A} {Y = B})
Σ-Prop/fst-embedding {A = A} {B = B} BP p₀ p₁ =
  tr
    {X = p₀ ≡ p₁ → (fst p₀) ≡ (fst p₁)}
    {Y =  λ ϕ → isbiinvt ϕ }
    (lol p₀ p₁ ⁻¹)
    (snd (≃/tran ≡→≡Σ-≃ ((fst' p₀ p₁ , fst'-≃ p₀ p₁)))) 
 where
  fst' : (p₀ p₁ : Σ B) →  p₀ ≡Σ p₁ → (fst p₀) ≡ (fst p₁)
  fst' p₀ p₁ = fst

  lol : (p₀ p₁ : Σ B) →  ap {x₀ = p₀} {x₁ = p₁} (fst {X = A} {Y = B}) ≡ (fst' p₀ p₁) ∘ ≡→≡Σ
  lol p₀ p₁ = funext λ e →  𝕁 (Σ B) (λ p₀ p₁ e → ap fst e ≡ (fst' p₀ p₁) (≡→≡Σ e) ) (λ p → refl _) p₀ p₁ e

  fst'-≃ : (p₀ p₁ : Σ B) → (isbiinvt (fst' p₀ p₁))
  fst'-≃ p₀ p₁ =  (g , α) , (g , β)
   where
    f = fst' p₀ p₁
    g : fst p₀ ≡ fst p₁ → p₀ ≡Σ p₁
    α : f ∘ g ∼ id
    β : g ∘ f ∼ id
    g e = (e , BP _ _ _)
    α e = refl e
    β (e , ee) = ≡Σ→≡ (refl e , isProp→isSet (BP _) _ _ _ _)
{-
   The goal is to show that the first projection `fst (a , b) ↦ a` induces an equivalence of paths, i.e.
   that for any `(a₀ , b₀)` and `(a₁ , b₁)`,  `ap fst : (a₀ , b₀) ≡ (a₁ , b₁) → a₀ ≡ a₁` is bi-invertible.
   We could (probably) prove it from the ground up, but here we use the fact that we know that:
   * `(ap fst) ≡ fst ∘ ≡→≡Σ` (that's `lol`)
   * `≡→≡Σ` is biinvertible (that's known already)
   * `fst` is biinvertible (that's `fst'-≃`)
-}


Σ-Prop/≡-≃ : {A : 𝓤 ℓ} {B : A → 𝓤 ℓ'}
                    → ((a : A) → isProp (B a))
                    → (p₀ p₁ : Σ B)
                    → (p₀ ≡ p₁) ≃ (fst p₀ ≡ fst p₁)
Σ-Prop/≡-≃ {A = A} {B = B} BP p₀ p₁ = (ap fst , Σ-Prop/fst-embedding BP p₀ p₁)

𝟚-isnotProp : ¬ (isProp 𝟚)
𝟚-isnotProp = λ ϕ → ₀≢₁ (ϕ ₀ ₁)
 where

  code : (x y : 𝟚) → 𝓤₀
  code ₀ ₀ = 𝟙
  code ₁ ₁ = 𝟙
  code ₀ ₁ = 𝟘
  code ₁ ₀ = 𝟘

  enc : (x y : 𝟚) → x ≡ y → code x y
  enc ₁ ₁ (refl ₁) = ⋆
  enc ₀ ₀ (refl ₀) = ⋆

  ₀≢₁ : ₀ ≢ ₁
  ₀≢₁ = λ e → enc ₀ ₁ e

𝔹-isnotProp : ¬ (isProp 𝔹)
𝔹-isnotProp = λ ϕ → 𝕥≢𝕗 (ϕ 𝕥 𝕗)

-- The universe is not a set

𝓤-isnotSet : ¬ (isSet (𝓤₀))
𝓤-isnotSet ϕ = 𝔹≡𝔹-isnotProp (ϕ 𝔹 𝔹)
 where

  s : 𝔹 → 𝔹
  s = 𝔹-recursion _ 𝕗 𝕥

  swap : 𝔹 ≃ 𝔹
  swap = (s , (s , 𝔹-induction _ (refl _) (refl _)) , (s , 𝔹-induction _ (refl _) (refl _)))

  swap≡ : 𝔹 ≡ 𝔹
  swap≡ = ua swap

  swap≡-≢-refl : swap≡ ≢ refl _
  swap≡-≢-refl e =
    𝕥≢𝕗
    (
     𝕥                          ≡⟨ refl _ ⟩
     s 𝕗                        ≡⟨ refl _ ⟩
     (fst swap) 𝕗               ≡⟨ ap (λ - → (fst -) 𝕗) ( ua-α swap ⁻¹) ⟩
     (fst (idtoeqv swap≡)) 𝕗    ≡⟨ ap (λ - → fst (idtoeqv - ) 𝕗) e ⟩
     (fst (idtoeqv (refl 𝔹))) 𝕗 ≡⟨ refl _  ⟩
     𝕗 ∎
   ) 

  𝔹≡𝔹-isnotProp : ¬ (isProp (𝔹 ≡ 𝔹))  
  𝔹≡𝔹-isnotProp = λ ϕ →  swap≡-≢-refl (ϕ swap≡ (refl _))  
  



-- Truncation

record propositional-truncation : Agda.Primitive.Setω where
   field
    ∥_∥                  : {ℓ : Level} → 𝓤 ℓ → 𝓤 ℓ
    ∥∥-isProp            : {ℓ : Level} {X : 𝓤 ℓ} → isProp ∥ X ∥
    ∣_∣                   : {ℓ : Level} {X : 𝓤 ℓ} → X → ∥ X ∥   -- WARNING the symbol ∣ is \mid
    ∥∥-recursion         : {ℓ ℓ' : Level} {X : 𝓤 ℓ} {P : 𝓤 ℓ'}
                         → isProp P → (X → P) → ∥ X ∥ → P
   infix 0 ∥_∥
  
postulate pt : propositional-truncation
open propositional-truncation pt public

∥∥-of-Prop : {X : 𝓤 ℓ}
            → isProp X
            → X ≃ ∥ X ∥
∥∥-of-Prop {X = X} XP = 
  biimplied-Props→≃
    XP
    ∥∥-isProp
    (λ (x : X) → ∣ x ∣)
    (∥∥-recursion XP id)

