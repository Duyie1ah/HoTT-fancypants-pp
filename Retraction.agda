-- {-# OPTIONS --without-K --exact-split --safe --allow-unsolved-metas #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`



module Retraction where

open import Spartan-MLTT public
open import Basics public
open import Equivalence public
open import Structure-Of-Type-Formers public
open import Props-Etc public
open import Propositional-Resizing public
open import Logic public



_≲_ : (A : 𝓤 ℓ) (B : 𝓤 ℓ') → (𝓤 (ℓ ⊔ ℓ'))
A ≲ B = Σ[ s ∈ (A → B) ] Σ[ r ∈ (B → A) ] (r ∘ s ∼ id)

≲/refl : (A : 𝓤 ℓ) → A ≲ A
≲/refl A = (id , id , refl)
≲/tran : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {C : 𝓤 ℓ''} → (A ≲ B) → (B ≲ C) → (A ≲ C)
≲/tran {A = A} {B = B} {C = C} (r₀ , s₀ , α₀) (r₁ , s₁ , α₁) = r₁ ∘ r₀ , s₀ ∘ s₁ , λ x → (s₀ ∘ s₁ ∘ r₁ ∘ r₀) x ≡⟨ (ap s₀  (α₁ (r₀ x))) ⟩  (s₀ ∘ r₀) x ≡⟨ α₀ x  ⟩ x ∎

≲/ap : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
      → (l : A ≲ B)
      → (a₀ a₁ : A)
      → (a₀ ≡ a₁) ≲ (fst l a₀ ≡ fst l a₁)
≲/ap {A = A} {B} (s , r , α) a₀ a₁ = s≡ , r≡ , α≡
 where
  s≡ : a₀ ≡ a₁ → s a₀ ≡ s a₁
  s≡ = ap s
  r≡ : s a₀ ≡ s a₁ → a₀ ≡ a₁
  r≡ p = (α a₀) ⁻¹ ∙ (ap r p) ∙ (α a₁)

  α≡ : r≡ ∘ s≡ ∼ id
  α≡ p = (r≡ ∘ s≡) p                          ≡⟨  refl _ ⟩
         (α a₀) ⁻¹ ∙ (ap r (ap s p)) ∙ (α a₁) ≡⟨ ap (λ - → (α a₀ ⁻¹) ∙ - ∙ (α a₁)) (ap/∘ s r p ⁻¹) ⟩
         (α a₀) ⁻¹ ∙ (ap (r ∘ s) p) ∙ (α a₁)  ≡⟨ ∼/natural' (r ∘ s) (id) α p ⁻¹  ⟩
         (ap (id) p)                          ≡⟨  ap/id p   ⟩
         p                                    ∎


isCtr/≲ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
         → A ≲ B → isCtr B → isCtr A
isCtr/≲ {A = A} {B} (s , r , α) (b , bb) = r b , λ a → r b        ≡⟨ ap r (bb (s a))  ⟩
                                                         r (s a)   ≡⟨ α a ⟩
                                                         a         ∎ 
isLvl/≲ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
         → A ≲ B → (n : ℕ) → isLvl n B
         → isLvl n A
isLvl/≲ {A = A} {B = B} ret zero BC = isCtr/≲ ret BC 
isLvl/≲ {A = A} {B = B} (s , r , α) (succ n) Bn a₀ a₁ = isLvl/≲ (≲/ap (s , r , α) a₀ a₁) n (Bn (s a₀) (s a₁)) 
