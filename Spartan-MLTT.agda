{-# OPTIONS --without-K --exact-split --safe #-}


-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`



module Spartan-MLTT where

open import Universes public

variable
  ℓ ℓ' ℓ'' ℓ''' : Level


data 𝟙 : 𝓤₀ where
 ⋆ : 𝟙

𝟙-induction : (A : 𝟙 → 𝓤  ℓ ) → A ⋆ → (x : 𝟙) → A x
𝟙-induction A a ⋆ = a

𝟙-recursion : (B : 𝓤 ℓ ) → B → (𝟙 → B)
𝟙-recursion B b x = 𝟙-induction (λ _ → B) b x

data 𝟘 :  𝓤₀ where

𝟘-induction : (A : 𝟘 → 𝓤 ℓ) → (x : 𝟘) → A x
𝟘-induction A ()

𝟘-recursion : (A : 𝓤 ℓ) → 𝟘 → A
𝟘-recursion A a = 𝟘-induction (λ _ → A) a
!𝟘 = 𝟘-recursion

is-empty : 𝓤 ℓ →  𝓤 ℓ
is-empty X = X → 𝟘

¬ : 𝓤 ℓ →  𝓤 ℓ
¬ X = X → 𝟘

¬¬ : 𝓤 ℓ → 𝓤 ℓ
¬¬ X = ¬ (¬ X)

¬¬¬ : 𝓤 ℓ → 𝓤 ℓ
¬¬¬ X = ¬ (¬ (¬ X))

data 𝔹 : 𝓤₀ where
 𝕥 : 𝔹
 𝕗 : 𝔹

𝔹-induction : (A : 𝔹 → 𝓤 ℓ) → (A 𝕥) → (A 𝕗) → (b : 𝔹) → A b
𝔹-induction A if else 𝕥 = if 
𝔹-induction A if else 𝕗 = else 

𝔹-recursion : (A : 𝓤 ℓ) → A → A → (b : 𝔹) → A
𝔹-recursion A if else v = 𝔹-induction (λ - → A) if else v


data ℕ : 𝓤₀ where
 zero : ℕ
 succ : ℕ → ℕ

ℕ-induction : (A : ℕ → 𝓤 ℓ)
            → A zero
            → ((n : ℕ) → A n → A (succ n))
            → (n : ℕ) → A n

ℕ-induction A a₀ f = h
 where
  h : (n : ℕ) → A n
  h zero        = a₀
  h (succ n) = f n (h n)

ℕ-recursion : (X : 𝓤 ℓ)
            → X
            → (ℕ → X → X)
            → ℕ → X

ℕ-recursion X = ℕ-induction (λ _ → X)

ℕ-iteration : (X : 𝓤 ℓ)
            → X
            → (X → X)
            → ℕ → X

ℕ-iteration X x f = ℕ-recursion X x (λ _ x → f x)

one two : ℕ
one = succ zero
two = succ one


data _+_ {ℓ ℓ'} (X : 𝓤 ℓ) (Y : 𝓤 ℓ') : 𝓤 (ℓ ⊔ ℓ') where
 inl : X → X + Y
 inr : Y → X + Y

+-induction : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} (A : X + Y → 𝓤 ℓ'')
            → ((x : X) → A (inl x))
            → ((y : Y) → A (inr y))
            → (z : X + Y) → A z

+-induction A f g (inl x) = f x
+-induction A f g (inr y) = g y

+-recursion : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {A : 𝓤 ℓ''} → (X → A) → (Y → A) → X + Y → A
+-recursion {ℓ} {ℓ'} {𝓦} {X} {Y} {A} = +-induction (λ _ → A)

𝟚 : 𝓤₀
𝟚 = 𝟙 + 𝟙

pattern ₀ = inl ⋆
pattern ₁ = inr ⋆

𝟚-induction : (A : 𝟚 → 𝓤 ℓ) → A ₀ → A ₁ → (n : 𝟚) → A n
𝟚-induction A a₀ a₁ ₀ = a₀
𝟚-induction A a₀ a₁ ₁ = a₁

𝟚-induction' : (A : 𝟚 → 𝓤 ℓ) → A ₀ → A ₁ → (n : 𝟚) → A n
𝟚-induction' A a₀ a₁ = +-induction A
                         (𝟙-induction (λ (x : 𝟙) → A (inl x)) a₀)
                         (𝟙-induction (λ (y : 𝟙) → A (inr y)) a₁)



record Σ {ℓ ℓ'} {X : 𝓤 ℓ} (Y : X → 𝓤 ℓ') : 𝓤 ((ℓ ⊔ ℓ')) where
  no-eta-equality -- To be closer to the HoTT book 
  pattern         -- I added this `pattern` to be able to define `pr₁` and `pr₂` in spite of `no-eta-equality` → is that wrong? 
  constructor
   _,_
  field
   x : X
   y : Y x

pr₁ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} → Σ Y → X
pr₁ (x , y) = x

pr₂ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} → (z : Σ Y) → Y (pr₁ z)
pr₂ (x , y) = y

-Σ : {ℓ ℓ' : Level} (X : 𝓤 ℓ) (Y : X → 𝓤 ℓ') → 𝓤 (ℓ ⊔ ℓ')
-Σ X Y = Σ Y

+Σ : {ℓ ℓ' : Level} (X : 𝓤 ℓ) (Y : X → 𝓤 ℓ') → 𝓤 (ℓ ⊔ ℓ')
+Σ X Y = Σ Y

syntax -Σ X (λ x → y) = Σ x ꞉ X , y

-- notation stolen from 1lab
syntax +Σ X (λ x → y) = Σ[ x ∈ X ]  y

Σ-induction : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {A : Σ Y → 𝓤 ℓ''}
            → ((x : X) (y : Y x) → A (x , y))
            → (p : Σ Y) → A p

Σ-induction g (x , y) = g x y

curry : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {A : Σ Y → 𝓤 ℓ''}
      → (((x , y) : Σ Y) → A (x , y))
      → ((x : X) (y : Y x) → A (x , y))

curry f x y = f (x , y)

_×_ : 𝓤 ℓ → 𝓤 ℓ' → 𝓤 (ℓ ⊔ ℓ') 
X × Y = Σ x ꞉ X , Y

Σ-recursion : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {A : 𝓤 ℓ''}
            → ((x : X) (y : Y x) → A)
            → Σ Y → A
Σ-recursion {X = X} {Y = Y} {A = A} f p = Σ-induction {X = X} {Y = Y} {A = λ - → A} f p     

Π : {X : 𝓤 ℓ} (A : X → 𝓤 ℓ') → 𝓤 (ℓ ⊔ ℓ')
Π {ℓ} {ℓ'} {X} A = (x : X) → A x

-Π : {ℓ ℓ' : Level} (X : 𝓤 ℓ) (Y : X → 𝓤 ℓ') → 𝓤 (ℓ ⊔ ℓ')
-Π X Y = Π Y

+Π : {ℓ ℓ' : Level} (X : 𝓤 ℓ) (Y : X → 𝓤 ℓ') → 𝓤 (ℓ ⊔ ℓ')
+Π X Y = Π Y

syntax -Π A (λ x → b) = Π x ꞉ A , b

-- notation stolen from 1lab
syntax +Π X (λ x → y) = Π[ x ∈ X ]  y

id : {X : 𝓤 ℓ} → X → X
id x = x

𝑖𝑑 : (X : 𝓤 ℓ) → X → X
𝑖𝑑 X = id

_∘_ : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} {Z : Y → 𝓤 ℓ''}
    → ((y : Y) → Z y)
    → (f : X → Y)
    → (x : X) → Z (f x)

g ∘ f = λ x → g (f x)

domain : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} → (X → Y) →  𝓤 ℓ
domain {ℓ} {ℓ'} {X} {Y} f = X

codomain : {X : 𝓤 ℓ} {Y : 𝓤 ℓ'} → (X → Y) → 𝓤 ℓ' 
codomain {ℓ} {ℓ'} {X} {Y} f = Y

type-of : {X : 𝓤 ℓ} → X →  𝓤 ℓ
type-of {ℓ} {X} x = X

data Id {ℓ} (X : 𝓤 ℓ) : X → X → 𝓤 ℓ where
 refl : (x : X) → Id X x x

_≡_ : {X : 𝓤 ℓ} → X → X →  𝓤 ℓ
x ≡ y = Id _ x y

_≢_ : {X : 𝓤 ℓ} → X → X →  𝓤 ℓ
x ≢ y = ¬ (x ≡ y)

𝕁 : (X : 𝓤 ℓ) (A : (x y : X) → x ≡ y → 𝓤 ℓ')
  → ((x : X) → A x x (refl x))
  → (x y : X) (p : x ≡ y) → A x y p

𝕁 X A f x x (refl x) = f x

𝕁' : {X : 𝓤 ℓ} {A : (x y : X) → x ≡ y → 𝓤 ℓ'}
  → ((x : X) → A x x (refl x))
  → (x y : X) (p : x ≡ y) → A x y p
𝕁' {X = X} {A = A} a x y p = 𝕁 X A a x y p

lhs : {X : 𝓤 ℓ} {x y : X} → x ≡ y → X
lhs {ℓ} {X} {x} {y} p = x

rhs : {X : 𝓤 ℓ} {x y : X} → x ≡ y → X
rhs {ℓ} {X} {x} {y} p = y

{-
ℍ : {X : 𝓤 ℓ} (x : X) (B : (y : X) → x ≡ y → 𝓤 ℓ')
  → B x (refl x)
  → (y : X) (p : x ≡ y) → B y p

ℍ x B b x (refl x) = b
-}



-- All copied from Escardo
record Lift {ℓ : Level} (ℓ' : Level) (X : 𝓤  ℓ) : 𝓤 (ℓ ⊔ ℓ') where
 constructor
  lift
 field
  lower : X

open Lift public

-- The functions Lift, lift and lower have the following types:

type-of-Lift  :             type-of (Lift  {ℓ} ℓ')       ≡ (𝓤 ℓ → 𝓤 (ℓ ⊔ ℓ'))
type-of-lift  : {X : 𝓤 ℓ} → type-of (lift  {ℓ} {ℓ'} {X}) ≡ (X → Lift ℓ' X)
type-of-lower : {X : 𝓤 ℓ} → type-of (lower {ℓ} {ℓ'} {X}) ≡ (Lift ℓ' X → X)

type-of-Lift  = refl _
type-of-lift  = refl _
type-of-lower = refl _

-- The induction and recursion principles are as follows:

Lift-induction : ∀ {ℓ} ℓ' (X : 𝓤 ℓ) (A : Lift ℓ' X → 𝓤 ℓ'')
               → ((x : X) → A (lift x))
               → (l : Lift ℓ' X) → A l

Lift-induction ℓ' X A φ (lift x) = φ x


Lift-recursion : ∀ {ℓ} ℓ' {X : 𝓤 ℓ} {B : 𝓤 ℓ''}
               → (X → B) → Lift ℓ' X → B

Lift-recursion ℓ' {X} {B} = Lift-induction ℓ' X (λ _ → B)


lower-lift : {X : 𝓤 ℓ} (x : X) → lower {ℓ} {ℓ'} (lift x) ≡ x
lower-lift = refl


lift-lower : {X : 𝓤 ℓ} (l : Lift ℓ' X) → lift (lower l) ≡ l
lift-lower = refl




infixr 50 _,_
infixr 30 _×_
infixr 20 _+_
infixl 70 _∘_
infix   0 Id
infix   0 _≡_
infixr -1 -Σ
infixr -1 -Π




