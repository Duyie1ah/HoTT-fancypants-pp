-- {-# OPTIONS --without-K --exact-split --safe --allow-unsolved-metas #-}
{-# OPTIONS --without-K --exact-split #-}


-- Convention: `fun/prop` corresponds to the term `fun` having property `prop`. 

-- This uses the base from Escardo's notes/source,
-- i.e. what serves as the "basic rules" for HoTT.
-- Then, I try to follow the HoTT book without cheating.
-- Page numbers are wrt the HoTT book, version
--   `first-edition-1005-ge9c58d7`


-- module should cover, for each type former, it's "funcoriality" wrt equality, its "intrinsic" equality type, and so on, as in
-- $2.6 p. 105 and following $s of the book

module Structure-Of-Type-Formers where

open import Basics public
open import Equivalence public

-- Dependent sums: Σ

-- Equality for dependent sums

_≡Σ_ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} (t₀ t₁ : Σ Y) → 𝓤 (ℓ ⊔ ℓ')
_≡Σ_ {X = X} {Y} t₀ t₁ = Σ λ (e : fst t₀ ≡ fst t₁) →  ((tr {X = X} {Y = Y} e (snd t₀)) ≡ snd t₁)

≡→≡Σ  : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {t₀ t₁ : Σ Y} → t₀ ≡ t₁ → t₀ ≡Σ t₁
≡→≡Σ {X = X} {Y} {t₀} {t₁} p =
  𝕁
    (Σ Y)
    (λ t₀ t₁ p → t₀ ≡Σ t₁)
    -- (λ (t : Σ Y) → (refl (fst t) , tr/refl-at {X = X} {Y = Y} (fst t) (snd t))) -- refl-at is defeq `refl` so no need for it
    (λ (t : Σ Y) → (refl (fst t) , refl (snd t)))
    t₀ t₁ p

≡Σ→≡ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {t₀ t₁ : Σ Y} → t₀ ≡Σ t₁ → t₀ ≡ t₁
≡Σ→≡ {X = X} {Y} {t₀} {t₁} e =
  Σ-induction {Y = Y} {A = λ t₀ → (t₁ : Σ Y) → (e : t₀ ≡Σ t₁) → t₀ ≡ t₁}
    ( λ (x₀ : X) (y₀ : Y x₀) →
      Σ-induction {Y = Y} {A = λ (t₁ : Σ Y) → (e : (x₀ , y₀) ≡Σ t₁) → (x₀ , y₀) ≡ t₁}
      ( λ (x₁ : X) (y₁ : Y x₁) →
        Σ-induction {Y = λ (e : x₀ ≡ x₁) → tr e y₀ ≡ y₁} {A = λ (e : (x₀ , y₀) ≡Σ (x₁ , y₁)) → (x₀ , y₀) ≡ (x₁ , y₁)}
        ( λ (e : x₀ ≡ x₁) (f : tr e y₀ ≡ y₁) →
          𝕁
            X
            ( λ x₀ x₁ e → (y₀ : Y x₀) → (y₁ : Y x₁) → (f : tr e y₀ ≡ y₁) → (x₀ , y₀) ≡ (x₁ , y₁))
            ( λ x →
              𝕁
                ( Y x )
                ( λ (y₀ : Y x) (y₁ : Y x) (f : y₀ ≡ y₁) → (x , y₀) ≡ (x , y₁) )
                ( λ y → refl _ )
            )
            x₀ x₁ e y₀ y₁ f
        ) 
      )
    )
    t₀ t₁ e


≡Σ→≡/refl,refl : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
               → (x : X) (y : Y x)
               → ≡Σ→≡ {X = X} {Y = Y} {t₀ = x , y} {t₁ = x , y} (refl x , refl y) ≡ refl (x , y)
≡Σ→≡/refl,refl x y = refl _ 
               
≡→≡Σ/refl-pair : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
               → (x : X) (y : Y x)
               → ≡→≡Σ {X = X} {Y = Y} {t₀ = x , y} {t₁ = x , y} (refl (x , y)) ≡ (refl x , refl y)
≡→≡Σ/refl-pair x y = refl _ 

≡Σ→≡' : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {t₀ t₁ : Σ Y} → t₀ ≡Σ t₁ → t₀ ≡ t₁
≡Σ→≡' {X = X} {Y} {t₀} {t₁} e = (Σ-uniq t₀) ∙ pairs≡ ∙  (Σ-uniq t₁) ⁻¹
 where

  u₀ : Σ Y
  u₀ = (fst t₀ , snd t₀)
  u₁ : Σ Y
  u₁ = (fst t₁ , snd t₁)

  pairs≡ : u₀ ≡ u₁
  pairs≡ =
    𝕁
      X
      (λ a₀' a₁' α → (b₀' : Y a₀') → (b₁' : Y a₁') → (r : (tr {X = X} {Y = Y} α b₀') ≡ b₁') → (a₀' , b₀') ≡ (a₁' , b₁'))
      (λ a b₀' b₁' r →
        𝕁
          (Y a)
          (λ b₀'' b₁'' β → (a , b₀'') ≡ (a , b₁''))
          (λ b → refl (a , b))
          b₀' b₁' (tr {X = Y a} {Y = _≡ b₁' } (tr/refl-at {X = X} {Y = Y} a b₀') r)
      )
      (fst t₀) (fst t₁) (fst e)
      (snd t₀) (snd t₁) (snd e)

≡→≡Σ-α : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {t₀ t₁ : Σ Y} →(≡→≡Σ {t₀ = t₀} {t₁ = t₁}) ∘ ≡Σ→≡ ∼ id
≡→≡Σ-α {X = X} {Y = Y} {t₀} {t₁} =
  Σ-induction {A = λ (t₀ : Σ Y) → (t₁ : Σ Y) → (e : t₀ ≡Σ t₁) → ≡→≡Σ {t₀ = t₀} {t₁ = t₁} (≡Σ→≡ e) ≡ e}
    ( λ x₀ y₀ →
      Σ-induction -- {A = λ (t₁ : Σ Y) → (e : (x₀ , y₀) ≡Σ t₁) → ≡→≡Σ {t₀ = x₀ , y₀} {t₁ = t₁} (≡Σ→≡ e) ≡ e}
        ( λ x₁ y₁ →
          Σ-induction -- {A = λ (e : (x₀ , y₀) ≡Σ (x₁ , y₁)) → ≡→≡Σ {t₀ = x₀ , y₀} {t₁ = x₁ , y₁} (≡Σ→≡ e) ≡ e}
            ( λ e f →
              𝕁
                X
                ( λ x₀ x₁ e → (y₀ : Y x₀) → (y₁ : Y x₁) → (f : tr e y₀ ≡ y₁)
                            → ≡→≡Σ {t₀ = x₀ , y₀} {t₁ = x₁ , y₁} (≡Σ→≡ (e , f)) ≡ (e , f))
                (λ x → 𝕁 (Y x) _ (λ y → refl _))
                x₀ x₁ e y₀ y₁ f
            )
        )
    )
    t₀ t₁

≡→≡Σ-β : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {t₀ t₁ : Σ Y} →(≡Σ→≡ {t₀ = t₀} {t₁ = t₁}) ∘ ≡→≡Σ ∼ id
≡→≡Σ-β {X = X} {Y = Y} {t₀} {t₁} e =
  𝕁
    ( Σ Y )
    ( λ t₀ t₁ e → ≡Σ→≡ {t₀ = t₀} {t₁ = t₁} (≡→≡Σ e) ≡ e )
    ( Σ-induction {A = λ (t : Σ Y) →   ≡Σ→≡ {t₀ = t} {t₁ = t} (≡→≡Σ (refl t)) ≡ (refl t)}
        ( λ x y →  refl _)
    )
    t₀ t₁ e


≡→≡Σ/qinv : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {t₀ t₁ : Σ Y} → qinv (≡→≡Σ {t₀ = t₀} {t₁ = t₁})
≡→≡Σ/qinv {X = X} {Y = Y} {t₀} {t₁} = (≡Σ→≡ {t₀ = t₀} {t₁} , ≡→≡Σ-α {t₀ = t₀} {t₁} , ≡→≡Σ-β {t₀ = t₀} {t₁}) 

Σ-uniq' : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} (t : Σ Y) → t ≡ (fst t , snd t)
Σ-uniq' t = ≡Σ→≡ {t₀ = t} {t₁ = (fst t , snd t)} (refl _ , refl _)

≡→≡Σ-≃ :  {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {t₀ t₁ : Σ Y}
       → (t₀ ≡ t₁) ≃ (t₀ ≡Σ t₁) 
≡→≡Σ-≃ {X = X} {Y} {t₀} {t₁} = (≡→≡Σ , qinv→isbiinvt ≡→≡Σ/qinv) 

pair= = ≡Σ→≡

{- Type confusion!!!

-- p. 111
-- Theorem 2.7.4

Σ/tr : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {A : Σ Y → 𝓤 ℓ''} {x₀ x₁ : X}
     → (p : x₀ ≡ x₁) (u₀ : Y x₀) (z₀ : A (x₀ , u₀))
     → tr {X = X} {Y = λ x → Σ[ u ∈ Y x ] A (x , u) }
          p (u₀ , z₀)
       ≡ (tr {X = X} {Y = Y} p u₀ , ≡Σ→≡ {t₀ = ()} (p , tr (refl (tr p u₀)) z₀))
Σ/tr {X = X} {Y} {A} {x₀} {x₁} p u₀ z₀ = ?
-}

-- p. 111
-- «We leave it as an exercise to the reader to …»
Σ-fun : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
      → {X' : 𝓤 ℓ} {Y' : X' → 𝓤 ℓ'}
      → (f : X → X') → (g : (x : X) → Y x → Y' (f x))
      → Σ Y → Σ Y'
Σ-fun {X = X} {Y} {X'} {Y'} f g = Σ-induction (λ (x : X) (y : Y x) → (f x , g x y))

≡Σ/ap : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
      → {X' : 𝓤 ℓ} {Y' : X' → 𝓤 ℓ'}
      → (f : X → X') → (g : (x : X) → (Y x) → Y' (f x))
      → {x₀ : X} {y₀ : Y x₀} {x₁ : X} {y₁ : Y x₁} -- {t₀ t₁ : Σ Y}
      → (e : x₀ ≡ x₁) → (h : tr e y₀ ≡ y₁)
      →   ap (Σ-fun {X = X} {Y} {X'} {Y'} f g) (≡Σ→≡ {t₀ = x₀ , y₀} {t₁ = x₁ , y₁} (e , h))
        ≡
          ≡Σ→≡ {t₀ = Σ-fun f g (x₀ , y₀)}
               {t₁ = Σ-fun {X = X} {Y} {X'} {Y'} f g (x₁ , y₁)}
               ( ap {X = X} {Y = X'} f e
               , ( tr (ap f e) (g x₀ y₀) ≡⟨ tr/ap-at f e (g x₀ y₀) ⟩
                   tr e (g x₀ y₀)        ≡⟨ tr/tr {X = X} {Y} {X'} {Y'} f g e ⟩
                   g x₁ (tr e y₀)        ≡⟨ ap {X = Y x₁} {Y' (f x₁)} (g x₁) h ⟩
                   g x₁ y₁               ∎ ) 
               )
≡Σ/ap {X = X} {Y} {X'} {Y'} f g {x} {y} {x} {y} (refl x) (refl y) = refl _
-- We cheat here because we're too lazy but it's just applying 𝕁 twice

-- TODO : make it such that `Σ≡/ap` accepts `t₀ t₁ : Σ Y` and `p : t₀ ≡Σ t₁` instead of the deconstructed values taken now.
-- Should be easy to construct, simply by induction, but the type risks getting uglier ?

≡Σ/ap' : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
      → {X' : 𝓤 ℓ} {Y' : X' → 𝓤 ℓ'}
      → (f : X → X') → (g : (x : X) → (Y x) → Y' (f x))
      → {t₀ t₁ : Σ Y}
      → (p : t₀ ≡Σ t₁)
      → type-of (≡Σ/ap {X = X} {Y} {X'} {Y'} f g {fst t₀} {snd t₀} {fst t₁} {snd t₁} (fst p) (snd p))
      -- haha I'm clever! not sure exactly why the types are correct though
≡Σ/ap' {X = X} {Y} {X'} {Y'} f g {(x₀ , y₀)} {(x₁ , y₁)} (e , h) = ≡Σ/ap {X = X} {Y} {X'} {Y'} f g {x₀} {y₀} {x₁} {y₁} e h

{-
I can't get this thing below to work, so I cheat above
≡Σ/ap' {X = X} {Y} {X'} {Y'} f g {t₉} {t₁} p =
  Σ-induction
    ( λ (x₀ : X) (y₀ : Y x₀) →
      Σ-induction
        ( λ (x₁ : X) (y₁ : Y x₁) →
          Σ-induction
            ( λ (e : x₀ ≡ x₁) (h : tr e y₀ ≡ y₁) →
              ≡Σ/ap {X = X} {Y} {X'} {Y'} f g {x₀} {y₀} {x₁} {y₁} e h
            )
        )
    )
    t₀ t₁ p
-}



Σ/fibers-≃ : {A : 𝓤 ℓ} {B₀ : A → 𝓤 ℓ'} {B₁ : A → 𝓤 ℓ''}
           → ((a : A) → B₀ a ≃ B₁ a)
           → (Σ B₀) ≃ (Σ B₁)
Σ/fibers-≃ {A = A} {B₀} {B₁} BE = f , (g , α) , (h , β)
 where
  f : Σ B₀ → Σ B₁
  f (a , b) = a , fst (BE a) b
  g : Σ B₁ → Σ B₀
  g (a , b) = a , fst (fst (snd (BE a))) b
  h : Σ B₁ → Σ B₀
  h (a , b) = a , fst (snd (snd (BE a))) b

  α : f ∘ g ∼ id
  α (a , b) =  ≡Σ→≡ (refl a , snd (fst (snd (BE a))) b)
  β : h ∘ f ∼ id
  β (a , b) = ≡Σ→≡ (refl a , snd (snd (snd (BE a))) b )


≡×→≡ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} {p₀ p₁ : A × B} → (fst p₀ ≡ fst p₁) × (snd p₀ ≡ snd p₁) → p₀ ≡ p₁
≡×→≡ {A = A} {B} {(a , b)} {(a , b)}  (refl a , refl b) = refl (a , b) 

×/≃ : {A A' : 𝓤 ℓ} {B B' : 𝓤 ℓ'}
     → (A ≃ A') → (B ≃ B')
     → (A × B) ≃ A' × B'
×/≃ {A = A} {A'} {B} {B'} (Af , (Ag , Aα) , (Ah , Aβ)) (Bf , (Bg , Bα) , (Bh , Bβ)) = (f , (g , α) , (h , β))
 where
  f : A × B → A' × B'
  f (a , b) = Af a , Bf b
  g : A' × B' → A × B
  g (a , b) = Ag a , Bg b
  h : A' × B' → A × B
  h (a , b) = Ah a , Bh b
  α : f ∘ g ∼ id
  α (a , b) = ≡×→≡ (Aα a , Bα b)
  β : h ∘ f ∼ id
  β (a , b) = ≡×→≡ (Aβ a , Bβ b)

-- Preservation of ≃

{- MUCH TOO COMPLICATED I'M GETTING LOST!!!!!
Σ/of-≃ : {A A' : 𝓤 ℓ} {B : A → 𝓤 ℓ'} {B' : A' → 𝓤 ℓ''}
       → (e : A ≃ A') → ((x : A) → B x ≃  B' (fst e x))
       → Σ B ≃ Σ B'
Σ/of-≃ {A = A} {A'} {B} {B'} e eu = F , qinv→isbiinvt Q
 where


  f : A → A'
  f = fst e
  q = isbiinvt→qinv (snd e)

  g : A' → A
  g = qinv#g q
  α : f ∘ g ∼ id
  α = qinv#α q
  β : g ∘ f ∼ id
  β = qinv#β q

  fu : (x : A) → B x → B' (f x)
  fu = λ x → fst (eu x)
  qu : (x : A) → qinv (fu x)
  qu = λ x → isbiinvt→qinv (snd (eu x))
  
  gu : (x : A) → B' (f x) → B x
  gu = λ x → qinv#g (qu x)
  αu : (x : A) → (fu x ∘ gu x) ∼ id 
  αu = λ x → qinv#α (qu x)
  βu : (x : A) → (gu x ∘ fu x) ∼ id 
  βu = λ x → qinv#β (qu x)
 
  
  F : Σ B → Σ B'
  F = Σ-induction (λ a b → f a , fu a b )

  G : Σ B' → Σ B
  G = Σ-induction (λ a' b' → g a' , gu (g a') (tr {X = A'} {Y = B'} (α a' ⁻¹) b'))

  αα : F ∘ G ∼ id
  αα = Σ-induction
       ( λ a' b' →
           (F ∘ G) (a' , b') ≡⟨ refl _ ⟩
           F (g a' ,  gu (g a') (tr {X = A'} {Y = B'} (α a' ⁻¹) b')) ≡⟨ refl _ ⟩
           (f (g a') , fu (g a') (gu (g a') (tr {X = A'} {Y = B'} (α a' ⁻¹) b'))) ≡⟨ ≡Σ→≡( α a' , refl _) ⟩
           (a' , tr (α a') (fu (g a') (gu (g a') (tr {X = A'} {Y = B'} (α a' ⁻¹) b')))) ≡⟨ {!tr/tr f fu (α a')!} ⟩
           (a' , fu (g (f (g a'))) (gu (g (f (g a')))) (tr (α (g (f a')) ⁻¹) (tr (α a') b') ) ????
           (a' , b') ∎
           
       )
  
  
  ββ : G ∘ F ∼ id
  ββ = {!!}

  Q = G , αα , ββ
-}

-- The unit type 𝟙 

≡𝟙 : (x y : 𝟙) → (x ≡ y) ≃ 𝟙
≡𝟙 x y = L→R x y , qinv→isbiinvt (R→L x y , R→R x y , L→L x y)
 where
  L = (x y : 𝟙) → (x ≡ y)

  L→R : (x y : 𝟙) → (x ≡ y) → 𝟙
  L→R x x (refl x) = ⋆

  R→L : (x y : 𝟙) → 𝟙 → (x ≡ y)
  R→L ⋆ ⋆ ⋆ = refl ⋆ 

  R→R : (x y : 𝟙) → (p : 𝟙) → L→R x y (R→L x y p) ≡ p
  R→R ⋆ ⋆ ⋆ = refl _

  L→L : (x y : 𝟙) → (e : x ≡ y) → R→L x y (L→R x y e) ≡ e
  -- L→L x x (refl x) = 𝟙-induction (λ x →  R→L x x (L→R x x (refl x)) ≡ refl x ) (refl _) x 
  L→L ⋆ ⋆ (refl ⋆) = refl _

-- Corollary
-- This could be proven using the equivalence above: given x and y, the preimage of ⋆ through the equivalence
-- is a path x ≡ y
𝟙-isProp : (x y : 𝟙) → x ≡ y
𝟙-isProp = 𝟙-induction (λ x → (y : 𝟙) → x ≡ y) (𝟙-induction _ (refl ⋆))
      

-- Dependent functions : Π

≡→≡Π : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
     → {f₀ f₁ : Π Y}
     → f₀ ≡ f₁
     → f₀ ≡Π f₁
≡→≡Π {X = X} {Y = Y} {f₀} {f₁} e = 𝕁 (Π Y) (λ f₀ f₁ e → f₀ ≡Π f₁) (≡Π/refl) f₀ f₁ e

happly = ≡→≡Π

FunctionExtensionality : 𝓤 ((ℓ ⊔ ℓ') ₊)
FunctionExtensionality {ℓ} {ℓ'} = {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {f₀ f₁ : Π Y} → qinv (happly {f₀ = f₀} {f₁ = f₁})

postulate FunExt : {ℓ : Level} {ℓ' : Level} → FunctionExtensionality {ℓ} {ℓ'}

≡Π→≡ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
     → {f₀ f₁ : Π Y}
     → f₀ ≡Π f₁ → f₀ ≡ f₁
≡Π→≡ {ℓ} {ℓ'} {X = X} {Y = Y} {f₀} {f₁} = (fst (FunExt {ℓ} {ℓ'} {X = X} {Y} {f₀} {f₁}))

funext = ≡Π→≡

funext-≃ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'} {f₀ f₁ : Π Y} →  (f₀ ≡ f₁) ≃ (f₀ ∼ f₁) 
funext-≃ = happly , qinv→isbiinvt FunExt

≡→≡Π-α : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
       → {f₀ f₁ : Π Y}
       →  ≡→≡Π ∘ ≡Π→≡ ∼ 𝑖𝑑 (f₀ ≡Π f₁)
≡→≡Π-α {ℓ} {ℓ'} {X = X} {Y = Y} {f₀} {f₁} = fst (snd ( FunExt {ℓ} {ℓ'} {X = X} {Y} {f₀} {f₁} ))

funext-α = ≡→≡Π-α

≡→≡Π-β : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
       → {f₀ f₁ : Π Y}
       →  ≡Π→≡ ∘ ≡→≡Π ∼ 𝑖𝑑 (f₀ ≡ f₁)
≡→≡Π-β {ℓ} {ℓ'} {X = X} {Y = Y} {f₀} {f₁} = snd (snd ( FunExt {ℓ} {ℓ'} {X = X} {Y} {f₀} {f₁} ))

funext-β = ≡→≡Π-β

funext/refl : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
       → (f : Π Y)
       → refl f ≡ funext (≡Π/refl f)
funext/refl f =  (funext-β (refl f)) ⁻¹

funext/⁻¹ : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
          → {f₀ f₁ : Π Y}
          → (e : f₀ ≡ f₁)
          → e ⁻¹ ≡ funext (≡Π/symm (happly e))
funext/⁻¹ {Y = Y} {f₀} {f₁} e =
  𝕁
    ( Π Y )
    ( λ f₀ f₁ e → e ⁻¹ ≡ funext (≡Π/symm (happly e)) )
    ( λ f →  funext-β (refl f ⁻¹) ⁻¹ )
    f₀ f₁ e 

funext/∙  : {X : 𝓤 ℓ} {Y : X → 𝓤 ℓ'}
          → {f₀ f₁ f₂ : Π Y}
          → (e₀ : f₀ ≡ f₁) (e₁ : f₁ ≡ f₂)
          → e₀ ∙ e₁ ≡ funext (≡Π/tran (happly e₀) (happly e₁))
funext/∙ {Y = Y} {f₀} {f₁} {f₂} e₀ e₁ = 
  𝕁
    ( Π Y )
    ( λ f₀ f₁ e₀ → (f₂ : Π Y) → (e₁ : f₁ ≡ f₂) → e₀ ∙ e₁ ≡ funext (≡Π/tran (happly e₀) (happly e₁)) )
    ( λ f → λ f₂ e₁ →  (funext-β e₁) ⁻¹)
    f₀ f₁ e₀ f₂ e₁


funext₂ : {X : 𝓤 ℓ } {Y : X → 𝓤 ℓ'} {Z : (x : X) → Y x → 𝓤 ℓ''}
        → {f₀ f₁ : (x : X) → (y : Y x) →  Z x y}
        → (e : (x : X) → (y : Y x) → f₀ x y ≡ f₁ x y)
        → f₀ ≡ f₁
funext₂ {X = X} {Y} {Z} {f₀} {f₁} e = funext (λ x → funext (e x))
{-
   Could also proceed as follows:
   Z is equivalent to Z' : Σ Y → 𝒰 ℓ''
   f₀ f₁ become f₀' f₁' : (p : Σ Y) → 𝓤 ℓ''
   then by e, f₀' and f₁' are homotopic, hence equal,
   and we conclude that f₀ and f₁ are also equal
   (the map f ↦ f' is an equivalence (presumably))
-}

-- TODO : the rest of $2.9 (transport properties of function types)


-- Universes

_≡𝓤_ = _≃_

≡→≡𝓤 : {A : 𝓤 ℓ} {B : 𝓤 ℓ} (p : A ≡ B) → A ≡𝓤 B
≡→≡𝓤 {ℓ} {A = A} {B} p = 𝕁 (𝓤 ℓ) (λ A B e →  A ≡𝓤 B) (λ A → id , (id , refl) , (id , refl) ) A B p
{-
tr-𝑖𝑑-𝓤 : {A : 𝓤 ℓ} {B : 𝓤 ℓ} (p : A ≡ B) → isbiinvt (tr {X = 𝓤 ℓ} {Y = λ A → A} p)
tr-𝑖𝑑-𝓤 {ℓ} {A = A} {B = B} p =
  𝕁
    (𝓤 ℓ)
    (λ A B p → isbiinvt (tr {X = 𝓤 ℓ} {Y = λ A → A} p))
    (λ A → (id , refl) , (id , refl) )
    A B p

≡→≡𝓤 {ℓ} {A = A} {B} p = tr {X = 𝓤 ℓ} {Y = λ A → A} p , tr-𝑖𝑑-𝓤 p
-}
idtoeqv = ≡→≡𝓤 

idtoeqv/fst : {A : 𝓤 ℓ} {B : 𝓤 ℓ} (p : A ≡ B) → (fst (idtoeqv p)) ≡ tr p
idtoeqv/fst {A = A} {B = A} (refl A) = refl _ 

Univalence : 𝓤 (ℓ ₊)
Univalence {ℓ} = {A B : 𝓤 ℓ} → qinv (idtoeqv {A = A} {B = B})

postulate Univ : {ℓ : Level} → Univalence {ℓ}

≡-≃-≃ : {A B : 𝓤 ℓ} →  (A ≡ B) ≃ (A ≃ B)
≡-≃-≃ {ℓ} {A} {B} = (idtoeqv {A = A} {B = B} , qinv→isbiinvt (Univ {ℓ}))

≡𝓤→≡ : {A B : 𝓤 ℓ}
     → A ≡𝓤 B → A ≡ B
≡𝓤→≡ {ℓ} {A = A} {B = B}  = (fst (Univ {ℓ} {A = A} {B}))

ua = ≡𝓤→≡

≡→≡𝓤-α : {A B : 𝓤 ℓ}
        → ≡→≡𝓤 ∘ ≡𝓤→≡ ∼ id
≡→≡𝓤-α {ℓ} {A = A} {B = B} = fst (snd (Univ {ℓ} {A = A} {B}))

ua-α = ≡→≡𝓤-α 

≡→≡𝓤-β : {A B : 𝓤 ℓ}
        → ≡𝓤→≡ ∘ ≡→≡𝓤 ∼ id
≡→≡𝓤-β {ℓ} {A = A} {B = B} = snd (snd (Univ {ℓ} {A = A} {B}))

ua-β = ≡→≡𝓤-β 

ua/fst : (A B : 𝓤 ℓ) → (e : A ≡𝓤 B) →  fst e ≡ tr (ua e)
ua/fst A B e = (ap fst (ua-α e ⁻¹ )) ∙ idtoeqv/fst (ua e) 



ua/refl : (A : 𝓤 ℓ) → (refl A) ≡ ua (idtoeqv (refl A))
ua/refl A =  (ua-β (refl A)) ⁻¹

ua/symm' : (A B : 𝓤 ℓ) → (e : A ≡ B) → e ⁻¹ ≡ ua (≃/symm (idtoeqv e))
ua/symm' {ℓ} =
  𝕁
    (𝓤 ℓ)
    (λ A B e →  e ⁻¹ ≡ ua (≃/symm (idtoeqv e)))
    (λ A → ua/refl A
      {- refl A ⁻¹ ≡⟨  refl _ ⟩
      refl A ≡⟨  ua/refl A ⟩
      ua (idtoeqv (refl A)) ≡⟨ refl _ ⟩
      ua (≃/refl A) ≡⟨  refl _ ⟩
      ua (≃/symm (≃/refl A)) ≡⟨  refl _ ⟩
      ua (≃/symm (idtoeqv (refl A))) ∎ -}
      
 )

ua/symm : {A B : 𝓤 ℓ} → (F : A ≡𝓤 B) → ua F ⁻¹ ≡ ua (≃/symm F)
ua/symm {A = A} {B} F =
  (ua F) ⁻¹                    ≡⟨ ua/symm' A B (ua F) ⟩
  ua (≃/symm (idtoeqv (ua F))) ≡⟨ ap (λ - → (ua (≃/symm -))) (ua-α F) ⟩
  ua (≃/symm F)                ∎

ua/tran' : (A B C : 𝓤 ℓ) → (e : A ≡ B) (f : B ≡ C)  → e ∙ f ≡ ua (≃/tran (idtoeqv e) (idtoeqv f))
ua/tran' {ℓ} A B C e f =
  𝕁
    (𝓤 ℓ)
    (λ A B e → (C : 𝓤 ℓ) (f : B ≡ C) →  e  ∙ f ≡ ua (≃/tran (idtoeqv e) (idtoeqv f) ))
    (λ A C f → 
      𝕁
        (𝓤 ℓ)
        (λ A C f  →  (refl A) ∙ f ≡ ua (≃/tran (idtoeqv (refl A)) (idtoeqv f) ))
        (λ A →  ua/refl A)
        A C f
    )
    A B e C f

ua/tran : {A B C : 𝓤 ℓ} → (F : A ≡𝓤 B) (G : B ≡𝓤 C) → (ua F) ∙ (ua G) ≡ ua (≃/tran F G)
ua/tran {A = A} {B} {C} F G =
  (ua F) ∙ (ua G)                               ≡⟨ ua/tran' A B C (ua F) (ua G) ⟩
  ua (≃/tran (idtoeqv (ua F)) (idtoeqv (ua G))) ≡⟨ ap (λ - → ua (≃/tran - (idtoeqv (ua G)))) (ua-α F) ⟩
  ua (≃/tran F (idtoeqv (ua G)))                ≡⟨ ap (λ - → ua (≃/tran F - )) (ua-α G) ⟩
  ua (≃/tran F G)                               ∎




-- Identity type

≡/preserves-≃ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
              → {f : A → B} → isbiinvt f
              → (a₀ a₁ : A)
              → isbiinvt (ap {x₀ = a₀} {x₁ = a₁} f)

ap/≃ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
     → (e : A ≃ B) → (a₀ a₁ : A) → (a₀ ≡ a₁) ≃ (fst e a₀ ≡ fst e a₁)
ap/≃ {A = A} {B} (f , b) a₀ a₁ = (ap {x₀ = a₀} {x₁ = a₁} f , ≡/preserves-≃ b a₀ a₁)

≡/preserves-≃ {A = A} {B} {f} e a₀ a₁ = qinv→isbiinvt (g≡ , α≡ , β≡)
 where
  q = isbiinvt→qinv e
  g = qinv#inv q
  α = qinv#α q
  β = qinv#β q

  f≡ : a₀ ≡ a₁ → f a₀ ≡ f a₁
  f≡ = ap f
  g≡ : f a₀ ≡ f a₁ → a₀ ≡ a₁
  g≡ p = (β a₀) ⁻¹ ∙ (ap g p) ∙ (β a₁)

  postulate α≡ : f≡ ∘ g≡ ∼ id
  {- α≡ p =
    (f≡ ∘ g≡) p
      ≡⟨ refl _ ⟩
    (ap f ((β a₀) ⁻¹ ∙ (ap g p) ∙ (β a₁)))
      ≡⟨ {!!} ⟩
    (α (f a₀) ⁻¹ ∙ (α (f a₀))) ∙ (ap f ((β a₀) ⁻¹ ∙ (ap g p) ∙ (β a₁))) ∙ (α (f a₁) ∙ ((α (f a₁)) ⁻¹))
      ≡⟨ {!!} ⟩
    α (f a₀) ⁻¹ ∙ ((α (f a₀)) ∙ (ap f ((β a₀) ⁻¹ ∙ (ap g p) ∙ (β a₁))) ∙ α (f a₁)) ∙ ((α (f a₁)) ⁻¹)
      ≡⟨ {!!} ⟩
    α (f a₀) ⁻¹ ∙ ( ap f ( ap g (ap f ((β a₀) ⁻¹ ∙ (ap g p) ∙ (β a₁))) ∙ α (f a₁)) )  ∙ ((α (f a₁)) ⁻¹)
      ≡⟨ {!!} ⟩
    α (f a₀) ⁻¹ ∙ ( ap f ( (β a₀) ∙ ((β a₀) ⁻¹ ∙ (ap g p) ∙ (β a₁)) ∙ (β a₁ ⁻¹) ) )  ∙ ((α (f a₁)) ⁻¹)
      ≡⟨ {!!} ⟩
    α (f a₀) ⁻¹ ∙ ( ap f ( (ap g p) )  ∙ ((α (f a₁)) ⁻¹)
      ≡⟨ {!!} ⟩
    α (f a₀) ⁻¹ ∙ ( ap (f ∘ g) p )  ∙ ((α (f a₁)) ⁻¹)
      ≡⟨ {!!} ⟩
    α (f a₀) ⁻¹ ∙ ( ap (f ∘ g) p )  ∙ ((α (f a₁)) ⁻¹)
      ≡⟨ ? ⟩
    p
      ∎
  -}

  β≡ : g≡ ∘ f≡ ∼ id
  β≡ p = (g≡ ∘ f≡) p                          ≡⟨  refl _ ⟩
         (β a₀) ⁻¹ ∙ (ap g (ap f p)) ∙ (β a₁) ≡⟨ ap (λ - → (β a₀ ⁻¹) ∙ - ∙ (β a₁)) (ap/∘ f g p ⁻¹) ⟩
         (β a₀) ⁻¹ ∙ (ap (g ∘ f) p) ∙ (β a₁)  ≡⟨ ∼/natural' (g ∘ f) (id) β p ⁻¹  ⟩
         (ap (id) p)                          ≡⟨  ap/id p   ⟩
         p                                    ∎

-- transport :

≡/tr-left : {A : 𝓤 ℓ} (a : A) {a₀ a₁ : A}
          → (p : a₀ ≡ a₁)
          → (q : a ≡ a₀)
          → tr {X = A} {Y = a ≡_} p q ≡ q ∙ p
≡/tr-left {A = A} a {a₀} {a₁} p =
  𝕁
    A
    (λ a₀ a₁ p → (q : a ≡ a₀) → tr p q ≡ q ∙ p)
    (λ (a' : A) (q : a ≡ a') → ∙/right-refl q  ⁻¹)
    a₀ a₁ p

≡/tr-right : {A : 𝓤 ℓ} (a : A) {a₀ a₁ : A}
          → (p : a₀ ≡ a₁)
          → (q : a₀ ≡ a)
          → tr {X = A} {Y = _≡ a} p q ≡ (p ⁻¹) ∙ q
≡/tr-right {A = A} a {a₀} {a₁} p =
  𝕁
    A
    (λ a₀ a₁ p → (q : a₀ ≡ a) → tr p q ≡ p ⁻¹ ∙ q)
    (λ (a' : A) (q : a' ≡ a) → ∙/left-refl q ⁻¹)
    a₀ a₁ p

≡/tr-leftright : {A : 𝓤 ℓ} {a₀ a₁ : A}
          → (p : a₀ ≡ a₁)
          → (q : a₀ ≡ a₀)
          → tr {X = A} {Y = λ a → a ≡ a} p q ≡ (p ⁻¹) ∙ q ∙ p
≡/tr-leftright {A = A} {a₀} {a₁} p =
  𝕁
    A
    (λ a₀ a₁ p → (q : a₀ ≡ a₀) → tr p q ≡ p ⁻¹ ∙ q ∙ p)
    (λ (a' : A) (q : a' ≡ a') → ∙/right-refl q ⁻¹) -- cheating: only ∙/right-refl is not defeq to refl (when using `∙/left-refl q` above, we didn't need it, but used it for visual symmetry but now we don't bother)
    a₀ a₁ p


≡/tr : {A : 𝓤 ℓ} {B : A → 𝓤 ℓ'} {a₀ a₁ : A}
     → (f g : Π B)
     → (p : a₀ ≡ a₁) → (q : f a₀ ≡ g a₀)
     → tr {X = A} {Y = λ a → f a ≡ g a} p q ≡ apd f p ⁻¹ ∙ ap (tr {Y = B} p) q ∙ apd g p
≡/tr {A = A} {B = B} {a₀ = a₀} {a₁ = a₁} f g p q =
  𝕁
    A
    ( λ a₀ a₁ p → (q : f a₀ ≡ g a₀) →   tr {Y = λ a → f a ≡ g a} p q ≡ apd f p ⁻¹ ∙ ap (tr {Y = B} p) q ∙ apd g p)
    ( λ a q →  tr {Y = λ (a : A) → f a ≡ g a} (refl a) q ≡⟨ refl _ ⟩
                q                                        ≡⟨ ∙/right-refl q ⁻¹ ⟩
                (refl (f a)) ∙ q ∙ (refl (g a))          ≡⟨ refl _ ⟩ -- ≡⟨ ap (λ - → - ∙ q ∙ (refl (g a))) (refl _) ⟩
                apd f (refl a) ∙ q ∙ (refl (g a))        ≡⟨ refl _  ⟩ -- ≡⟨ ap (λ - → _ ∙ _ ∙ -) (refl _) ⟩
                apd f (refl a) ∙ q ∙ (apd g (refl a))    ≡⟨ refl _ ⟩
                apd f (refl a) ⁻¹ ∙ q ∙ (apd g (refl a)) ≡⟨ ap (λ - → (apd f (refl a) ⁻¹) ∙ - ∙ (apd g (refl a))) (ap/id q ⁻¹)  ⟩
                apd f (refl a) ⁻¹ ∙ ap (tr {Y = B} (refl a)) q ∙ apd g (refl a) ∎ )
    a₀ a₁ p q

-- as a special case, with f=id and g = constant_a₁ and other combinations, we get the above transports
          


-- Coproducts

_≡+_ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
       → (x₀ x₁ : A + B)
       → 𝓤 (ℓ ⊔ ℓ')
_≡+_ {ℓ} {ℓ'} {A = A} {B} =
  +-induction
    ( λ x₀ → (x₁ : A + B) → 𝓤 (ℓ ⊔ ℓ') )
    ( λ a₀ →
      +-induction (λ x₁ → 𝓤 (ℓ ⊔ ℓ'))
      (λ a₁ → Lift ℓ' (a₀ ≡ a₁))
      (λ b₁ → Lift (ℓ ⊔ ℓ') 𝟘)
    )
    ( λ b₀ →
      +-induction (λ x₁ → 𝓤 (ℓ ⊔ ℓ'))
      (λ a₁ → Lift (ℓ ⊔ ℓ') 𝟘)
      (λ b₁ → Lift ℓ (b₀ ≡ b₁))
    )

≡→≡+ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
     → {x₀ x₁ : A + B}
     → x₀ ≡ x₁
     → x₀ ≡+ x₁
≡→≡+ {A = A} {B} {x₀} {x₀} (refl x₀) = +-induction (λ x → x ≡+ x) (λ a → lift (refl a)) (λ b → lift (refl b)) x₀

≡+→≡ : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
     → {x₀ x₁ : A + B}
     → x₀ ≡+ x₁
     → x₀ ≡ x₁
≡+→≡ {ℓ} {ℓ'} {A = A} {B} {x₀} {x₁} =
  +-induction
    ( λ x₀ → (x₁ : A + B) → (e : x₀ ≡+ x₁) → x₀ ≡ x₁ )
    ( λ a₀ →
      +-induction (λ x₁ → (e : inl a₀ ≡+ x₁) → inl a₀ ≡ x₁)
      (λ a₁ e →  ap inl (lower e) )
      (λ b₁ e → !𝟘 _ (lower e))
    )
    ( λ b₀ →
      +-induction (λ x₁  →  (e : inr b₀ ≡+ x₁) → inr b₀ ≡ x₁)
      (λ a₁ e → !𝟘 _ (lower e))
      (λ b₁ e → ap inr (lower e))
    )
    x₀ x₁


inl≢inr : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} → (a : A) (b :  B) → inl a ≢ inr b
inl≢inr {A = A} {B} a b e = lower (≡→≡+ e)

inl≢inl : {A : 𝓤 ℓ} {B : 𝓤 ℓ'} → (a₀ a₁ : A) →  (a₀ ≢ a₁) → inl {Y = B} a₀ ≢ inl a₁
inl≢inl {A = A} {B} a₀ a₁ ne = λ e →  ne (lower (≡→≡+ e))

≡→≡+-α : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
       → {x₀ x₁ : A + B}
       → (≡→≡+ {x₀ = x₀} {x₁ = x₁}) ∘ (≡+→≡ {x₀ = x₀} {x₁ = x₁}) ∼ id
≡→≡+-α {ℓ} {ℓ'} {A = A} {B} {x₀} {x₁} e =
  +-induction
    ( λ x₀ → (x₁ : A + B) → (e : x₀ ≡+ x₁) → ≡→≡+ {x₀ = x₀} {x₁ = x₁} (≡+→≡ e) ≡ e )
    ( λ a₀ →
      +-induction (λ x₁ → (e : inl a₀ ≡+ x₁) → ≡→≡+ {x₀ = inl a₀} {x₁ = x₁} (≡+→≡ e) ≡ e )
      (λ a₁ e →
        𝕁 A
          (λ a₀ a₁ e → ≡→≡+ {x₀ = inl a₀} {x₁ = inl a₁} (≡+→≡ (lift e)) ≡ lift e)
          (λ a → refl _)
          a₀ a₁ (lower e)
      )
      (λ b₁ e →   !𝟘 _ (lower e) )
    )
    ( λ b₀ →
      +-induction (λ x₁  →  (e : inr b₀ ≡+ x₁) → ≡→≡+ {x₀ = inr b₀} {x₁ = x₁} (≡+→≡ e) ≡ e )
      (λ a₁ e →   !𝟘 _ (lower e) )
      (λ b₁ e →
        𝕁 B
          (λ b₀ b₁ e → ≡→≡+ {x₀ = inr b₀} {x₁ = inr b₁} (≡+→≡ (lift e)) ≡ lift e)
          (λ b → refl _)
          b₀ b₁ (lower e)
      )
    )
    x₀ x₁ e


≡→≡+-β : {A : 𝓤 ℓ} {B : 𝓤 ℓ'}
       → {x₀ x₁ : A + B}
       → (≡+→≡ {x₀ = x₀} {x₁ = x₁}) ∘ (≡→≡+ {x₀ = x₀} {x₁ = x₁}) ∼ id

-- use 𝕁, then matching on x as either inl a or inr b
≡→≡+-β {ℓ} {ℓ'} {A = A} {B} {inl a} {inl a} (refl (inl a)) = refl _ 
≡→≡+-β {ℓ} {ℓ'} {A = A} {B} {inr b} {inr b} (refl (inr b)) = refl _


-- Booleans: it's a simpler version of what's done for +, so let's be

_≡𝔹_ : (x₀ x₁ : 𝔹) →  𝓤₀
x₀ ≡𝔹 x₁ =
  𝔹-recursion
    (𝔹 → 𝓤₀)
    (𝔹-recursion 𝓤₀ 𝟙 𝟘)
    (𝔹-recursion 𝓤₀ 𝟘 𝟙)
  x₀ x₁
    
 
≡→≡𝔹 : {x₀ x₁ : 𝔹} → x₀ ≡ x₁ → x₀ ≡𝔹 x₁
≡→≡𝔹 {x₀ = x} {x₁ = x} (refl x) = 𝔹-induction (λ x → x ≡𝔹 x) ⋆ ⋆ x

≡𝔹→≡ : {x₀ x₁ : 𝔹} → x₀ ≡𝔹 x₁ → x₀ ≡ x₁
≡𝔹→≡ {x₀ = x₀} {x₁ = x₁} p =
  𝔹-induction
    ( λ x₀ → (x₁ : 𝔹) → x₀ ≡𝔹 x₁ → x₀ ≡ x₁ )
    ( 𝔹-induction (λ x₁ → 𝕥 ≡𝔹 x₁ → 𝕥 ≡ x₁)
        (λ e → refl 𝕥)
        (λ e → !𝟘 (𝕥 ≡ 𝕗) e)
    )
    ( 𝔹-induction (λ x₁ → 𝕗 ≡𝔹 x₁ → 𝕗 ≡ x₁)
        (λ e → !𝟘 (𝕗 ≡ 𝕥) e)
        (λ e → refl 𝕗)
    )
    x₀ x₁ p

≡→≡𝔹-α : {x₀ x₁ : 𝔹} → (≡→≡𝔹 {x₀ = x₀} {x₁ = x₁}) ∘ ≡𝔹→≡ ∼ id
≡→≡𝔹-α {𝕥} {𝕥} ⋆ = refl _ 
≡→≡𝔹-α {𝕗} {𝕗} ⋆ = refl _ 
≡→≡𝔹-α {𝕗} {𝕥} p = !𝟘 _ p 
≡→≡𝔹-α {𝕥} {𝕗} p = !𝟘 _ p 

≡→≡𝔹-β : {x₀ x₁ : 𝔹} → (≡𝔹→≡ {x₀ = x₀} {x₁ = x₁}) ∘ ≡→≡𝔹 ∼ id
≡→≡𝔹-β {𝕥} {𝕥} (refl 𝕥) = refl _
≡→≡𝔹-β {𝕗} {𝕗} (refl 𝕗) = refl _


𝕥≢𝕗 : 𝕥 ≢ 𝕗
𝕥≢𝕗 = ≡→≡𝔹 {𝕥} {𝕗}

-- Naturals



_≡ℕ_ : ℕ → ℕ → 𝓤₀
_≡ℕ_ = 
 ℕ-induction (λ (m : ℕ) → (n : ℕ) → 𝓤₀)
  -- If `m = 0`
  (ℕ-induction (λ (n : ℕ) → 𝓤₀)
    𝟙                                {- if `n = 0` -}
    (λ (n : ℕ) (z≡ℕn : 𝓤₀) → 𝟘)     {- if `succ n`, and we're given `z ≡ℕ n :  𝓤₀` -}
  )
  -- If `succ m`, and we have `m≡ℕ_`
  (λ (m : ℕ) (m≡ℕ- : ℕ → 𝓤₀) →
    ℕ-induction (λ (n : ℕ) → 𝓤₀)
      𝟘                                 {- if `n = 0` -}
      (λ (n : ℕ) (sm≡ℕn : 𝓤₀) → m≡ℕ- n){- if `succ n` and we have `succ m ≡ℕ n` for `succ m` and `n` -}
  )



≡→≡ℕ : (m : ℕ) → (n : ℕ) → (p : m ≡ n) → m ≡ℕ n
≡→≡ℕ =
  𝕁
    ℕ
    (λ x y eq → x ≡ℕ y)
    (ℕ-induction (λ m → m ≡ℕ m) ⋆ (λ m p → p))

≡→≡ℕ/zero : ≡→≡ℕ zero zero (refl zero) ≡ ⋆
≡→≡ℕ/zero = refl _
≡→≡ℕ/succ : {n : ℕ} →  ≡→≡ℕ (succ n) (succ n) (refl (succ n)) ≡ ≡→≡ℕ n n (refl n)
≡→≡ℕ/succ {n} = refl _



≡ℕ→≡ : (m n : ℕ) → m ≡ℕ n → m ≡ n
≡ℕ→≡ =
  ℕ-induction (λ m → (n : ℕ) → m ≡ℕ n → m ≡ n)
    ( ℕ-induction _
        ( λ - → refl zero )
        ( λ n IHn zeqs → !𝟘 (zero ≡ succ n) zeqs )
    )
    ( λ m IHm →
      ℕ-induction _
      ( λ seqz → !𝟘 (succ m ≡ zero) seqz )
      ( λ n IHn seqs → ap succ (IHm n seqs) )
    )

≡ℕ→≡/zero-zero : {p : zero ≡ℕ zero} → ≡ℕ→≡ zero zero p ≡ refl zero
≡ℕ→≡/zero-zero {p} = refl _ 
≡ℕ→≡/succ-succ : {m n : ℕ} {p : succ m ≡ℕ succ n} → ≡ℕ→≡ (succ m) (succ n) p ≡ ap succ (≡ℕ→≡ m n p)
≡ℕ→≡/succ-succ {m} {n} {p} = refl _ 




postulate ≡→≡ℕ-α : (m : ℕ) → (n : ℕ) → (≡→≡ℕ m n) ∘ (≡ℕ→≡ m n) ∼ id
{- ≡→≡ℕ-α m n p = 
  ℕ-induction (λ m → (n : ℕ) → (p : m ≡ℕ n) → ((≡→≡ℕ m n) ∘ (≡ℕ→≡ m n)) p ≡ p )
    ( ℕ-induction _
        ( 𝟙-induction _ (refl _) )
        ( λ n IHn zeqs → !𝟘 _ zeqs )
    )
    ( λ m IHm →
      ℕ-induction _
      ( λ seqz → !𝟘 _ seqz )
      ( λ n IHn seqs →
        (≡→≡ℕ (succ m) (succ n) ∘ ≡ℕ→≡ (succ m) (succ n)) seqs
      ≡⟨ refl _ ⟩
        ≡→≡ℕ (succ m) (succ n) (ap succ (≡ℕ→≡ m n seqs))
      ≡⟨ {!!} ⟩
        tr {Y = (succ m) ≡ℕ_} (ap succ (≡ℕ→≡ m n seqs)) ( ≡→≡ℕ (succ m) (succ m) (refl (succ m)) )
      ≡⟨ {!!}  ⟩
        tr {Y = ((succ m) ≡ℕ_) ∘ succ} (≡ℕ→≡ m n seqs) (≡→≡ℕ (succ m) (succ m) (refl (succ m)) )
      ≡⟨ refl _ ⟩
        tr {Y = λ n → (succ m) ≡ℕ (succ n)} (≡ℕ→≡ m n seqs) (≡→≡ℕ (succ m) (succ m) (refl (succ m)) )
      ≡⟨ {!!} ⟩
        tr {Y = m ≡ℕ_} (≡ℕ→≡ m n seqs) (≡→≡ℕ m m (refl m) )
      ≡⟨ {!!} ⟩
        ≡→≡ℕ m n (≡ℕ→≡ m n seqs)
      ≡⟨ IHm n seqs ⟩
        seqs                                                   ∎
      
      )
    )
    m n p
-}

≡→≡ℕ-β : (m : ℕ) → (n : ℕ) → (≡ℕ→≡ m n) ∘ (≡→≡ℕ m n) ∼ id
≡→≡ℕ-β m m (refl m) =
  ℕ-induction
    ( λ m → ((≡ℕ→≡ m m) ∘ (≡→≡ℕ m m)) (refl m) ≡ refl m )
    ( refl _ )
    ( λ m IHm → ((≡ℕ→≡ (succ m) (succ m)) ∘ (≡→≡ℕ (succ m) (succ m))) (refl (succ m)) ≡⟨ refl _ ⟩ -- inductive construction of `≡→≡ℕ`
                (≡ℕ→≡ (succ m) (succ m)) (≡→≡ℕ m m (refl m))                          ≡⟨ refl _ ⟩ -- inductive construction of `≡ℕ→≡`
                ap succ (≡ℕ→≡ m m (≡→≡ℕ m m (refl m)))                                ≡⟨ ap (λ - → ap succ -) IHm ⟩
                ap succ (refl m)                                                      ≡⟨ refl _ ⟩
                refl (succ m)                                                         ∎
    )
    m


≡-≃-≡ℕ : (m n : ℕ) → (m ≡ n) ≃ (m ≡ℕ n)
≡-≃-≡ℕ m n = ≡→≡ℕ m n , (≡ℕ→≡ m n , ≡→≡ℕ-α m n) , (≡ℕ→≡ m n , ≡→≡ℕ-β m n)

succ/≃ : (m n : ℕ) → (succ m ≡ succ n) ≃ (m ≡ n)
succ/≃ m n = ≃/tran (≡-≃-≡ℕ (succ m) (succ n)) (≃/symm (≡-≃-≡ℕ m n)) 


zero≢succ : (n : ℕ) → zero ≢ succ n
zero≢succ n = ≡→≡ℕ zero (succ n)
