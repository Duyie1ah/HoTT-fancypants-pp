{-# OPTIONS --without-K --exact-split --safe #-}

module Universes where

open import Agda.Primitive public
 renaming (
            lzero to ℓ₀      
          ; lsuc to _₊     
          --; Setω to 𝓤ω        -- There is a universe 𝓤ω strictly above 𝓤₀, 𝓤₁, ⋯ , 𝓤ₙ, ⋯
          )
 using    (_⊔_; Level)               -- Least upper bound of two universes, e.g. 𝓤₀ ⊔ 𝓤₁ is 𝓤₁


𝓤 : (ℓ : Level) → Set (ℓ ₊)
𝓤 ℓ = Set ℓ

𝓤₀ = 𝓤 ℓ₀
𝓤₁ = 𝓤 (ℓ₀ ₊)
𝓤₂ = 𝓤 (ℓ₀ ₊ ₊)

universe-of : {ℓ : Level} (X : 𝓤 ℓ) → Level
universe-of {ℓ} X = ℓ

